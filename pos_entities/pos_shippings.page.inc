<?php

/**
 * @file
 * Contains pos_shippings.page.inc..
 *
 * Page callback for Shippings entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Shippings templates.
 *
 * Default template: pos_shippings.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_shippings(array &$variables) {
  // Fetch PosShippings Entity Object.
  $pos_shippings = $variables['elements']['#pos_shippings'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
