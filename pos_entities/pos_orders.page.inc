<?php

/**
 * @file
 * Contains pos_orders.page.inc..
 *
 * Page callback for Orders entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Orders templates.
 *
 * Default template: pos_orders.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_orders(array &$variables) {
  // Fetch PosOrders Entity Object.
  $pos_orders = $variables['elements']['#pos_orders'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
