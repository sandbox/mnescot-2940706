<?php

/**
 * @file
 * Contains pos_items.page.inc..
 *
 * Page callback for Items entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Items templates.
 *
 * Default template: pos_items.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_items(array &$variables) {
  // Fetch PosItems Entity Object.
  $pos_items = $variables['elements']['#pos_items'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
