<?php

/**
 * @file
 * Contains pos_products.page.inc..
 *
 * Page callback for Products entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Products templates.
 *
 * Default template: pos_products.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_products(array &$variables) {
  // Fetch PosProducts Entity Object.
  $pos_products = $variables['elements']['#pos_products'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
