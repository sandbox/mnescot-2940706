<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosReconciliationInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Reconciliation entities.
 *
 * @ingroup pos_entities
 */
interface PosReconciliationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Reconciliation name.
   *
   * @return string
   *   Name of the Reconciliation.
   */
  public function getName();

  /**
   * Sets the Reconciliation name.
   *
   * @param string $name
   *   The Reconciliation name.
   *
   * @return \Drupal\pos_entities\PosReconciliationInterface
   *   The called Reconciliation entity.
   */
  public function setName($name);

  /**
   * Gets the Reconciliation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Reconciliation.
   */
  public function getCreatedTime();

  /**
   * Sets the Reconciliation creation timestamp.
   *
   * @param int $timestamp
   *   The Reconciliation creation timestamp.
   *
   * @return \Drupal\pos_entities\PosReconciliationInterface
   *   The called Reconciliation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Reconciliation published status indicator.
   *
   * Unpublished Reconciliation are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Reconciliation is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Reconciliation.
   *
   * @param bool $published
   *   TRUE to set this Reconciliation to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosReconciliationInterface
   *   The called Reconciliation entity.
   */
  public function setPublished($published);

}
