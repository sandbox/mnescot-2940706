<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosCartListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Cart entities.
 *
 * @ingroup pos_entities
 */
class PosCartListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cart ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosCart */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pos_cart.edit_form', array(
          'pos_cart' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
