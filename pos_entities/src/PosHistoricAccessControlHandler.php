<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosHistoricAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Historic entity.
 *
 * @see \Drupal\pos_entities\Entity\PosHistoric.
 */
class PosHistoricAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosHistoricInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished historic entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published historic entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit historic entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete historic entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add historic entities');
  }

}
