<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosCartInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cart entities.
 *
 * @ingroup pos_entities
 */
interface PosCartInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cart session ID.
   *
   * @return string
   *   session ID of the Cart.
   */
  public function getSessionId();

  /**
   * Sets the Cart session ID.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setSessionId();

  /**
   * Gets the Cart product ID.
   *
   * @return string
   *   Product ID of the Cart.
   */
  public function getProductId();

  /**
   * Sets the Cart product ID.
   *
   * @param string $value
   *   The Cart product ID.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setProductId($value);

  /**
   * Gets the Cart SKU.
   *
   * @return string
   *   SKU of the Cart.
   */
  public function getSku();

  /**
   * Sets the Cart SKU.
   *
   * @param string $value
   *   The Cart SKU.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setSku($value);


  /**
   * Gets the Cart quantity.
   *
   * @return string
   *   Quantity of the Cart.
   */
  public function getQuantity();

  /**
   * Sets the Cart quantity.
   *
   * @param string $value
   *   The Cart quantity.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setQuantity($value);

  /**
   * Gets the Cart creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cart.
   */
  public function getCreatedTime();

  /**
   * Sets the Cart creation timestamp.
   *
   * @param int $timestamp
   *   The Cart creation timestamp.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cart published status indicator.
   *
   * Unpublished Cart are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cart is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cart.
   *
   * @param bool $published
   *   TRUE to set this Cart to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setPublished($published);

}
