<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosOrderStatusListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Order Status entities.
 *
 * @ingroup pos_entities
 */
class PosOrderStatusListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Order Status ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosOrderStatus */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pos_order_status.edit_form', array(
          'pos_order_status' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
