<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosShippingsInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Shippings entities.
 *
 * @ingroup pos_entities
 */
interface PosShippingsInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Shippings first name.
   *
   * @return string
   *   Name of the Customer Profiles.
   */
  public function getFirstName();

  /**
   * Sets the Shippings first name.
   *
   * @param string $value
   *   The Customer Profiles first name.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setFirstName($value);

  /**
   * Gets the Shippings last name.
   *
   * @return string
   *   Name of the Customer Profiles.
   */
  public function getLastName();

  /**
   * Sets the Shippings last name.
   *
   * @param string $value
   *   The Customer Profiles last name.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setLastName($value);

  /**
   * Gets the Shippings creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Shippings.
   */
  public function getCreatedTime();

  /**
   * Sets the Shippings creation timestamp.
   *
   * @param int $timestamp
   *   The Shippings creation timestamp.
   *
   * @return \Drupal\pos_entities\PosShippingsInterface
   *   The called Shippings entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Shippings published status indicator.
   *
   * Unpublished Shippings are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Shippings is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Shippings.
   *
   * @param bool $published
   *   TRUE to set this Shippings to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosShippingsInterface
   *   The called Shippings entity.
   */
  public function setPublished($published);

}
