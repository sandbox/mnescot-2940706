<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosReconciliationAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Reconciliation entity.
 *
 * @see \Drupal\pos_entities\Entity\PosReconciliation.
 */
class PosReconciliationAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosReconciliationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished reconciliation entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published reconciliation entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit reconciliation entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete reconciliation entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add reconciliation entities');
  }

}
