<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosReconciliationListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Reconciliation entities.
 *
 * @ingroup pos_entities
 */
class PosReconciliationListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Reconciliation ID');
    $header['sku'] = $this->t('SKU');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosReconciliation */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pos_reconciliation.edit_form', array(
          'pos_reconciliation' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
