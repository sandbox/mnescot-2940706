<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosProductsListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Products entities.
 *
 * @ingroup pos_entities
 */
class PosProductsListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Products ID');
    $header['sku'] = $this->t('SKU');
    $header['title'] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosProducts */
    $row['id'] = $entity->id();
    $row['sku'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pos_products.edit_form', array(
          'pos_products' => $entity->id(),
        )
      )
    );
    $row['title'] = $entity->getTheValue('title');
    return $row + parent::buildRow($entity);
  }

}
