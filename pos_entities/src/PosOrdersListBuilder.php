<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosOrdersListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Orders entities.
 *
 * @ingroup pos_entities
 */
class PosOrdersListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Orders ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosOrders */
    $a = 1;
    $row['id'] = $this->l(
      $entity->id(),
      new Url(
        'entity.pos_orders.edit_form', array(
          'pos_orders' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
