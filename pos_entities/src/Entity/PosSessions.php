<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosSessions.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pos_entities\PosSessionsInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Sessions entity.
 *
 * @ingroup pos_entities
 *
 * @ContentEntityType(
 *   id = "pos_sessions",
 *   label = @Translation("Sessions"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pos_entities\PosSessionsListBuilder",
 *     "views_data" = "Drupal\pos_entities\Entity\PosSessionsViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pos_entities\Form\PosSessionsForm",
 *       "add" = "Drupal\pos_entities\Form\PosSessionsForm",
 *       "edit" = "Drupal\pos_entities\Form\PosSessionsForm",
 *       "delete" = "Drupal\pos_entities\Form\PosSessionsDeleteForm",
 *     },
 *     "access" = "Drupal\pos_entities\PosSessionsAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pos_entities\PosSessionsHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pos_sessions",
 *   persistent_cache = FALSE,
 *   admin_permission = "administer sessions entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "order_id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pos_sessions/{pos_sessions}",
 *     "add-form" = "/admin/structure/pos_sessions/add",
 *     "edit-form" = "/admin/structure/pos_sessions/{pos_sessions}/edit",
 *     "delete-form" = "/admin/structure/pos_sessions/{pos_sessions}/delete",
 *     "collection" = "/admin/structure/pos_sessions",
 *   },
 *   field_ui_base_route = "pos_sessions.settings"
 * )
 */
class PosSessions extends ContentEntityBase implements PosSessionsInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * Overriding for getValue() function.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return mixed
   *   The field value.
   */
  public function getValue($field_name) {
    $value = parent::get($field_name)->getValue();
    if (isset($value[0]['value'])) {
      $value = $value[0]['value'];
    }
    elseif (isset($value[0]['target_id'])) {
      $value = $value[0]['target_id'];
    }
    else {
      $value = NULL;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->getValue('order_id');
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderId($order_id) {
    $this->set('order_id', $order_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerId() {
    return $this->getValue('customer_id');
  }

  /**
   * {@inheritdoc}
   */
  public function setCustomerId($customer_id) {
    $this->set('customer_id', $customer_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTimeTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Sessions entity.'))
      ->setSettings(array(
        'max_length' => 64,
      ))
      ->setDefaultValueCallback('Drupal\pos_entities\Entity\PosSessions::getSessionId')
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Sessions entity.'))
      ->setReadOnly(TRUE);

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pos_orders')
      ->setSetting('handler', 'default');

    $fields['customer_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pos_customer_profiles')
      ->setSetting('handler', 'default');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Callback for entity ID default value.
   *
   * @return string
   *   Session ID.
   */
  public static function getSessionId() {
    $session_manager = \Drupal::service('pos_operations.session');
    return $session_manager->getId();
  }

}
