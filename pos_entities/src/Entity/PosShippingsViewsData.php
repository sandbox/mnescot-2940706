<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosShippings.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Shippings entities.
 */
class PosShippingsViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_shippings']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Shippings'),
      'help' => $this->t('The Shippings ID.'),
    );

    return $data;
  }

}
