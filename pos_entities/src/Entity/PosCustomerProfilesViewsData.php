<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosCustomerProfiles.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Customer Profiles entities.
 */
class PosCustomerProfilesViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_customer_profiles']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Customer Profiles'),
      'help' => $this->t('The Customer Profiles ID.'),
    );

    return $data;
  }

}
