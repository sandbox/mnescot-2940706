<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosSessions.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Sessions entities.
 */
class PosSessionsViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_sessions']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Sessions'),
      'help' => $this->t('The Sessions ID.'),
    );

    return $data;
  }

}
