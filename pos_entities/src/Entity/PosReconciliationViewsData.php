<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosReconciliation.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Reconciliation entities.
 */
class PosReconciliationViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_reconciliation']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Reconciliation'),
      'help' => $this->t('The Reconciliation ID.'),
    );

    return $data;
  }

}
