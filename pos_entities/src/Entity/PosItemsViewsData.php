<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosItems.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Items entities.
 */
class PosItemsViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_items']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Items'),
      'help' => $this->t('The Items ID.'),
    );

    return $data;
  }

}
