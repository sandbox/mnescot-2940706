<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosCart.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pos_entities\PosCartInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cart entity.
 *
 * @ingroup pos_entities
 *
 * @ContentEntityType(
 *   id = "pos_cart",
 *   label = @Translation("Cart"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pos_entities\PosCartListBuilder",
 *     "views_data" = "Drupal\pos_entities\Entity\PosCartViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pos_entities\Form\PosCartForm",
 *       "add" = "Drupal\pos_entities\Form\PosCartForm",
 *       "edit" = "Drupal\pos_entities\Form\PosCartForm",
 *       "delete" = "Drupal\pos_entities\Form\PosCartDeleteForm",
 *     },
 *     "access" = "Drupal\pos_entities\PosCartAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pos_entities\PosCartHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pos_cart",
 *   persistent_cache = FALSE,
 *   admin_permission = "administer cart entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "session_id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pos_cart/{pos_cart}",
 *     "add-form" = "/admin/structure/pos_cart/add",
 *     "edit-form" = "/admin/structure/pos_cart/{pos_cart}/edit",
 *     "delete-form" = "/admin/structure/pos_cart/{pos_cart}/delete",
 *     "collection" = "/admin/structure/pos_cart",
 *   },
 *   field_ui_base_route = "pos_cart.settings"
 * )
 */
class PosCart extends ContentEntityBase implements PosCartInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSessionId() {
    return $this->get('session_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSessionId() {
    $session_manager = \Drupal::service('pos_operations.session');
    $this->set('session_id', $session_manager->getId());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProductId() {
    return $this->get('product_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProductId($value) {
    $this->set('product_id', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSku() {
    return $this->get('sku')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSku($value) {
    $this->set('sku', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantity() {
    return $this->get('quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity($value) {
    $this->set('quantity', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCopyLimit() {
    return $this->get('copy_limit')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCopyLimit($value) {
    $this->set('copy_limit', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setReadOnly(TRUE);

    $fields['session_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Session ID'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pos_sessions')
      ->setSetting('handler', 'default');

    $fields['product_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product ID'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pos_products')
      ->setSetting('handler', 'default');

    $fields['sku'] = BaseFieldDefinition::create('string')
      ->setLabel(t('SKU'))
      ->setSettings(array(
        'max_length' => 10,
      ));

    $fields['quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quantity'))
      ->setDefaultValue(0);

    $fields['copy_limit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Copy Limit'))
      ->setDefaultValue(0);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
