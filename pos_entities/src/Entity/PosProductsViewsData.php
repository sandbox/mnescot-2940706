<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosProducts.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Products entities.
 */
class PosProductsViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_products']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Products'),
      'help' => $this->t('The Products ID.'),
    );

    return $data;
  }

}
