<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosOrderStatus.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pos_entities\PosOrderStatusInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Order Status entity.
 *
 * @ingroup pos_entities
 *
 * @ContentEntityType(
 *   id = "pos_order_status",
 *   label = @Translation("Order Status"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pos_entities\PosOrderStatusListBuilder",
 *     "views_data" = "Drupal\pos_entities\Entity\PosOrderStatusViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pos_entities\Form\PosOrderStatusForm",
 *       "add" = "Drupal\pos_entities\Form\PosOrderStatusForm",
 *       "edit" = "Drupal\pos_entities\Form\PosOrderStatusForm",
 *       "delete" = "Drupal\pos_entities\Form\PosOrderStatusDeleteForm",
 *     },
 *     "access" = "Drupal\pos_entities\PosOrderStatusAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pos_entities\PosOrderStatusHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pos_order_status",
 *   admin_permission = "administer order status entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",

 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pos_order_status/{pos_order_status}",
 *     "add-form" = "/admin/structure/pos_order_status/add",
 *     "edit-form" = "/admin/structure/pos_order_status/{pos_order_status}/edit",
 *     "delete-form" = "/admin/structure/pos_order_status/{pos_order_status}/delete",
 *     "collection" = "/admin/structure/pos_order_status",
 *   },
 *   field_ui_base_route = "pos_order_status.settings"
 * )
 */
class PosOrderStatus extends ContentEntityBase implements PosOrderStatusInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Order Status entity.'))
      ->setReadOnly(TRUE)
      ->setSettings(array(
        'max_length' => 32,
      ));
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Order Status entity.'))
      ->setSettings(array(
        'max_length' => 64,
      ));

    return $fields;
  }

}
