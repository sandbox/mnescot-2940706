<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosOrders.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Orders entities.
 */
class PosOrdersViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_orders']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Orders'),
      'help' => $this->t('The Orders ID.'),
    );

    return $data;
  }

}
