<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosCart.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Cart entities.
 */
class PosCartViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_cart']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Cart'),
      'help' => $this->t('The Cart ID.'),
    );

    return $data;
  }

}
