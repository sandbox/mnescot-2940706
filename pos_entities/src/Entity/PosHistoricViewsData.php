<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosHistoric.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Historic entities.
 */
class PosHistoricViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_historic']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Historic'),
      'help' => $this->t('The Historic ID.'),
    );

    return $data;
  }

}
