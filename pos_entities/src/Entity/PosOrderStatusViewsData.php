<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosOrderStatus.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Order Status entities.
 */
class PosOrderStatusViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pos_order_status']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Order Status'),
      'help' => $this->t('The Order Status ID.'),
    );

    return $data;
  }

}
