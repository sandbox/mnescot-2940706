<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosOrders.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pos_entities\PosOrdersInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Orders entity.
 *
 * @ingroup pos_entities
 *
 * @ContentEntityType(
 *   id = "pos_orders",
 *   label = @Translation("Orders"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pos_entities\PosOrdersListBuilder",
 *     "views_data" = "Drupal\pos_entities\Entity\PosOrdersViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pos_entities\Form\PosOrdersForm",
 *       "add" = "Drupal\pos_entities\Form\PosOrdersForm",
 *       "edit" = "Drupal\pos_entities\Form\PosOrdersForm",
 *       "delete" = "Drupal\pos_entities\Form\PosOrdersDeleteForm",
 *     },
 *     "access" = "Drupal\pos_entities\PosOrdersAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pos_entities\PosOrdersHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pos_orders",
 *   persistent_cache = FALSE,
 *   admin_permission = "administer orders entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "customer_id" = "customer_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pos_orders/{pos_orders}",
 *     "add-form" = "/admin/structure/pos_orders/add",
 *     "edit-form" = "/admin/structure/pos_orders/{pos_orders}/edit",
 *     "delete-form" = "/admin/structure/pos_orders/{pos_orders}/delete",
 *     "collection" = "/admin/structure/pos_orders",
 *   },
 *   field_ui_base_route = "pos_orders.settings"
 * )
 */
class PosOrders extends ContentEntityBase implements PosOrdersInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * Overriding for getValue() function.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return mixed
   *   The field value.
   */
  public function getValue($field_name) {
    $value = parent::get($field_name)->getValue();
    if (isset($value[0]['value'])) {
      $value = $value[0]['value'];
    }
    elseif (isset($value[0]['target_id'])) {
      $value = $value[0]['target_id'];
    }
    else {
      $value = NULL;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('email')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($email) {
    $this->set('email', $email);
  }


  /**
   * {@inheritdoc}
   */
  public function getCallerLanguage() {
    return $this->get('caller_language')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCallerLanguage($caller_language) {
    $this->set('caller_language', $caller_language);
  }


  /**
   * {@inheritdoc}
   */
  public function getCallCenter() {
    return $this->get('call_center')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCallCenter($call_center) {
    $this->set('call_center', $call_center);
  }

  /**
   * {@inheritdoc}
   */
  public function getHowReceived() {
    return $this->get('how_received')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHowReceived($how_received) {
    $this->set('how_received', $how_received);
  }

  /**
   * {@inheritdoc}
   */
  public function getHowHeard() {
    return $this->get('how_heard')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHowHeard($how_heard) {
    $this->set('how_heard', $how_heard);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizationType() {
    return $this->get('organization_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrganizationType($organization_type) {
    $this->set('organization_type', $organization_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getJobTitle() {
    return $this->get('job_title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobTitle($job_title) {
    $this->set('job_title', $job_title);
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerId() {
    return $this->get('customer_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCustomerId($id) {
    $this->set('customer_id', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function setBulk($isbulk) {
    $this->set('is_bulk', $isbulk);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBulk() {
    return $this->get('is_bulk')->value;
  }
  

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($vid) {
    $this->set('status', $vid);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Orders entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Orders entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Orders entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Status'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pos_order_status')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 10,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['customer_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'pos_customer_profiles')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 15,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 15,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    /*
    $fields['is_bulk'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Is this a bulk order?'))
        ->setDefaultValue(FALSE)
        ->setDescription(t('Are there quantities in this order that exceed the copy limit?'))
        ->setRevisionable(TRUE)
        ->setTranslatable(TRUE)
        ->setDisplayOptions('form', array(
            'type' => 'boolean_checkbox',
            'settings' => array(
                'display_label' => TRUE,
            ),
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    */
    $fields['is_bulk'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Is this a bulk order?'))
        ->setDefaultValue(FALSE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription('The email address of the customer.')
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['caller_language'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Caller Language'))
        ->setDescription('The Caller Language.')
        ->setRevisionable(TRUE)
        ->setSetting('target_type', 'taxonomy_term')
        ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'language']])
        ->setTargetEntityTypeId('language')
        ->setSetting('handler', 'default')
        ->setTranslatable(TRUE)
        ->setDisplayOptions('view', array(
            'label' => 'hidden',
            'type' => 'string',
            'weight' => 23,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'options_select',
            'weight' => 25,
            'settings' => array(
                'match_operator' => 'CONTAINS',
                'size' => '60',
                'autocomplete_type' => 'tags',
                'placeholder' => '',
            ),
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);


    $fields['call_center'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Call Center'))
      ->setDescription('The Call Center on which the Order was generated.')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'pos_call_center']])
      ->setTargetEntityTypeId('pos_call_center')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 25,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 25,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['how_received'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('How it was received'))
      ->setDescription('The means by which the Order was received.')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'pos_how_received']])
      ->setTargetEntityTypeId('pos_how_received')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 30,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 30,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['how_heard'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('How heard'))
      ->setDescription('How do you heard about NIA Publications?')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'pos_how_heard']])
      ->setTargetEntityTypeId('pos_how_heard')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 35,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 35,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['job_title'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Job title'))
      ->setDescription('What’s your job title?')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
//      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'job_title']])
      ->setSetting('handler_settings', [
        'target_bundles' => ['taxonomy_term' => 'job_title'],
//        'auto_create' => TRUE,
      ])
      ->setTargetEntityTypeId('job_title')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 40,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 40,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['organization_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Organization?'))
      ->setDescription('What best describes your organization?')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'organization_type']])
      ->setTargetEntityTypeId('organization_type')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 40,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 40,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['batch_owner_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Batch Owner'))
      ->setDescription(t('The user ID of batch owner.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['batched'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Batched'))
      ->setDescription(t('The time that the entity was batched.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));


    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
