<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Entity\PosProducts.
 */

namespace Drupal\pos_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pos_entities\PosProductsInterface;
use Drupal\user\UserInterface;
use Drupal\link\LinkItemInterface;

/**
 * Defines the Products entity.
 *
 * @ingroup pos_entities
 *
 * @ContentEntityType(
 *   id = "pos_products",
 *   label = @Translation("Products"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pos_entities\PosProductsListBuilder",
 *     "views_data" = "Drupal\pos_entities\Entity\PosProductsViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pos_entities\Form\PosProductsForm",
 *       "add" = "Drupal\pos_entities\Form\PosProductsForm",
 *       "edit" = "Drupal\pos_entities\Form\PosProductsForm",
 *       "delete" = "Drupal\pos_entities\Form\PosProductsDeleteForm",
 *     },
 *     "access" = "Drupal\pos_entities\PosProductsAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pos_entities\PosProductsHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pos_products",
 *   persistent_cache = FALSE,
 *   admin_permission = "administer products entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "sku",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "active",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pos_products/{pos_products}",
 *     "add-form" = "/admin/structure/pos_products/add",
 *     "edit-form" = "/admin/structure/pos_products/{pos_products}/edit",
 *     "delete-form" = "/admin/structure/pos_products/{pos_products}/delete",
 *     "collection" = "/admin/structure/pos_products",
 *   },
 *   field_ui_base_route = "pos_products.settings"
 * )
 */
class PosProducts extends ContentEntityBase implements PosProductsInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCopyLimit() {
    return $this->get('copy_limit')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCopyLimit($value) {
    $this->set('copy_limit', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSku() {
    return $this->get('sku')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSku($value) {
    $this->set('sku', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Products entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId');

    $fields['sku'] = BaseFieldDefinition::create('string')
      ->setLabel(t('SKU'))
      ->setSettings(array(
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Digital Document'))
      ->setSettings([
//        'uri_scheme' => 'public://publication_file',
        'alt_field_required' => FALSE,
        'file_extensions' => 'pdf doc docx',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'default',
        'weight' => 13,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'hidden',
        'type' => 'image_image',
        'weight' => 13,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setSettings([
//        'uri_scheme' => 'public://publication_cover',
        'alt_field_required' => FALSE,
        'file_extensions' => 'png jpg jpeg',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'default',
        'weight' => 15,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'hidden',
        'type' => 'image_image',
        'weight' => 15,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['topics'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Topics'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'topics']])
      ->setTargetEntityTypeId('topics')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 21,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 21,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['featured'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Featured'))
      ->setSettings(array(
        'allowed_values' => array(
          'featured' => 'Featured Publications',
          'topics' => 'Featured in Topics',
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'list_default',
        'weight' => 22,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_buttons',
        'weight' => 22,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['link_other_language'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Link to other language'))
      ->setDescription(t('Lorem ipsum dolor sit amet, consectetur adipiscing elit.'))
      ->setSettings(array(
        'link_type' => LinkItemInterface::LINK_INTERNAL,
        'title' => DRUPAL_DISABLED,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'link_default',
        'weight' => 23,
      ));

    $fields['other_language'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Other Language'))
      ->setDescription(t('The other language of the Publication.'))
      ->setSettings(array(
        'allowed_values' => array(
          '1' => 'English',
          '2' => 'Spanish',
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'list_default',
        'weight' => 24,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_buttons',
        'weight' => 24,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['stock'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stock'))
      ->setDefaultValue(0)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 25,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 25,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['stock_threshold'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stock Threshold'))
      ->setDefaultValue(0)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 30,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 30,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['copy_limit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Copy Limit'))
      ->setDefaultValue(0)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 35,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 35,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 40,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'boolean',
        'weight' => 40,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['hidden'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Hide from Reports'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 45,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'boolean',
        'weight' => 45,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));



    return $fields;
  }

  /**
   * Custon method to extract the value of a field.
   *
   * This method saves time and code because most of the fields in this
   * entity are not multi valued.
   * Therefore this method saves coding: $value[0]['value']
   *
   * @param string $field_name
   *   The field name.
   *
   * @return mixed|null
   *   The value of the field;
   */
  public function getTheValue($field_name) {
    $value = NULL;
    if (isset($this->$field_name)) {
      $value = $this->$field_name->getValue();
      if (isset($value[0]['value'])) {
        $value = $value[0]['value'];
      }
      elseif (isset($value[0]['target_id']) && count($value[0]) == 1) {
        $value = $value[0]['target_id'];
      }
      else {
        $value = $value[0];
      }

    }
    return $value;
  }

}
