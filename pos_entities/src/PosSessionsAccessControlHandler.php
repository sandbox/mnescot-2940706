<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosSessionsAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Sessions entity.
 *
 * @see \Drupal\pos_entities\Entity\PosSessions.
 */
class PosSessionsAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosSessionsInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished sessions entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published sessions entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit sessions entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete sessions entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add sessions entities');
  }

}
