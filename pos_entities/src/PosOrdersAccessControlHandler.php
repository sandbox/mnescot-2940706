<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosOrdersAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Orders entity.
 *
 * @see \Drupal\pos_entities\Entity\PosOrders.
 */
class PosOrdersAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosOrdersInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished orders entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published orders entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit orders entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete orders entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add orders entities');
  }

}
