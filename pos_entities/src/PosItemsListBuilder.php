<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosItemsListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Items entities.
 *
 * @ingroup pos_entities
 */
class PosItemsListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Items ID');
    $header['order_id'] = $this->t('Order Number');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosItems */
    $row['id'] = $entity->id();
    $row['order_id'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pos_items.edit_form', array(
          'pos_items' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
