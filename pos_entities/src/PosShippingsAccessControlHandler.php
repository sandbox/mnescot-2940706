<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosShippingsAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Shippings entity.
 *
 * @see \Drupal\pos_entities\Entity\PosShippings.
 */
class PosShippingsAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosShippingsInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished shippings entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published shippings entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit shippings entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete shippings entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add shippings entities');
  }

}
