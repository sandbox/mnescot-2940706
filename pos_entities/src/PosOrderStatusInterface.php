<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosOrderStatusInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Order Status entities.
 *
 * @ingroup pos_entities
 */
interface PosOrderStatusInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Order Status name.
   *
   * @return string
   *   Name of the Order Status.
   */
  public function getName();

  /**
   * Sets the Order Status name.
   *
   * @param string $name
   *   The Order Status name.
   *
   * @return \Drupal\pos_entities\PosOrderStatusInterface
   *   The called Order Status entity.
   */
  public function setName($name);

  /**
   * Gets the Order Status creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Order Status.
   */
  public function getCreatedTime();

  /**
   * Sets the Order Status creation timestamp.
   *
   * @param int $timestamp
   *   The Order Status creation timestamp.
   *
   * @return \Drupal\pos_entities\PosOrderStatusInterface
   *   The called Order Status entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Order Status published status indicator.
   *
   * Unpublished Order Status are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Order Status is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Order Status.
   *
   * @param bool $published
   *   TRUE to set this Order Status to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosOrderStatusInterface
   *   The called Order Status entity.
   */
  public function setPublished($published);

}
