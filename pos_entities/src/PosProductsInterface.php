<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosProductsInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Products entities.
 *
 * @ingroup pos_entities
 */
interface PosProductsInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Products title.
   *
   * @return string
   *   Title of the Products.
   */
  public function getTitle();

  /**
   * Sets the Products title.
   *
   * @param string $value
   *   The Products title.
   *
   * @return \Drupal\pos_entities\PosProductsInterface
   *   The called Products entity.
   */
  public function setTitle($value);

  /**
   * Gets the Products copy limit.
   *
   * @return string
   *   Copy limit of the Products.
   */
  public function getCopyLimit();

  /**
   * Sets the Products copy limit.
   *
   * @param string $value
   *   The Products copy limit.
   *
   * @return \Drupal\pos_entities\PosProductsInterface
   *   The called Products entity.
   */
  public function setCopyLimit($value);

  /**
   * Gets the Cart SKU.
   *
   * @return string
   *   SKU of the Cart.
   */
  public function getSku();

  /**
   * Sets the Cart SKU.
   *
   * @param string $value
   *   The Cart SKU.
   *
   * @return \Drupal\pos_entities\PosCartInterface
   *   The called Cart entity.
   */
  public function setSku($value);

  /**
   * Gets the Products creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Products.
   */
  public function getCreatedTime();

  /**
   * Sets the Products creation timestamp.
   *
   * @param int $timestamp
   *   The Products creation timestamp.
   *
   * @return \Drupal\pos_entities\PosProductsInterface
   *   The called Products entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Products published status indicator.
   *
   * Unpublished Products are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Products is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Products.
   *
   * @param bool $published
   *   TRUE to set this Products to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosProductsInterface
   *   The called Products entity.
   */
  public function setPublished($published);

}
