<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosHistoricInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Historic entities.
 *
 * @ingroup pos_entities
 */
interface PosHistoricInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Historic Order ID.
   *
   * @return int
   *   Status of the Historic.
   */
  public function getOrderId();

  /**
   * Sets the Historic Order ID.
   *
   * @param int $order_id
   *   The Historic Order ID.
   *
   * @return \Drupal\pos_entities\PosHistoricInterface
   *   The called Historic entity.
   */
  public function setOrderId($order_id);

  /**
   * Gets the Historic status.
   *
   * @return string
   *   Status of the Historic.
   */
  public function getStatus();

  /**
   * Sets the Historic status.
   *
   * @param string $status
   *   The Historic status.
   *
   * @return \Drupal\pos_entities\PosHistoricInterface
   *   The called Historic entity.
   */
  public function setStatus($status);

  /**
   * Gets the Historic comment.
   *
   * @return string
   *   Comment of the Historic.
   */
  public function getComment();

  /**
   * Sets the Historic comment.
   *
   * @param string $comment
   *   The Historic comment.
   *
   * @return \Drupal\pos_entities\PosHistoricInterface
   *   The called Historic entity.
   */
  public function setComment($comment);

  /**
   * Gets the Historic creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Historic.
   */
  public function getCreatedTime();

  /**
   * Sets the Historic creation timestamp.
   *
   * @param int $timestamp
   *   The Historic creation timestamp.
   *
   * @return \Drupal\pos_entities\PosHistoricInterface
   *   The called Historic entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Historic published status indicator.
   *
   * Unpublished Historic are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Historic is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Historic.
   *
   * @param bool $published
   *   TRUE to set this Historic to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosHistoricInterface
   *   The called Historic entity.
   */
  public function setPublished($published);

}
