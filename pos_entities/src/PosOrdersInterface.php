<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosOrdersInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Orders entities.
 *
 * @ingroup pos_entities
 */
interface PosOrdersInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.


  /**
   * Gets the Orders email.
   *
   * @return string
   *   The email of the Orders.
   */
  public function getEmail();

  /**
   * Sets the email of the Order.
   *
   * @param string $email
   *   The email of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setEmail($email);



  /**
   * Gets the Orders caller language.
   *
   * @return int
   *   The caller language of the Orders.
   */
  public function getCallerLanguage();

  /**
   * Sets the caller language of the Order.
   *
   * @param int $caller_language
   *   The caller language of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setCallerLanguage($caller_language);
  
  
  
  
  /**
   * Gets the Orders call center.
   *
   * @return int
   *   The call center of the Orders.
   */
  public function getCallCenter();

  /**
   * Sets the call center of the Order.
   *
   * @param int $call_center
   *   The call center of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setCallCenter($call_center);

  /**
   * Gets the Orders how received.
   *
   * @return int
   *   The how received of the Orders.
   */
  public function getHowReceived();

  /**
   * Sets the how received of the Order.
   *
   * @param int $how_received
   *   The how received of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setHowReceived($how_received);

  /**
   * Gets the Orders how heard.
   *
   * @return int
   *   The how heard of the Order.
   */
  public function getHowHeard();

  /**
   * Sets the how heard of the Order.
   *
   * @param int $how_heard
   *   The how heard of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setHowHeard($how_heard);

  /**
   * Gets the Orders job title.
   *
   * @return int
   *   The job title of the Order.
   */
  public function getJobTitle();

  /**
   * Sets the job title of the Order.
   *
   * @param int $job_title
   *   The job title of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setJobTitle($job_title);

  /**
   * Gets the Order's organization type.
   *
   * @return int
   *   The organization type of the Order.
   */
  public function getOrganizationType();

  /**
   * Sets the organization type of the Order.
   *
   * @param int $organization_type
   *   The organization type of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setOrganizationType($organization_type);

  /**
   * Gets the customer ID of the Order.
   *
   * @return int
   *   The customer ID  of the Orders.
   */
  public function getCustomerId();

  /**
   * Sets the customer ID of the Order.
   *
   * @param int $id
   *   The customer ID the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setCustomerId($id);

  /**
   * Gets the vid of Status of the Order.
   *
   * @return int
   *   Vid of the Status of the Orders.
   */
  public function getStatus();

  /**
   * Sets the vid of Status of the Order.
   *
   * @param int $vid
   *   The vid of Status of the Order.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Order entity.
   */
  public function setStatus($vid);

  /**
   * Gets the Orders creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Orders.
   */
  public function getCreatedTime();

  /**
   * Sets the Orders creation timestamp.
   *
   * @param int $timestamp
   *   The Orders creation timestamp.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Orders entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Orders published status indicator.
   *
   * Unpublished Orders are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Orders is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Orders.
   *
   * @param bool $published
   *   TRUE to set this Orders to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosOrdersInterface
   *   The called Orders entity.
   */
  public function setPublished($published);

}
