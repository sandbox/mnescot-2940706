<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosCustomerProfilesListBuilder.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Customer Profiles entities.
 *
 * @ingroup pos_entities
 */
class PosCustomerProfilesListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Customer Profiles ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pos_entities\Entity\PosCustomerProfiles */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pos_customer_profiles.edit_form', array(
          'pos_customer_profiles' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
