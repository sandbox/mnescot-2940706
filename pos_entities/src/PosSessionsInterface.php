<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosSessionsInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Sessions entities.
 *
 * @ingroup pos_entities
 */
interface PosSessionsInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Session Order ID.
   *
   * @return string
   *   Order ID of the Session.
   */
  public function getOrderId();

  /**
   * Sets the Session Order ID.
   *
   * @param string $order_id
   *   The Session Order ID.
   *
   * @return \Drupal\pos_entities\PosSessionsInterface
   *   The called Sessions entity.
   */
  public function setOrderId($order_id);

  /**
   * Gets the Session name.
   *
   * @return string
   *   Name of the Session.
   */
  public function getName();

  /**
   * Sets the Session name.
   *
   * @param string $name
   *   The Session name.
   *
   * @return \Drupal\pos_entities\PosSessionInterface
   *   The called Session entity.
   */
  public function setName($name);


  /**
   * Gets the Customer ID.
   *
   * @return int
   *   Customer ID.
   */
  public function getCustomerId();

  /**
   * Sets the Customer ID.
   *
   * @param int $customer_id
   *   Customer ID.
   *
   * @return \Drupal\pos_entities\PosSessionInterface
   *   The called Session entity.
   */
  public function setCustomerId($customer_id);


  /**
   * Gets the Session creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Session.
   */
  public function getCreatedTime();

  /**
   * Sets the Session creation timestamp.
   *
   * @param int $timestamp
   *   The Session creation timestamp.
   *
   * @return \Drupal\pos_entities\PosSessionInterface
   *   The called Session entity.
   */
  public function setCreatedTime($timestamp);


  /**
   * Gets the Session changing timestamp.
   *
   * @return int
   *   Changing timestamp of the Session.
   */
  public function getChangedTime();

  /**
   * Sets the Session changing timestamp.
   *
   * @param int $timestamp
   *   The Session changing timestamp.
   *
   * @return \Drupal\pos_entities\PosSessionInterface
   *   The called Session entity.
   */
  public function setChangedTime($timestamp);


  /**
   * Returns the Session published status indicator.
   *
   * Unpublished Session are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Session is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Session.
   *
   * @param bool $published
   *   TRUE to set this Session to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosSessionInterface
   *   The called Session entity.
   */
  public function setPublished($published);


}
