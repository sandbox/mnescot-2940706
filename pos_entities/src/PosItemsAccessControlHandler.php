<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosItemsAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Items entity.
 *
 * @see \Drupal\pos_entities\Entity\PosItems.
 */
class PosItemsAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosItemsInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished items entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published items entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit items entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete items entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add items entities');
  }

}
