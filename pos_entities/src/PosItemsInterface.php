<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosItemsInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Items entities.
 *
 * @ingroup pos_entities
 */
interface PosItemsInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Items name.
   *
   * @return string
   *   Name of the Items.
   */
  public function getName();

  /**
   * Sets the Items name.
   *
   * @param string $name
   *   The Items name.
   *
   * @return \Drupal\pos_entities\PosItemsInterface
   *   The called Items entity.
   */
  public function setName($name);

  /**
   * Gets the Items creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Items.
   */
  public function getCreatedTime();

  /**
   * Sets the Items creation timestamp.
   *
   * @param int $timestamp
   *   The Items creation timestamp.
   *
   * @return \Drupal\pos_entities\PosItemsInterface
   *   The called Items entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Items published status indicator.
   *
   * Unpublished Items are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Items is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Items.
   *
   * @param bool $published
   *   TRUE to set this Items to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosItemsInterface
   *   The called Items entity.
   */
  public function setPublished($published);

}
