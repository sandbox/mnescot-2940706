<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosOrderStatusAccessControlHandler.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Order Status entity.
 *
 * @see \Drupal\pos_entities\Entity\PosOrderStatus.
 */
class PosOrderStatusAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pos_entities\PosOrderStatusInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished order status entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published order status entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit order status entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete order status entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add order status entities');
  }

}
