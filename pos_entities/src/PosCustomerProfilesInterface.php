<?php

/**
 * @file
 * Contains \Drupal\pos_entities\PosCustomerProfilesInterface.
 */

namespace Drupal\pos_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Customer Profiles entities.
 *
 * @ingroup pos_entities
 */
interface PosCustomerProfilesInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Customer Profiles email.
   *
   * @return string
   *   Name of the Customer Profiles.
   */
  public function getEmail();

  /**
   * Sets the Customer Profiles email.
   *
   * @param string $value
   *   The Customer Profiles email.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setEmail($value);

  /**
   * Gets the Customer Profiles first name.
   *
   * @return string
   *   Name of the Customer Profiles.
   */
  public function getFirstName();

  /**
   * Sets the Customer Profiles first name.
   *
   * @param string $value
   *   The Customer Profiles first name.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setFirstName($value);

  /**
   * Gets the Customer Profiles last name.
   *
   * @return string
   *   Name of the Customer Profiles.
   */
  public function getLastName();

  /**
   * Sets the Customer Profiles last name.
   *
   * @param string $value
   *   The Customer Profiles last name.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setLastName($value);


  /**
   * Gets the Customer Profiles creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Customer Profiles.
   */
  public function getCreatedTime();

  /**
   * Sets the Customer Profiles creation timestamp.
   *
   * @param int $timestamp
   *   The Customer Profiles creation timestamp.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Customer Profiles published status indicator.
   *
   * Unpublished Customer Profiles are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Customer Profiles is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Customer Profiles.
   *
   * @param bool $published
   *   TRUE to set this Customer Profiles to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pos_entities\PosCustomerProfilesInterface
   *   The called Customer Profiles entity.
   */
  public function setPublished($published);

}
