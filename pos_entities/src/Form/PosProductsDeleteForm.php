<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosProductsDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Products entities.
 *
 * @ingroup pos_entities
 */
class PosProductsDeleteForm extends ContentEntityDeleteForm {

}
