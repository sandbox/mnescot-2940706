<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosCustomerProfilesForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal;

/**
 * Form controller for Customer Profiles edit forms.
 *
 * @ingroup pos_entities
 */
class PosCustomerProfilesForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pos_entities\Entity\PosCustomerProfiles */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    if($entity->id()) {
      $form_state->set('profile_id',$entity->id());
    }

    $form['state']['widget'][0]['value']['#type'] = 'select';
    $form['state']['widget'][0]['value']['#options'] = _pos_utils_us_states(TRUE);
    unset($form['state']['widget'][0]['value']['#attributes']);
    unset($form['state']['widget'][0]['value']['#size']);
    unset($form['state']['widget'][0]['value']['#maxlength']);
    $form['email']['widget'][0]['value']['#required'] = TRUE;
    $form['first_name']['widget'][0]['value']['#required'] = TRUE;
    $form['phone']['widget'][0]['value']['#required'] = TRUE;
    $form['#validate'][] = '::customValidateForm';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Customer Profiles.', [
            '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Customer Profiles.', [
            '%label' => $entity->label(),
        ]));
    }
    //    $form_state->setRedirect('entity.pos_customer_profiles.canonical', ['pos_customer_profiles' => $entity->id()]);
    $url = Url::fromUri('internal:/admin/customer-contact-report-search' . '?email=' . $entity->get('email')->value);
    $form_state->setRedirectUrl($url);
  }



  /**
   * {@inheritdoc}
   */
  public function customValidateForm(array &$form, FormStateInterface $form_state)
  {

    parent::validateForm($form, $form_state);
    $email_placeholder = 'na@na.com';

    $profile_id = $form_state->get('profile_id');

    $email = $form_state->getValue('email');
    //Do not check email format if placeholder email is used
    //Do not check if email exists in the system if placeholder email is used

    if ($email['0']['value']!=$email_placeholder) {
      $query = Drupal::service('entity.query')
          ->get('pos_customer_profiles')
          ->condition('email', $email['0']['value']);

      $checking_profile = $query->execute();

      $checking_profile = array_shift($checking_profile);
      if (($profile_id && $checking_profile && $profile_id != $checking_profile) || (!$profile_id && $checking_profile)) {
        $form_state->setErrorByName('email', $this->t("There's already a customer with this email: %email", ['%email' => $email['0']['value']]));
      }

      if (!filter_var($email['0']['value'], FILTER_VALIDATE_EMAIL)) {
        // invalid email address
        $form_state->setErrorByName('email', $this->t('Email Address in invalid format.'));
      }
    }

    $zip = $form_state->getValue('zip');

    if(!empty($zip['0']['value'])) {
      if (!validateUSAZip($zip['0']['value'])) {
        $form_state->setErrorByName('zip', $this->t('Invalid zip. Enter either 5-digit or 9-digit format.'));
      }
    }

    $phone = $form_state->getValue('phone');
    if(!validatePhone($phone['0']['value'])) {
      $form_state->setErrorByName('phone', $this->t('Invalid phone. Enter numeric values only.'));
    }

    $phoneext = $form_state->getValue('phoneext');
    if(!validatePhoneExt($phoneext['0']['value'])) {
      $form_state->setErrorByName('phoneext', $this->t('Invalid Extension. Enter numeric values only.'));
    }


  }
}