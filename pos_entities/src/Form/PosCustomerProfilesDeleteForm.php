<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosCustomerProfilesDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Customer Profiles entities.
 *
 * @ingroup pos_entities
 */
class PosCustomerProfilesDeleteForm extends ContentEntityDeleteForm {

}
