<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosProductsForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\pathauto;
/**
 * Form controller for Products edit forms.
 *
 * @ingroup pos_entities
 */
class PosProductsForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pos_entities\Entity\PosProducts */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    if (!\Drupal::currentUser()->hasPermission('edit stock numbers')) {
      $form['stock']['widget'][0]['value']['#attributes'] = array('readonly' => 'readonly');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    $nid       = $entity->id();
    //remove all speciual characters from the title and replace spaces with underscores
    $title= $entity->get('title')->value;
    $title = iconv('UTF-8', 'ASCII//TRANSLIT', $title);
    $title = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $title);
    $title = strtolower(trim($title, '-'));
    $title = preg_replace("/[\/_| -]+/", '-', $title);
    $alias =
        \Drupal::service('path.alias_manager')
            ->getAliasByPath('/publication/'.$entity->id());
    $new_alias='/publication/'.$title;
    //delete old alias
    $conditions = array('source' => '/publication/'.$entity->id());
    \Drupal::service('path.alias_storage')->delete($conditions);
    //create new alias
    $path      = \Drupal::service('path.alias_storage')->save('/publication/'.$entity->id(), '/publication/'.$title);
    drupal_flush_all_caches();
    
    switch ($status) {
      case SAVED_NEW:

        drupal_set_message($this->t('Created the %label Products.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:


        drupal_set_message($this->t('Saved the %label Products.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.pos_products.canonical', ['pos_products' => $entity->id()]);
  }

}
