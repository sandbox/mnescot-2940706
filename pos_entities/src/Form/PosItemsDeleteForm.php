<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosItemsDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Items entities.
 *
 * @ingroup pos_entities
 */
class PosItemsDeleteForm extends ContentEntityDeleteForm {

}
