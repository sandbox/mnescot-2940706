<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosHistoricDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Historic entities.
 *
 * @ingroup pos_entities
 */
class PosHistoricDeleteForm extends ContentEntityDeleteForm {

}
