<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosOrderStatusDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Order Status entities.
 *
 * @ingroup pos_entities
 */
class PosOrderStatusDeleteForm extends ContentEntityDeleteForm {

}
