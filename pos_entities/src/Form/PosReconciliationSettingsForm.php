<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosReconciliationSettingsForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PosReconciliationSettingsForm.
 *
 * @package Drupal\pos_entities\Form
 *
 * @ingroup pos_entities
 */
class PosReconciliationSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'PosReconciliation_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Defines the settings form for Reconciliation entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['PosReconciliation_settings']['#markup'] = 'Settings form for Reconciliation entities. Manage field settings here.';
    return $form;
  }

}
