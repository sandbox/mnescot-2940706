<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosCartDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cart entities.
 *
 * @ingroup pos_entities
 */
class PosCartDeleteForm extends ContentEntityDeleteForm {

}
