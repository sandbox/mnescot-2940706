<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosOrdersForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Orders edit forms.
 *
 * @ingroup pos_entities
 */
class PosOrdersForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pos_entities\Entity\PosOrders */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Orders.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Orders.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.pos_orders.canonical', ['pos_orders' => $entity->id()]);
  }

}
