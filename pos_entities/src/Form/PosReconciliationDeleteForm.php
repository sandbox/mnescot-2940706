<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosReconciliationDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Reconciliation entities.
 *
 * @ingroup pos_entities
 */
class PosReconciliationDeleteForm extends ContentEntityDeleteForm {

}
