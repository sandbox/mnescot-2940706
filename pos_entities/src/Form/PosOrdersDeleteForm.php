<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosOrdersDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Orders entities.
 *
 * @ingroup pos_entities
 */
class PosOrdersDeleteForm extends ContentEntityDeleteForm {

}
