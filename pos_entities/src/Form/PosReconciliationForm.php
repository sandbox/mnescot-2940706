<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosReconciliationForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Reconciliation edit forms.
 *
 * @ingroup pos_entities
 */
class PosReconciliationForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pos_entities\Entity\PosReconciliation */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Reconciliation.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Reconciliation.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.pos_reconciliation.canonical', ['pos_reconciliation' => $entity->id()]);
  }

}
