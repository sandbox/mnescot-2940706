<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosSessionsDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Sessions entities.
 *
 * @ingroup pos_entities
 */
class PosSessionsDeleteForm extends ContentEntityDeleteForm {

}
