<?php

/**
 * @file
 * Contains \Drupal\pos_entities\Form\PosShippingsDeleteForm.
 */

namespace Drupal\pos_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Shippings entities.
 *
 * @ingroup pos_entities
 */
class PosShippingsDeleteForm extends ContentEntityDeleteForm {

}
