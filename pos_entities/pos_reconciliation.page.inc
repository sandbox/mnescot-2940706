<?php

/**
 * @file
 * Contains pos_reconciliation.page.inc..
 *
 * Page callback for Reconciliation entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Reconciliation templates.
 *
 * Default template: pos_reconciliation.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_reconciliation(array &$variables) {
  // Fetch PosReconciliation Entity Object.
  $pos_reconciliation = $variables['elements']['#pos_reconciliation'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
