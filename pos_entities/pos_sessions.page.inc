<?php

/**
 * @file
 * Contains pos_sessions.page.inc..
 *
 * Page callback for Sessions entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Sessions templates.
 *
 * Default template: pos_sessions.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_sessions(array &$variables) {
  // Fetch PosSessions Entity Object.
  $pos_sessions = $variables['elements']['#pos_sessions'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
