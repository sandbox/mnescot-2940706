<?php

/**
 * @file
 * Contains pos_cart.page.inc..
 *
 * Page callback for Cart entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Cart templates.
 *
 * Default template: pos_cart.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_cart(array &$variables) {
  // Fetch PosCart Entity Object.
  $pos_cart = $variables['elements']['#pos_cart'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
