<?php

/**
 * @file
 * Contains pos_order_status.page.inc..
 *
 * Page callback for Order Status entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Order Status templates.
 *
 * Default template: pos_order_status.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_order_status(array &$variables) {
  // Fetch PosOrderStatus Entity Object.
  $pos_order_status = $variables['elements']['#pos_order_status'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
