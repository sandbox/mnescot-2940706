<?php

/**
 * @file
 * Contains pos_historic.page.inc..
 *
 * Page callback for Historic entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Historic templates.
 *
 * Default template: pos_historic.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pos_historic(array &$variables) {
  // Fetch PosHistoric Entity Object.
  $pos_historic = $variables['elements']['#pos_historic'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
