<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 7/13/17
 * Time: 4:47 PM
 */


/**
 * Implements hook_views_query_alter().
 */
function pos_custom_reports_views_query_alter(Drupal\views\ViewExecutable $view, Drupal\views\Plugin\views\query\Sql $query) {
  if ($view->id() === 'publications_transactions') {
    $table = $query->getTableInfo('pos_products_pos_items');
    $table['join']->type = 'RIGHT';
  }
}

/**
 * Implements hook_preprocess_views_view_field().
 */
function pos_custom_reports_preprocess_views_view_field(&$variables) {
  if (
    $variables['view']->id() == 'publications_transactions' &&
    in_array(@$variables['field']->field, [
      'stock',
      'copy_limit',
      'id',
      'quantity',
    ])
  ) {
    $variables['output'] = str_repeat(chr(32), 12 - strlen($variables['output'])) . $variables['output'];
  }
}
