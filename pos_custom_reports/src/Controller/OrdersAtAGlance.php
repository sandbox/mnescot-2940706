<?php

/**
 * @file
 * Contains \Drupal\pos_custom_reports\Controller\OrdersAtAGlance.
 */

namespace Drupal\pos_custom_reports\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OrdersAtAGlance.
 *
 * @package Drupal\pos_custom_reports\Controller
 */
class OrdersAtAGlance extends ControllerBase {
  /**
   * Content.
   *
   * @return array
   *   Return render array
   */
  public function content() {
    return [
      '#theme' => 'orders_at_a_glance',
      '#cache' => [
            'max-age' => 0, // never
        ],
      '#cart' => \Drupal::service('pos_operations.session')->countSessions(),
      '#checkout' => \Drupal::service('pos_operations.order')->countOrders('checkout'),
      '#canceled' => \Drupal::service('pos_operations.order')->countOrders('canceled'),
      '#bulk' => \Drupal::service('pos_operations.order')->countOrders('bulk'),
      '#pending' => \Drupal::service('pos_operations.order')->countOrders('pending'),
      '#progress' => \Drupal::service('pos_operations.order')->countOrders('progress'),
      '#shipping' => \Drupal::service('pos_operations.order')->countOrders('shipping'),
      '#completed' => \Drupal::service('pos_operations.order')->countOrders('completed'),
      '#returned' => \Drupal::service('pos_operations.order')->countOrders('returned'),
      '#returned_canceled' => \Drupal::service('pos_operations.order')->countOrders('returned_canceled'),
      '#total' => \Drupal::service('pos_operations.order')->countOrders(),
    ];
  }

}
