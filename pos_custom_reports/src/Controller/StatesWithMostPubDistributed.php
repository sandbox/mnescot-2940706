<?php

/**
 * @file
 * Contains \Drupal\pos_custom_reports\Controller\ProductsTransactionsResults.
 */

namespace Drupal\pos_custom_reports\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class StatesWithMostPubDistributed.
 *
 * @package Drupal\pos_custom_reports\Controller
 */
class StatesWithMostPubDistributed extends ControllerBase {
  /**
   * Results.
   *
   * @return array
   *   Return render array.
   */
  public function results() {

    $header = [
        [
            'data' => [
                '#type' => 'markup',
                '#markup' => $this->t('State'),
                '#prefix' => '<div class="ptr-state ptr-align-left">',
                '#suffix' => '</div>',
            ],
        ],
        [
            'data' => [
                '#type' => 'markup',
                '#markup' => $this->t('Quantity'),
                '#prefix' => '<div class="ptr-quantity ptr-align-right">',
                '#suffix' => '</div>',
            ],
        ],
    ];

    $results = \Drupal::service('pos_operations.item')->getReportByState();
    $rows = [];
    foreach ($results as $result) {
      $rows[] = [
          [
              'data' => [
                  '#type' => 'markup',
                  '#markup' => $result['state'],
                  '#prefix' => '<div class="ptr-state ptr-align-left">',
                  '#suffix' => '</div>',
              ],
          ],
          [
              'data' => [
                  '#type' => 'markup',
                  '#markup' => $result['quantity'] ? number_format($result['quantity']) : 0,
                  '#prefix' => '<div class="ptr-quantity ptr-align-right">',
                  '#suffix' => '</div>',
              ],
          ],
      ];
    }
    $arr = getLastMonth(new \DateTime());
    $newtitle="(".$arr[0]." - ".$arr[1].")";
    return [
        '#id' => 'products-transaction-results',
        '#caption' => $this->t($newtitle),
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => ['class' => ['search-ptr']],
    ];
  }

}
