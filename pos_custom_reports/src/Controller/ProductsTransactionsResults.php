<?php

/**
 * @file
 * Contains \Drupal\pos_custom_reports\Controller\ProductsTransactionsResults.
 */

namespace Drupal\pos_custom_reports\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ProductsTransactionsResults.
 *
 * @package Drupal\pos_custom_reports\Controller
 */
class ProductsTransactionsResults extends ControllerBase {
  /**
   * Results.
   *
   * @return array
   *   Return render array.
   */
  public function results($date_from = NULL, $date_to = NULL, $sku = NULL, $title = NULL) {

    $header = [
      [
        'data' => [
          '#type' => 'markup',
          '#markup' => $this->t('SKU'),
          '#prefix' => '<div class="ptr-sku ptr-align-left">',
          '#suffix' => '</div>',
        ],
      ],
      [
        'data' => [
          '#type' => 'markup',
          '#markup' => $this->t('Title'),
          '#prefix' => '<div class="ptr-title ptr-align-left">',
          '#suffix' => '</div>',
        ],
      ],
      [
        'data' => [
          '#type' => 'markup',
          '#markup' => $this->t('Stock'),
          '#prefix' => '<div class="ptr-stock ptr-align-right">',
          '#suffix' => '</div>',
        ],
      ],
      [
        'data' => [
          '#type' => 'markup',
          '#markup' => $this->t('Copy Limit'),
          '#prefix' => '<div class="ptr-copy-limi ptr-align-right">',
          '#suffix' => '</div>',
        ],
      ],
      [
        'data' => [
          '#type' => 'markup',
          '#markup' => $this->t('Transactions'),
          '#prefix' => '<div class="ptr-transactions ptr-align-right">',
          '#suffix' => '</div>',
        ],
      ],
      [
        'data' => [
          '#type' => 'markup',
          '#markup' => $this->t('Quantity'),
          '#prefix' => '<div class="ptr-quantity ptr-align-right">',
          '#suffix' => '</div>',
        ],
      ],
    ];

    $results = \Drupal::service('pos_operations.product')->getProductsTransaction($date_from, $date_to, $sku, $title);
    $rows = [];
    foreach ($results as $result) {
      $rows[] = [
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => $result['sku'],
            '#prefix' => '<div class="ptr-sku ptr-align-left">',
            '#suffix' => '</div>',
          ],
        ],
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => $result['title'],
            '#prefix' => '<div class="ptr-title ptr-align-left">',
            '#suffix' => '</div>',
          ],
        ],
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => number_format($result['stock']),
            '#prefix' => '<div class="ptr-stock ptr-align-right">',
            '#suffix' => '</div>',
          ],
        ],
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => number_format($result['copy_limit']),
            '#prefix' => '<div class="ptr-copy-limi ptr-align-right">',
            '#suffix' => '</div>',
          ],
        ],
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => number_format($result['transactions']),
            '#prefix' => '<div class="ptr-transactions ptr-align-right">',
            '#suffix' => '</div>',
          ],
        ],
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => $result['quantity'] ? number_format($result['quantity']) : 0,
            '#prefix' => '<div class="ptr-quantity ptr-align-right">',
            '#suffix' => '</div>',
          ],
        ],
      ];
    }

    return [
      '#id' => 'products-transaction-results',
      '#caption' => $this->t('Results'),
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['search-ptr']],
    ];
  }

}
