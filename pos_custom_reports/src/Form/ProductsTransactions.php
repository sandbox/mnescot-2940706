<?php

/**
 * @file
 * Contains \Drupal\pos_custom_reports\Form\ProductsTransactions.
 */

namespace Drupal\pos_custom_reports\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\pos_custom_reports\Controller\ProductsTransactionsResults;

/**
 * Class ProductsTransactions.
 *
 * @package Drupal\pos_custom_reports\Form
 */
class ProductsTransactions extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'products_transactions';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = $this->t('Publications Transactions');

    $form['custom_title'] = array(
      '#type' => 'markup',
      '#markup' => $this->t('Publications Transactions'),
      '#prefix' => '<h2>',
      '#sufix' => '</h2>',
      '#weight' => 0,
    );

    $form['from'] = array(
      '#id' => 'pt-from',
      '#type' => 'date',
      '#title' => $this->t('From'),
      '#weight' => 10,
    );
    $form['to'] = array(
      '#id' => 'pt-to',
      '#type' => 'date',
      '#title' => $this->t('To'),
      '#weight' => 15,
    );
    $form['sku'] = array(
      '#id' => 'pt-sku',
      '#type' => 'textfield',
      '#title' => $this->t('SKU'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => 20,
    );
    $form['title'] = array(
      '#id' => 'pt-title',
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => 25,
    );

    $form['search'] = array(
      '#type' => 'button',
      '#name' => 'search',
      '#value' => $this->t("Search"),
      '#ajax' => array(
        'callback' => array($this, 'searchCallback'),
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => t("Pulling data..."),
        ),
      ),
      '#weight' => 50,
    );

    $form['reset'] = array(
      '#name' => 'reset',
      '#type' => 'button',
      '#value' => $this->t("Reset"),
      '#weight' => 55,
      '#ajax' => array(
        'callback' => array($this, 'resetFormCallback'),
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => t("Resetting..."),
        ),
      ),
    );

    $results = $form_state->getUserInput() ? NULL : $this->getResults($form_state);
    $form['results'] = array(
      '#type' => 'markup',
      '#markup' => $results,
      '#prefix' => '<div id="products-transaction-results">',
      '#sufix' => '</div>',
      '#weight' => 90,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax callback for "Search".
   *
   * @param array $form
   *   The $form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */
  public function searchCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $results = $this->getResults($form_state);
    $response->addCommand(new InvokeCommand('#products-transaction-results', 'html', array($results)));
    return $response;
  }

  /**
   * Performs the search and formats the result.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return string
   *   Markup with the results;
   */
  public function getResults(FormStateInterface $form_state) {
    $date_from = strtotime($form_state->getValue('from'));
    $date_to = strtotime($form_state->getValue('to'));
    $sku = $form_state->getValue('sku');
    $title = $form_state->getValue('title');
    $controller = new ProductsTransactionsResults();
    $results = $controller->results($date_from, $date_to, $sku, $title);
    $output = \Drupal::service('renderer')->render($results);
    return $output;

//  use Drupal\views\Views;
//    $view = Views::getView('publications_transactions');
//    $view->setDisplay('block_1');
//    $output = \Drupal::service('renderer')->render($view->render());
//    return $output;
  }

  /**
   * Ajax callback for clearing the form fields.
   *
   * @param array $form
   *   The $form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */
  public function resetFormCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand('#pt-from', 'val', array(NULL)));
    $response->addCommand(new InvokeCommand('#pt-to', 'val', array(NULL)));
    $response->addCommand(new InvokeCommand('#pt-sku', 'val', array(NULL)));
    $response->addCommand(new InvokeCommand('#pt-title', 'val', array(NULL)));
    return $response;
  }

}
