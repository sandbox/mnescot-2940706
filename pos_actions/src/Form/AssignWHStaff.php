<?php

/**
 * Confirm Assignment
 * User: hablat@jbsinternational.com
 * Date: 12/6/2017
 * Time: 10:01 PM
 */

namespace Drupal\pos_actions\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AssignWHStaff extends ConfirmFormBase {

  /**
   * The array of orders to assign.
   *
   * @var string[][]
   */
  protected $orderInfo = [];

  /**
   * The tempstore factory.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;


  /**
   * Entity type manager
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface|\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $manager;

  /**
   * The form id
   *
   * @var string
   */
  private $id = 'pos_orders_assign_wh_staff_confirm';

  /**
   * ID of order operations service
   *
   * @var string
   */
  private $order_ops_service_id = 'pos_operations.order';

  /**
   * Role name for warehouse Staff
   *
   * @var string
   */
  private $wh_staff_role = 'warehouse_staff';

  /**
   * Route name of return page
   *
   * @var string
   */
  private $return_page = 'view.pending_orders.page_1';

  /**
   * Constructs a AssignWHStaff form object.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface|\Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity manager.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return $this->id;
  }

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->orderInfo), 'Which warehouse staff do you want to assign this order to?', 'Which warehouse staff do you want to assign these orders to');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    // return to pending orders view
    return Url::fromRoute($this->return_page);
  }

  public function getConfirmText() {
    return t('Assign');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // get orders
    $this->orderInfo = $this->tempStoreFactory->get($this->id)->get(\Drupal::currentUser()->id());
    if (empty($this->orderInfo)) {
      return new RedirectResponse($this->getCancelUrl()->setAbsolute()->toString());
    }

    // stores order items to display
    $items = [];

    // go through each order and add order item for form to display
    foreach (array_keys($this->orderInfo) as $id) {
      $items[$id] = 'Order ' . $id;
    }

    // get warehouse staff users
    $user_properties = [
      'status' => 1,
      'roles' => [$this->wh_staff_role]
    ];
    $users = $this->manager->getStorage('user')->loadByProperties($user_properties);

    // stores user options
    $user_options = [];

    // add warehouse staff users to user options
    foreach ($users as $id => $user) {
      if ($user->hasField('field_alias') && trim($user->get('field_alias')->value) != '') {
        $user_options[$id] = $user->get('field_alias')->value . ' <' . $user->get('mail')->value . '>';
      } else {
        $user_options[$id] = $user->get('name')->value . ' <' . $user->get('mail')->value . '>';
      }
    }

    $form['wh_staff'] = [
      '#type' => 'select',
      '#title' => t('Assign to'),
      '#options' => $user_options,
      '#required' => TRUE
    ];

    $form['orders'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('confirm') && !empty($this->orderInfo)) {
      $batch_owner_id = $form_state->getValue('wh_staff');

      foreach (array_keys($this->orderInfo) as $id) {
        // assign order to selected warehouse staff
        \Drupal::service($this->order_ops_service_id)->setOrderStatusToInProgress($id, $batch_owner_id);
      }
    }

    $form_state->setRedirect($this->return_page);
  }
}