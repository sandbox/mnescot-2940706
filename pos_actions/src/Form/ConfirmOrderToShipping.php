<?php

/**
 * Confirm Set order to shipping
 * User: hablat@jbsinternational.com
 * Date: 12/6/2017
 * Time: 10:01 PM
 */

namespace Drupal\pos_actions\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ConfirmOrderToShipping extends ConfirmFormBase {

  /**
   * The array of orders to assign.
   *
   * @var string[][]
   */
  protected $orderInfo = [];

  /**
   * The tempstore factory.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;


  /**
   * Entity type manager
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface|\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $manager;

  /**
   * The form id
   *
   * @var string
   */
  private $id = 'confirm_order_to_shipping';

  /**
   * ID of order operations service
   *
   * @var string
   */
  private $order_ops_service_id = 'pos_operations.order';

  /**
   * Route name of return page
   *
   * @var string
   */
  private $return_page = 'view.in_progress_orders.page_1';

  /**
   * The status to move to
   *
   * @var string
   */
  private $move_to_status = 'shipping';

  /**
   * The label for shipping status
   *
   * @var string
   */
  protected $status_label = '';

  /**
   * Constructs a ConfirmOrderToShipping form object.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface|\Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity manager.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->manager = $manager;
    $this->status_label = \Drupal::service('pos_operations.status')->getStatusName($this->move_to_status);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return $this->id;
  }

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->orderInfo), 'Please confirm the following order will be set to ' . $this->status_label . ':', 'Please confirm the following orders will be set to ' . $this->status_label . ':');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    // return to pending orders view
    return Url::fromRoute($this->return_page);
  }

  public function getConfirmText() {
    return t('Set to ' . $this->status_label);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // get orders
    $this->orderInfo = $this->tempStoreFactory->get($this->id)->get(\Drupal::currentUser()->id());
    if (empty($this->orderInfo)) {
      return new RedirectResponse($this->getCancelUrl()->setAbsolute()->toString());
    }

    // stores order items to display
    $items = [];

    // go through each order and add order item for form to display
    foreach (array_keys($this->orderInfo) as $id) {
      $items[$id] = 'Order ' . $id;
    }

    $form['orders'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('confirm') && !empty($this->orderInfo)) {
      // change status of order
      \Drupal::service($this->order_ops_service_id)->massStatusSetting($this->move_to_status, array_keys($this->orderInfo));
    }

    $form_state->setRedirect($this->return_page);
  }
}