<?php
/**
 * Prints label for order(s).
 *
 * @Action(
 *   id = "pos_actions_print_order_label",
 *   label = @Translation("Print label for order(s)"),
 *   type = "pos_orders"
 * )
 */

namespace Drupal\pos_actions\Plugin\Action;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

class PrintOrderLabel extends ActionBase {

  /**
   * Checks object access.
   *
   * @param mixed $object
   *   The object to execute the action on.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user for which to check access, or NULL to check access
   *   for the current user. Defaults to NULL.
   * @param bool $return_as_object
   *   (optional) Defaults to FALSE.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result. Returns a boolean if $return_as_object is FALSE (this
   *   is the default) and otherwise an AccessResultInterface object.
   *   When a boolean is returned, the result of AccessInterface::isAllowed() is
   *   returned, i.e. TRUE means access is explicitly allowed, FALSE means
   *   access is either explicitly forbidden or "no opinion".
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $object->access('update', $account, $return_as_object);
  }

  /**
   * @param array $entities
   */
  public function executeMultiple(array $entities) {
    // holds the IDs for our orders
    $order_ids = [];

    // get order ids of selected orders
    foreach ($entities as $order) {
      $order_ids[] = $order->id();
    }

    // print the label of selected orders
    \Drupal::service('pos_operations.generate_pdf')->labels($order_ids);
  }

  /**
   * Executes the plugin.
   */
  public function execute($entity =  null) {
    $this->executeMultiple([$entity]);
  }
}