<?php

/**
 * @file
 * Contains \Drupal\pos_blocks\Plugin\Block\AddProduct.
 */

namespace Drupal\pos_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'AddProduct' block.
 *
 * @Block(
 *  id = "add_product",
 *  admin_label = @Translation("Add product"),
 * )
 */
class AddProduct extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $path = \Drupal::service('path.current')->getPath();
    $url = Url::fromRoute('entity.pos_products.add_form', ['destination' => $path]);
    $url->setOptions([
      'attributes' => [
        'class' => ['button button-action button--primary button--small'],
      ],
    ]);
    $build = array(
      '#title' => '',
      '#theme' => 'add_product_button_block',
      '#add_entity_link' => Link::fromTextAndUrl(t('Add Product'), $url)->toString(),
      '#cache' => ['max-age' => 0],
    );
    return $build;
  }

}
