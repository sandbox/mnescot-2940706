<?php

/**
 * @file
 * Contains \Drupal\pos_blocks\Plugin\Block\ShowCartLink.
 */

namespace Drupal\pos_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'Show Cart Link' block.
 *
 * @Block(
 *  id = "show_cart_link",
 *  admin_label = @Translation("Show Cart Link"),
 * )
 */
class ShowCartLink extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = Url::fromRoute('products.cart_list');
    $url->setOptions([
      'attributes' => [
        'class' => ['button button-action button--primary button--small'],
      ],
    ]);
    $build = array(
      '#title' => '',
      '#theme' => 'show_cart_button_block',
      '#items_count' => \Drupal::service('pos_operations.cart_item')->itemsCount($review = FALSE),
      '#show_cart_link' => Link::fromTextAndUrl(t('Show Cart'), $url)->toString(),
      '#cache' => ['max-age' => 0],
    );
    return $build;
  }

}
