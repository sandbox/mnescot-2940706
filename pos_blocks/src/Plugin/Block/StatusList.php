<?php

/**
 * @file
 * Contains \Drupal\pos_blocks\Plugin\Block\StatusList.
 */

namespace Drupal\pos_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'Status List' block.
 *
 * @Block(
 *  id = "status)list",
 *  admin_label = @Translation("Status List"),
 * )
 */
class StatusList extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $status_list = \Drupal::service('pos_operations.status')->getStatusList();
    $first_element = [
      'id' => '',
      'name' => 'Any Status',
    ];
    array_unshift($status_list, $first_element);
    foreach ($status_list as &$status) {
      switch ($status['id']) {
        case 'progress':
          $url = URL::fromUserInput('/admin/in-progress-orders');
          break;

        case 'pending':
          $url = URL::fromUserInput('/admin/pending-orders');
          break;

        case 'returned':
          $url = URL::fromUserInput('/returned-orders');
          break;

        case 'shipping':
          $url = URL::fromUserInput('/ready-for-shipping-orders');
          break;

        default:
          $url = URL::fromUserInput('/order-status/' . $status['id']);

      }
      $status['link'] = Link::fromTextAndUrl($status['name'], $url)->toString();
    }
    $build = array(
      '#title' => $this->t("Order's Flow"),
      '#theme' => 'status_list_block',
      '#status_list' => $status_list,
    );
    return $build;
  }

}
