<?php

/**
 * @file
 * Contains \Drupal\pos_blocks\Plugin\Block\AtAGlance.
 */

namespace Drupal\pos_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\pos_custom_reports\Controller\OrdersAtAGlance;

/**
 * Provides a 'OrdersAtAGlance' block.
 *
 * @Block(
 *  id = "orders_at_a_glance_block",
 *  admin_label = @Translation("Orders at a Glance"),
 * )
 */
class AtAGlance extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $controller = new OrdersAtAGlance();
    return $controller->content();
  }

}
