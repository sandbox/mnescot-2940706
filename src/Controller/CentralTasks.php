<?php

/**
 * @file
 * Contains \Drupal\publications_ordering_system\Controller\CentralTasks.
 */

namespace Drupal\publications_ordering_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class CentralTasks.
 *
 * @package Drupal\publications_ordering_system\Controller
 */
class CentralTasks extends ControllerBase {
  /**
   * Content.
   *
   * @return array
   *   Return render array.
   */
  public function content() {

    $build = array();

    $build[] = array(
      '#type' => 'markup',
      '#markup' => '<ul class="admin-list">',
    );
    $menu_tree = $this->getPosCentralSubTree();
    foreach ($menu_tree as $item) {
      $plugin_definition = $item->link->getPluginDefinition();
      $url = Url::fromRoute($plugin_definition['route_name']);
      $link = Link::fromTextAndUrl($plugin_definition['title'], $url)->toString();
      $build[] = array(
        '#type' => 'markup',
        '#markup' => '<li>' .
        $link .
        '<div class="description">' . $plugin_definition['description'] .
        '</div></li>',
      );
    }



    $build[] = array(
      '#type' => 'markup',
      '#markup' => '</ul>',
    );

    return $build;

  }

  private function getPosCentralSubTree() {
    $result = array();
    $menu_tree = \Drupal::service('toolbar.menu_tree');
    $parameters = new MenuTreeParameters();
    $parameters->setMinDepth(2)->setMaxDepth(4)->onlyEnabledLinks();
    // @todo Make the menu configurable in https://www.drupal.org/node/1869638.
    $tree = $menu_tree->load('admin', $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
      array('callable' => 'toolbar_menu_navigation_links'),
    );
    $tree = $menu_tree->transform($tree, $manipulators);
    foreach ($tree as $sub_tree) {
      if ($sub_tree->link->getPluginId() == 'system.admin_structure') {
        foreach ($sub_tree->subtree as $sub_sub_tree) {
          if ($sub_sub_tree->link->getPluginId() == 'pos.central') {
            $result = $sub_sub_tree->subtree;
            break;
          }
        }
        break;
      }
    }
    return $result;
  }

}
