<?php

/**
 * @file
 * Contains \Drupal\pos_publications_page\Controller\PublicationPage.
 */

namespace Drupal\pos_publications_page\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PublicationPage.
 *
 * @package Drupal\pos_publications_page\Controller
 */
class PublicationPage extends ControllerBase {

  /**
   * Content.
   *
   * @return array
   *   Return render array.
   */
  public function content($id) {
    $output = array(
      '#cache' => ['max-age' => 0],
      '#theme' => 'publication_page',
      '#publication' => $this->getPublicationInfo($id),
      '#attached' => array(
        'library' => array('publications_ordering_system/pos_scripts'),
      ),
    );
    return $output;
  }

  /**
   * Get publication,s info and store it in an array to be sent to template.
   *
   * @param int $id
   *   Product/publication id.
   *
   * @return array
   *   Values to be sent to template.
   */
  private function getPublicationInfo($id) {
    $publication = \Drupal::service('pos_operations.product')->getProduct($id);
    $is_authenticated_user = (bool) \Drupal::currentUser()->id();

    if ($publication && ($publication['active'] || $is_authenticated_user)) {

      // Top wrapper.
      $image_info = $this->getFileInfo($publication['image']['target_id']);
      $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('publication_cover');
      $publication['image']['url'] = $style->buildUrl($image_info['uri']);
      if ($publication['topic']) {

        $term = Term::load($publication['topic']);
        $topic = $term->getName();
        $path = "";
        if (!$term->get('field_path_to_view_all_pubs')->isEmpty()) {
          $path = Term::load($publication['topic'])->get('field_path_to_view_all_pubs')->value;
        }

        $topic_url = URL::fromUserInput('/' . $path);
        $publication['topic'] = [
          'tid' => $publication['topic'],
          'name' => $topic,
          'link' => Link::fromTextAndUrl($topic, $topic_url)->toString(),
        ];
      }
      // Bottom Wrapper.
      // Spanish/English version.
      if ($publication['link_other_language']['uri']) {
        $other_language_url = URL::fromUri($publication['link_other_language']['uri']);
        $other_language_name = $publication['other_language'] == 2 ? 'Spanish Version' : 'English Version';
        $publication['link_other_language'] = $this->getLinkWithSpan($other_language_name, $other_language_url, 'btn-spanish');
      }
      else {
        $publication['link_other_language'] = chr(32);
      }
      // PDF.
      if ($publication['file']['target_id']) {
        $this->getFileInfo($publication['file']['target_id']);
        $file_info = $this->getFileInfo($publication['file']['target_id']);
        $file_url = URL::fromUri($file_info['url']);
        $file_size = _pos_utils_format_bytes($file_info['size']);
        $file_text = 'PDF (' . $file_size . ')';
        $publication['link_document'] = $this->getLinkWithSpan($file_text, $file_url, 'btn-pdf');
      }
      else {
        $publication['link_document'] = chr(32);
      }
      // Paper Copy or Out of Stock.
      if ($publication['stock']) {
        $paper_copy_url = Url::fromRoute('products.add_to_cart', [
          'destination' => \Drupal::service('path.current')->getPath(),
          'product_id' => $publication['id'],
          'title' => $publication['title'],
          'copy_limit' => $publication['copy_limit'],
          'sku' => $publication['sku'],
        ]);
        $publication['link_paper_copy'] = $this->getLinkWithSpan('Paper Copy', $paper_copy_url, 'btn-paper-copy', TRUE);
        $publication['out_of_stock'] = NULL;
      }
      else {
        $publication['link_paper_copy'] = NULL;
        $publication['out_of_stock'] = $this->getMessageOutOfStock();
      }

      // Related publications.
      $view = Views::getView('related_publications');
      $view->setDisplay('block_related_publications');
      $view->setArguments(array($id, $publication['topic']['tid']));
      $publication['related_publications']  = $view->render();
    }
    else {
      throw new NotFoundHttpException();
    }
    return $publication;
  }

  /**
   * Gets the URL and the size of a file.
   *
   * @param int $fid
   *   Managed file ID.
   *
   * @return array
   *   Associative array with URL and the size values.
   */
  private function getFileInfo($fid) {
    $file = File::load($fid);
    if (isset($file)) {
      $result = [
        'uri' => $file->getFileUri(),
        'url' => file_create_url($file->getFileUri()),
        'size' => $file->getSize(),
      ];
      return $result;
    }
    else {
      return NULL;
    }
  }

  /**
   * Generate a link with a <span>Get</span> added to the text.
   *
   * @param string $text
   *   Text of the link.
   * @param string $url
   *   URL of the link.
   * @param string $class
   *   Classes to be added to the tag.
   * @param bool $modal
   *   Whether is a link to a modal form.
   *
   * @return string
   *   Raw link.
   */
  private function getLinkWithSpan($text, $url, $class = '', $modal = FALSE) {
    $classes = explode(chr(32), $class);
    $classes[] = 'btn_get';
    if (!$modal) {
      $url->setOptions([
        'attributes' => [
          'class' => $classes,
        ],
      ]);
    }
    else {
      $classes[] = 'use-ajax';
      $url->setOptions([
        'attributes' => [
          'class' => $classes,
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 400]),
        ],
      ]);
    }
    $link = Link::fromTextAndUrl($text, $url)->toString()->getGeneratedLink();
    $link = str_replace('</a>', '<span>Get</span></a>', $link);
    return $link;
  }

  /**
   * Gets the message for "out of stock".
   */
  private function getMessageOutOfStock() {
    $default_config = \Drupal::config('pos_operations.settings');
    $message = $default_config->get('messages.out_of_stock.publications_page');
    return $message;
  }

  /**
   * Returns a page title.
   */
  public function getTitle() {
    $title = 'Publication';
    $request = \Drupal::request();
    if ($request->attributes) {
      if ($publication = \Drupal::service('pos_operations.product')->getProduct($request->attributes->get('id'))) {
        $title = $publication['title'];
      }
    }

    return $title;
}

}
