<?php

/**
 * @file
 * Contains \Drupal\pos_profile_management\Form\CustomerProfile.
 */

namespace Drupal\pos_profile_management\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pos_entities\Entity\PosCustomerProfiles;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal;

/**
 * Class CustomerProfile.
 *
 * @package Drupal\pos_profile_management\Form
 */
class CustomerProfile extends FormBase {

  use \Drupal\pos_forms\Form\ValidateAndSave;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->entityTitle = 'Customer Profile';
    $this->redirectRoute = 'customer.profile';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'customer_profile';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $profile_id = NULL) {

    $form['email'] = array(
      '#id' => 'edit-email',
      '#type' => 'textfield',
      '#field_name' => 'email',
      '#title' => $this->t('Customer email'),
      '#maxlength' => 254,
      '#weight' => 10,
      '#required' => TRUE,
    );

    $form['first_name'] = array(
      '#id' => 'edit-first-name',
      '#type' => 'textfield',
      '#field_name' => 'first_name',
      '#title' => $this->t('First name'),
      '#maxlength' => 50,
      '#weight' => 15,
    );

    $form['last_name'] = array(
      '#id' => 'edit-last-name',
      '#type' => 'textfield',
      '#field_name' => 'last_name',
      '#title' => $this->t('Last name'),
      '#maxlength' => 50,
      '#weight' => 20,
    );

    $form['honorific'] = array(
      '#id' => 'edit-honorific',
      '#type' => 'select',
      '#field_name' => 'honorific',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_honorific'),
      '#title' => $this->t('Title'),
      '#weight' => 25,
    );

    $form['phone'] = array(
        '#id' => 'edit-phone',
        '#type' => 'textfield',
        '#field_name' => 'phone',
        '#title' => $this->t('Phone'),
        '#maxlength' => 50,
        '#weight' => 30,
    );

    $form['phoneext'] = array(
        '#id' => 'edit-phoneext',
        '#type' => 'textfield',
        '#field_name' => 'phoneext',
        '#title' => $this->t('Extension'),
        '#maxlength' => 6,
        '#weight' => 32,
    );

    $form['organization'] = array(
      '#id' => 'edit-organization',
      '#type' => 'textfield',
      '#field_name' => 'organization',
      '#title' => $this->t('Organization'),
      '#maxlength' => 125,
      '#weight' => 35,
    );

    $form['address_1'] = array(
      '#id' => 'edit-address-1',
      '#type' => 'textfield',
      '#field_name' => 'address_1',
      '#title' => $this->t('Address 1'),
      '#maxlength' => 254,
      '#weight' => 40,
    );

    $form['address_2'] = array(
      '#id' => 'edit-address-2',
      '#type' => 'textfield',
      '#field_name' => 'address_2',
      '#title' => $this->t('Address 2'),
      '#maxlength' => 254,
      '#weight' => 45,
    );

    $form['city'] = array(
      '#id' => 'edit-city',
      '#type' => 'textfield',
      '#field_name' => 'city',
      '#title' => $this->t('City'),
      '#maxlength' => 60,
      '#weight' => 50,
    );

    $form['state'] = array(
      '#id' => 'edit-state',
      '#type' => 'select',
      '#field_name' => 'state',
      '#options' => _pos_utils_us_states(),
      '#title' => $this->t('State'),
      '#weight' => 55,
    );

    $form['zip'] = array(
      '#id' => 'edit-zip',
      '#type' => 'textfield',
      '#field_name' => 'zip',
      '#title' => $this->t('Zip'),
      '#maxlength' => 15,
      '#weight' => 60,
    );

    $form['how_heard'] = array(
      '#id' => 'edit-how-heard',
      '#type' => 'select',
      '#field_name' => 'how_heard',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_how_heard'),
      '#title' => $this->t('How heard'),
      '#weight' => 65,
    );

    if ($profile_id) {
      $form_state->set('profile_id', $profile_id);
      $this->entity = PosCustomerProfiles::load($profile_id);
      foreach ($form as &$item) {
        if (isset($item['#field_name'])) {
          $item['#value'] = $this->entity->getValue($item['#field_name']);
        }
      }
      $url = Url::fromRoute('customer.profile.delete', [
        'profile_id' => $this->entity->id(),
        'email' => $this->entity->getEmail(),
        'first_name' => $this->entity->getFirstName(),
        'last_name' => $this->entity->getLastName(),
      ]);
      $url->setOptions([
        'attributes' => [
          'class' => [
            'use-ajax',
            'button',
            'button--small',
            'button-add-to-cart',
          ],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 400]),
        ],
      ]);
      $delete_button = array(
        '#type' => 'link',
        '#title' => $this->t('Delete profile'),
        '#url' => $url,
        '#weight' => 20,
      );
    }
    else {
      $this->entity = PosCustomerProfiles::create();
      $delete_button = NULL;
    }

    $form['actions'] = array(
      '#weight' => 90,
      'submit' => array(
        '#type' => 'submit',
        '#value' => $this->t('Save profile'),
        '#weight' => 10,
      ),
      'delete' => $delete_button,
    );

    //$form['#validate'][] = '::validateForm';
    //$form['#validate'][] = '::customValidateForm';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->save($form, $form_state);
    $route = \Drupal::routeMatch()->getRouteName();
    if ($route == 'customer.profile.admin') {
      $url = Url::fromUri('internal:/admin/customer-contact-report-search' . '?email=' . $entity->get('email')->value);
      $form_state->setRedirectUrl($url);
    }
  }

  /**
   * {@inheritdoc}
   */
  /*
  public function customValidateForm(array &$form, FormStateInterface $form_state) {
    $profile_id = $form_state->get('profile_id');
    $email = $form_state->getValue('email');
    $query = Drupal::service('entity.query')
      ->get('pos_customer_profiles')
      ->condition('email', $email);
    $checking_profile = $query->execute();
    $checking_profile = array_shift($checking_profile);
    if (($profile_id && $checking_profile && $profile_id != $checking_profile) || (!$profile_id && $checking_profile)) {
      $form_state->setErrorByName('email', $this->t("There's already a customer with this email: %email", ['%email' => $email]));
    }
  }
  */


}
