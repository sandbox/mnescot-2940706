<?php

/**
 * @file
 * Contains \Drupal\pos_profile_management\Form\DeleteCustomerProfile.
 */

namespace Drupal\pos_profile_management\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pos_entities\Entity\PosCustomerProfiles;

/**
 * Class DeleteCustomerProfile.
 *
 * @package Drupal\pos_profile_management\Form
 */
class DeleteCustomerProfile extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_customer_profile';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $param = \Drupal::request()->query->all();
    $form['profile_id'] = array(
      '#type' => 'hidden',
      '#value' => $param['profile_id'],
    );
    $form['email'] = array(
      '#type' => 'item',
      '#markup' => $param['email'],
      '#value' => $param['email'],
    );
    $form['name'] = array(
      '#type' => 'item',
      '#markup' => $param['first_name'] . chr(32) . $param['last_name'],
      '#value' => $param['first_name'] . chr(32) . $param['last_name'],
    );
    $form['confirm'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Remove'),
      '#description' => $this->t('Please confirm the removal of this profile'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($entity = PosCustomerProfiles::load($form_state->getValue('profile_id'))) {
      $entity->delete();
      drupal_set_message($this->t('Removed the %label Customer Profile.', [
        '%label' => $form_state->getValue('email') . ' - ' . $form_state->getValue('name'),
      ]));
    }
    $form_state->setRedirect('customer.profile');
  }

}
