<?php

/**
 * @file
 * Contains \Drupal\pos_profile_management\Controller\OrdersByProfile.
 */

namespace Drupal\pos_profile_management\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OrdersByProfile.
 *
 * @package Drupal\pos_profile_management\Controller
 */
class OrdersByProfile extends ControllerBase {
  /**
   * Content.
   *
   * @return array
   *   Return render array.
   */
  public function content($profile_id) {
    if ($orders = \Drupal::service('pos_operations.order')->getOrderByProfile($profile_id)) {
      $build = [
        '#id' => 'orders-by-profile-table',
        '#type' => 'table',
        '#title' => 'Orders',
        '#header' => [
          'Order ID',
          'Created',
          'Changed',
          'Status',
        ],
        '#rows' => $orders,
        '#prefix' => '<div class="field__label pos-table-label">Orders</div>',
      ];
    }
    else {
      $build = [
        '#type' => 'markup',
        '#markup' => '<div><strong>' . $this->t('There are no Orders for this customer') . '</strong></div>',
      ];
    }
    return $build;
  }

}
