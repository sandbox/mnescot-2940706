<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 6/16/17
 * Time: 10:34 PM
 */

namespace Drupal\pos_pdf_printing;


/**
 * Class PosFPDF.
 *
 * Overrides some empty functions in \FPDF, like Header() and Footer().
 *
 * @package Drupal\pos_pdf_printing
 */
class PosFPDF extends \FPDF {

  private $contentSection = NULL;
  private $yTopItems = 0;
  private $headerText = [
    "NIA CLEARINGHOUSES",
    "P.O. Box 8057",
    "Gaithersburg, MD 20898-8057",
    "Telephone: 1-800-222-2225",
    "FAX: 1-301-589-3014",
    "Email: niaic@nia.nih.gov",
    "Your aging and Alzheimer's information resource",
  ];
  private $infoText = 'The enclosed materials are provided in response for your request for information.';
  private $addressLineSteps = 7;
  private $order = NULL;
  private $previousOrderId;
  private $isReturnedOrder = FALSE;

  /**
   * Set content section.
   *
   * @param string $value
   *   Value to be set.
   */
  public function setContentSection($value) {
    $this->contentSection = $value;
  }

  /**
   * Set Order values.
   *
   * @param string $value
   *   Value to be set.
   */
  public function setOrder($value) {
    $this->order = $value;
  }

  /**
   * Set the flag indicating whether the Order ia a returned one..
   *
   * @param string $value
   *   Value to be set.
   */
  public function setIsReturnedOrder($value) {
    $this->isReturnedOrder = $value;
  }

  /**
   * Get yTopItems.
   *
   * @return float
   *   yTopItems value.
   */
  public function getYTopItems() {
    return $this->yTopItems;
  }

  /**
   * {@inheritdoc}
   */
  function Header() {
    $file = drupal_get_path('module', 'pos_pdf_printing') . '/images/nih_logo.png';
    $x = $this->GetX();
    $y = $this->GetY();
    $this->Image($file, $x, $y, 25);
    $this->SetFont('Arial', '', 16);
    foreach ($this->headerText as $line) {
      $this->Cell(0, 10, $line, 0, 1, 'C');
      $y = $this->GetY();
      $this->SetY($y - 3);
    }
    $this->SetY($this->GetY() + 10);
    if ($this->contentSection == 'cover') {
      $y = $this->GetY();
      $page_width = $this->GetPageWidth();
      $this->setX(0);
      if ($this->isReturnedOrder) {
        $this->SetFont('Arial', 'B', 30);
        $this->Cell($page_width, 10, 'Invoices - Returned Orders', 0, 0, 'C');
      }
      else {
        $this->SetFont('Arial', '', 30);
        $this->Cell($page_width, 10, 'Invoices', 0, 0, 'C');
      }
      $y += 15;
      $this->SetY($y);
      $this->SetFont('Arial', '', 12);
      $margin = ($page_width - 150) / 2;
      $this->setX($margin);
      $this->Cell(7, 10, '', 0, 0, 'C');
      $this->Cell(25, 10, 'Order ID', 0, 0, 'C');
      $this->Cell(57, 10, 'Name', 0, 0, 'L');
      $this->Cell(60, 10, 'Batched', 0, 1, 'C');
      $y += 9;
      $this->Line($margin, $y, $margin + 150, $y);
    }
    else {
      $margin = 10;
      $this->SetLeftMargin($margin);
      $this->SetRightMargin($margin);
      $full_page_width = $this->GetPageWidth();
      $page_width = $full_page_width - ($margin * 2);

      if ($this->isReturnedOrder) {
        $this->SetFont('Arial', 'B', 30);
        $this->Cell($page_width, 10, 'Returned Order', 0, 1, 'C');
      }
//      else {
//        $this->SetFont('Arial', '', 30);
//        $this->Cell($page_width, 10, 'Order', 0, 1, 'C');
//      }

      $y_top = $this->GetY() + 10;
      $w = 55;

      // Order number.
      $this->SetY($y_top);
      $x_right_texts = -($w + $margin);
      $this->SetX($x_right_texts);
      $this->SetFont('Arial', '', 8);
      $this->Cell($w, 0, 'Order Number:', 0, 0, 'L');
      $this->SetX($x_right_texts);
      $this->SetFont('Arial', 'B', 14);
      $this->Cell($w, 0, $this->order['id'], 0, 0, 'R');

      if ($this->order['id'] != $this->previousOrderId) {

        $this->previousOrderId = $this->order['id'];

        // Address.
        $address_lines = $this->formatLabelLines($this->order['shipping']);
        $this->SetFont('Arial', 'B', 14);
        $y = $y_top;
        foreach ($address_lines as $address_line) {
          $this->Text($margin, $y, $address_line);
          $y += $this->addressLineSteps;
        }

        // Batched time.
        $this->SetY($y_top + 5);
        $this->SetX($x_right_texts);
        $this->SetFont('Arial', '', 8);
        $this->Cell($w, 0, 'Order Processed:', 0, 0, 'L');
        $this->SetX($x_right_texts);
        $this->Cell($w, 0, $this->order['batched'], 0, 1, 'R');

        // Info text.
        $this->SetX($margin);
        $this->SetY($this->GetY() + 30);
        $this->SetFont('Arial', '', 16);
        $this->Write(8, $this->infoText);

      }

      // Items.
      $this->SetY($this->GetY() + 15);
      $this->setX($margin);
      $this->SetFont('Arial', '', 16);
      $title_w = $page_width - 40;
      $this->Cell(20, 10, 'SKU', 0, 0, 'L');
      $this->Cell($title_w, 10, 'Title', 0, 0, 'L');
      $this->Cell(20, 10, 'Quantity', 0, 1, 'R');
      $y = $this->GetY();
      $this->Line($margin, $y, $full_page_width - $margin, $y);

    }
  }

  /**
   * {@inheritdoc}
   */
  function Footer() {
    $y = $this->GetPageHeight() - 16;
    $w = $this->GetPageHeight();
    $this->Line(0, $y, $w, $y);
    $this->SetY(-15);
    $this->SetFont('Arial', '', 12);
    $this->Cell(0, 10, 'Publication Ordering System ', 0, 0, 'L');
    $this->SetY(-15);
    $this->SetFont('Arial', '', 8);
    $this->Cell(0, 10, date('m-d-y h:i A') . '   -   Page ' . $this->PageNo(), 0, 0, 'R');
  }

  public function WordWrap(&$text, $maxwidth) {
    $text = trim($text);
    if ($text === '') {
      return 0;
    }
    $space = $this->GetStringWidth(' ');
    $lines = explode("\n", $text);
    $text = '';
    $count = 0;

    foreach ($lines as $line) {
      $words = preg_split('/ +/', $line);
      $width = 0;

      foreach ($words as $word) {
        $wordwidth = $this->GetStringWidth($word);
        if ($wordwidth > $maxwidth) {
          for ($i = 0; $i < strlen($word); $i++) {
            $wordwidth = $this->GetStringWidth(substr($word, $i, 1));
            if ($width + $wordwidth <= $maxwidth) {
              $width += $wordwidth;
              $text .= substr($word, $i, 1);
            }
            else {
              $width = $wordwidth;
              $text = rtrim($text) . "\n" . substr($word, $i, 1);
              $count++;
            }
          }
        }
        elseif ($width + $wordwidth <= $maxwidth) {
          $width += $wordwidth + $space;
          $text .= $word . ' ';
        }
        else {
          $width = $wordwidth + $space;
          $text = rtrim($text) . "\n" . $word . ' ';
          $count++;
        }
      }
      $text = rtrim($text) . "\n";
      $count++;
    }
    $text = rtrim($text);
    return $count;
  }

  /**
   * Formats the array with the address lines.
   *
   * @param array $shipping
   *   Shipping information.
   *
   * @return array
   *   The lines with the shipping.
   */
  private function formatLabelLines($shipping) {
    $result = [
      $shipping['first_name'] . chr(32) . $shipping['last_name'],
      $shipping['organization'],
      $shipping['address_1'],
      $shipping['address_2'],
      $shipping['city'] . ', ' . $shipping['state'] . chr(32) . $shipping['zip'],
    ];
    $result = array_values(array_filter($result, [$this, 'filterLines']));
    return $result;
  }

  /**
   * Callback for array_filter().
   *
   * Removes empty elements and other undesired values.
   *
   * @param string $element
   *   Element to be checked.
   *
   * @return bool
   *   Whether or not the string should be kept.
   */
  private function filterLines($element) {
    $element = trim($element);
    if (!$element) {
      return FALSE;
    }
    elseif (empty($element)) {
      return FALSE;
    }
    elseif ($element == '- None -' || $element == '_none') {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  function WriteHTML($html)
  {
    // HTML parser
    $html = str_replace("\n",' ',$html);
    $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
      if($i%2==0)
      {
        // Text
        if($this->HREF)
          $this->PutLink($this->HREF,$e);
        else
          $this->Write(5,$e);
      }
      else
      {
        // Tag
        if($e[0]=='/')
          $this->CloseTag(strtoupper(substr($e,1)));
        else
        {
          // Extract attributes
          $a2 = explode(' ',$e);
          $tag = strtoupper(array_shift($a2));
          $attr = array();
          foreach($a2 as $v)
          {
            if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
              $attr[strtoupper($a3[1])] = $a3[2];
          }
          $this->OpenTag($tag,$attr);
        }
      }
    }
  }

  function OpenTag($tag, $attr)
  {
    // Opening tag
    if($tag=='B' || $tag=='I' || $tag=='U')
      $this->SetStyle($tag,true);
    if($tag=='A')
      $this->HREF = $attr['HREF'];
    if($tag=='BR')
      $this->Ln(5);
  }

  function CloseTag($tag)
  {
    // Closing tag
    if($tag=='B' || $tag=='I' || $tag=='U')
      $this->SetStyle($tag,false);
    if($tag=='A')
      $this->HREF = '';
  }

  function SetStyle($tag, $enable)
  {
    // Modify style and select corresponding font
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach(array('B', 'I', 'U') as $s)
    {
      if($this->$s>0)
        $style .= $s;
    }
    $this->SetFont('',$style);
  }

  function PutLink($URL, $txt)
  {
    // Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
  }


}