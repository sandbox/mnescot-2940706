<?php

/**
 * @file
 * Contains \Drupal\pos_pdf_printing\PdfInvoice.
 */

namespace Drupal\pos_pdf_printing;

use Drupal\Core\Form\ConfigFormBase;
/**
 * Class PdfInvoice.
 *
 * @package Drupal\pos_pdf_printing
 */
class PdfInvoice {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Generates a PDF with the Invoice.
   *
   * @param array $orders
   *   Orders contained in the invoice.
   * @param bool $is_returned_order
   *   Whether the Order ia a returned one.
   */
  public function generateInvoices($orders, $is_returned_order = FALSE) {
    $pdf = new PosFPDF();
    $pdf->setIsReturnedOrder($is_returned_order);
    if (!$is_returned_order) {
      $this->coverPage($pdf, $orders);
    }
    $this->invoicesPages($pdf, $orders);
    $pdf_output = $pdf->Output('S');
    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename="Order_Invoice.pdf"');
    header('Content-Length: ' . strlen($pdf_output));
    echo $pdf_output;
    flush();
  }

  /**
   * Builds the cover page.
   *
   * @param \FPDF $pdf
   *   PDF object.
   * @param array $orders
   *   Orders contained in the invoice.
   */
  private function coverPage(\FPDF $pdf, $orders) {
    $pdf->setContentSection('cover');
    $pdf->AddPage();
    $page_width = $pdf->GetPageWidth();
    $margin = ($page_width - 150) / 2;
    $pdf->SetFillColor(230);
    foreach ($orders as $number => $order) {
      $pdf->setX($margin);
      $bg = ($number % 2) ? TRUE : FALSE;
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(7, 10, ($number + 1), 0, 0, 'C', $bg);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(26, 10, $order['id'], 0, 0, 'C', $bg);
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(57, 10, $order['shipping']['first_name'] . chr(32) . $order['shipping']['last_name'], 0, 0, 'L', $bg);
      $pdf->Cell(60, 10, $order['batched'], 0, 1, 'C', $bg);
    }
  }

  /**
   * Builds the invoices pages.
   *
   * @param \FPDF $pdf
   *   PDF object.
   * @param array $orders
   *   Orders contained in the invoice.
   */
  private function invoicesPages(\FPDF $pdf, $orders) {
    $pdf->setContentSection('invoices');
    $margin = 10;
    $pdf->SetLeftMargin($margin);
    $pdf->SetRightMargin($margin);
    $full_page_width = $pdf->GetPageWidth();
    $page_width = $full_page_width - ($margin * 2);
    $y_top = $pdf->getYTopItems();
    $pdf->SetY($y_top);
    $pdf->SetFont('Arial', '', 16);
    $pdf->WordWrap($this->infoText, $page_width);
    foreach ($orders as $order) {
      $pdf->setOrder($order);
      $pdf->AddPage();
      $pdf->SetFillColor(230);
      $pdf->SetFont('Arial', '', 12);
      foreach ($order['items'] as $number => $item) {
        $title_w = $page_width - 40;
        $bg = ($number % 2) ? TRUE : FALSE;
        $pdf->setX($margin);
        $pdf->Cell(20, 10, $item['sku'], 0, 0, 'L', $bg);
        $pdf->setX($margin + 20 + $title_w);
        $pdf->Cell(20, 10, $item['quantity'], 0, 0, 'R', $bg);
        $pdf->setX($margin + 20);
        $pdf->MultiCell($title_w, 10, $item['product']['title'], 0, 'L', $bg);
      }
      $y = $pdf->GetY();
      $pdf->Line($margin, $y, $full_page_width - $margin, $y);
      if ($order['historic']['status'] == 'returned') {
        $y = $pdf->GetY() + 10;
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Text($margin, $y, 'Comment');
        $pdf->SetY($y + 5);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Write(5, $order['historic']['comment']);
      }

      $config = \Drupal::service('config.factory')->getEditable('pos_invoice_promo_lang.settings');

      $paragraph1 = $config->get('pos_invoice_promo_lang', array('value' => '', 'format' => 'full_html'));
      $paragraph2 = $config->get('pos_invoice_promo_lang2', array('value' => '', 'format' => 'full_html'));
      $paragraph3 = $config->get('pos_invoice_promo_lang3', array('value' => '', 'format' => 'full_html'));
      $paragraph4 = $config->get('pos_invoice_promo_lang4', array('value' => '', 'format' => 'full_html'));
      $paragraph5 = $config->get('pos_invoice_promo_lang5', array('value' => '', 'format' => 'full_html'));
      $paragraph6 = $config->get('pos_invoice_promo_lang6', array('value' => '', 'format' => 'full_html'));
      $paragraph7 = $config->get('pos_invoice_promo_lang7', array('value' => '', 'format' => 'full_html'));
      $add_promo_language = $config->get('add_promo_language');
      if($add_promo_language=='yes') {
        $pdf->Ln();
        if(isset($paragraph1['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph1['value']));
          $pdf->Ln();
          $pdf->Ln();
        }

        if(isset($paragraph2['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph2['value']));
          $pdf->Ln();
          $pdf->Ln();
        }

        if(isset($paragraph3['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph3['value']));
          $pdf->Ln();
          $pdf->Ln();
        }

        if(isset($paragraph4['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph4['value']));
          $pdf->Ln();
          $pdf->Ln();
        }

        if(isset($paragraph5['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph5['value']));
          $pdf->Ln();
          $pdf->Ln();
        }

        if(isset($paragraph6['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph6['value']));
          $pdf->Ln();
          $pdf->Ln();
        }

        if(isset($paragraph7['value'])) {
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML(utf8_decode($paragraph7['value']));
          $pdf->Ln();
          $pdf->Ln();
        }
        //$pdf->Output();
      }
    }
  }

}
