<?php
/**
 * @file
 * Contains Drupal\pos_anonymization\PosAnonymization.
 */

namespace Drupal\pos_anonymization;

use DateTime;
use Faker\Factory;
use Drupal\Core\Database\Database;

/**
 * Class PosAnonymization.
 *
 * @package Drupal\pos_anonymization
 */
class PosAnonymization {

  private $selectedOrders = [];
  private $personas = [];
  private $totalAnonymizedOrders = 0;
  private $totalAnonymizedCustomers = 0;
  private $totalAnonymizedShippings = 0;
  private $faker = NULL;
  private $dateLimit = NULL;
  private $customersWithRecentOrders = [];
  private $customersAlreadyAnonymized = [];
  private $customersWithNoOrders = [];

  /**
   * PosAnonymization constructor.
   */
  public function __construct() {
    $this->faker = Factory::create();
    $date = new DateTime();
    $date->modify('-6 months');
    $this->dateLimit = $date->getTimestamp();
    //@TODO: delete this line.
    $this->dateLimit = 1501791421;
  }

  /**
   * Executes the anonymization process.
   *
   * @param string $scope
   *   The scope of the anonymization.
   */
  public function execute($scope = '6 months') {
    if ($scope == '6 months') {
      $this->getOlderThanSixMonthsOrders();
      $this->getCustomersWithRecentOrders();
    }
    elseif ($scope == 'development') {
      $this->getAllOrders();
    }
    if ($this->selectedOrders) {
      $this->generatePersonas();
      $this->anonymizeOrders();
      $this->anonymizeOrderProfiles();
      $this->anonymizeOrderShippings();
    }
    $this->getCustomersWithNoOrders();
    if ($this->customersWithNoOrders) {
      $this->anonymizeOrderProfilesWithNoOrders();
    }
  }

  /**
   * Get the totals of anonymized records.
   *
   * @return array
   *   Associative array: entity name => total or anonymized records.
   */
  public function getTotalAnonymized() {
    return [
      'orders' => $this->totalAnonymizedOrders,
      'customers' => $this->totalAnonymizedCustomers,
      'shippings' => $this->totalAnonymizedShippings,
    ];
  }

  /**
   * Get IDs of the orders older than 6 months that should be anonymized.
   */
  private function getOlderThanSixMonthsOrders() {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->addField('o', 'id');
    $query->addField('o', 'customer_id');
    $query->condition('changed', $this->dateLimit, '<=');
    $query->condition('uuid', 'anonymized%', 'NOT LIKE');
    $query->orderBy('id');
    $this->selectedOrders = $query->execute()->fetchAll();
  }


  /**
   * Get IDs of the all orders to be anonymized.
   */
  private function getAllOrders() {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->addField('o', 'id');
    $query->addField('o', 'customer_id');
    $query->orderBy('id');
    $this->selectedOrders = $query->execute()->fetchAll();
  }

  /**
   * Get IDs of all customers who had orders recently issued to them.
   */
  private function getCustomersWithRecentOrders() {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->addField('o', 'customer_id');
    $query->condition('changed', $this->dateLimit, '>');
    $query->groupBy('o.customer_id');
    $this->customersWithRecentOrders = [];
    foreach ($query->execute()->fetchAll() as $item) {
      $this->customersWithRecentOrders[$item->customer_id] = $item->customer_id;
    }
  }

  /**
   * Get IDs of all customers who have no orders associated to them.
   */
  private function getCustomersWithNoOrders() {
    $connection = Database::getConnection();
    $query = $connection->select('pos_customer_profiles', 'c');
    $query->addJoin('LEFT', 'pos_orders', 'o', 'o.customer_id = c.id');
    $query->addField('c', 'id');
    $query->condition('o.customer_id', NULL, 'IS NULL');
    $query->condition('c.changed', $this->dateLimit, '<=');
    $query->condition('c.uuid', 'anonymized%', 'NOT LIKE');
    $query->orderBy('id');
    $this->customersWithNoOrders = [];
    foreach ($query->execute()->fetchAll() as $item) {
      $this->customersWithNoOrders[$item->id] = $item->id;
    }
  }

  /**
   * Generate the first and last names the will be used across all entities.
   */
  private function generatePersonas() {
    foreach ($this->selectedOrders as $selected_order) {
      $first_name = $this->faker->firstName();
      $last_name = $this->faker->lastName();
      $this->personas[$selected_order->id] = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'full_name' => $first_name . chr(32) . $last_name,
        'email' => strtolower($first_name) . '.' . strtolower($last_name) . '@anonymous.org',
      ];
    }
  }

  /**
   * Radomly provide a value.
   *
   * @param string $function_name
   *   A Faker function mame.
   * @param int $chances
   *   Number of chances in one of a value will be provided.
   *
   * @return mixed
   *   Value or NULL
   */
  private function randomFilling($function_name = NULL, $chances = 2) {
    if ($function_name && rand(0, $chances) == 0) {
      return $this->faker->$function_name();
    }
    else {
      return NULL;
    }
  }

  /**
   * Generates Unique ID.
   *
   * @return string
   *   Unique ID.
   */
  private function generateUniqueId() {
    $uid = sprintf('anonymized-%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
    return $uid;
  }

  /**
   * Generates Address.
   *
   * @return string
   *   Address.
   */
  private function generateAddress() {
    return $this->faker->buildingNumber() . chr(32) .
      $this->faker->streetName() . chr(32) .
      $this->faker->streetSuffix();
  }

  /**
   * Anonymize orders.
   */
  private function anonymizeOrders() {
    $connection = Database::getConnection();
    $this->totalAnonymizedOrders = 0;
    foreach ($this->selectedOrders as $selected_order) {
      $query = $connection->update('pos_orders');
      $query->fields([
        'uuid' => $this->generateUniqueId(),
        'email' => $this->personas[$selected_order->id]['email'],
      ]);
      $query->condition('id', $selected_order->id, '=');
      $this->totalAnonymizedOrders += $query->execute();
    }
  }

  /**
   * Anonymize Customer Profiles that are linked to an Order.
   */
  private function anonymizeOrderProfiles() {
    $connection = Database::getConnection();
    $this->totalAnonymizedCustomers = 0;
    foreach ($this->selectedOrders as $selected_order) {
      if (
        $selected_order->customer_id &&
        !isset($this->customersWithRecentOrders[$selected_order->customer_id]) &&
        !isset($this->customersAlreadyAnonymized[$selected_order->customer_id])
      ) {
        $this->customersAlreadyAnonymized[$selected_order->customer_id] = $selected_order->customer_id;
        $query = $connection->update('pos_customer_profiles');
        $query->fields([
          'uuid' => $this->generateUniqueId(),
          'email' => $this->personas[$selected_order->id]['email'],
          'first_name' => $this->personas[$selected_order->id]['first_name'],
          'last_name' => $this->personas[$selected_order->id]['last_name'],
          'phone' => $this->faker->phoneNumber(),
          'organization' => $this->randomFilling('company'),
          'address_1' => $this->generateAddress(),
          'address_2' => $this->randomFilling('secondaryAddress'),
          // 'city' => $this->faker->city(),
          // 'state' => $this->faker->stateAbbr(),
          // 'zip' => $this->faker->postcode(),
        ]);
        $query->condition('id', $selected_order->customer_id, '=');
        $this->totalAnonymizedCustomers += $query->execute();
      }
    }
  }

  /**
   * Anonymize Customer Profiles that are NOT linked to an Order.
   */
  private function anonymizeOrderProfilesWithNoOrders() {
    $connection = Database::getConnection();
    $this->totalAnonymizedCustomers = 0;
    foreach ($this->customersWithNoOrders as $customers) {
      $first_name = $this->faker->firstName();
      $last_name = $this->faker->lastName();
      $email = $first_name . '.' . $last_name . '@anonymous.org';
      $query = $connection->update('pos_customer_profiles');
      $query->fields([
        'uuid' => $this->generateUniqueId(),
        'email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone' => $this->faker->phoneNumber(),
        'organization' => $this->randomFilling('company'),
        'address_1' => $this->generateAddress(),
        'address_2' => $this->randomFilling('secondaryAddress'),
        // 'city' => $this->faker->city(),
        // 'state' => $this->faker->stateAbbr(),
        // 'zip' => $this->faker->postcode(),
      ]);
      $query->condition('id', $customers, '=');
      $this->totalAnonymizedCustomers += $query->execute();
    }
  }

  /**
   * Anonymize Shippings.
   */
  private function anonymizeOrderShippings() {
    $connection = Database::getConnection();
    $this->totalAnonymizedShippings = 0;
    foreach ($this->selectedOrders as $selected_order) {
      $query = $connection->update('pos_shippings');
      $query->fields([
        'uuid' => $this->generateUniqueId(),
        'email' => $this->personas[$selected_order->id]['email'],
        'first_name' => $this->personas[$selected_order->id]['first_name'],
        'last_name' => $this->personas[$selected_order->id]['last_name'],
        'phone' => $this->faker->phoneNumber(),
        'organization' => $this->randomFilling('company'),
        'address_1' => $this->generateAddress(),
        'address_2' => $this->randomFilling('secondaryAddress'),
        // 'city' => $this->faker->city(),
        // 'state' => $this->faker->stateAbbr(),
        // 'zip' => $this->faker->postcode(),
      ]);
      $query->condition('order_id', $selected_order->id, '=');
      $this->totalAnonymizedShippings += $query->execute();
    }
  }

}