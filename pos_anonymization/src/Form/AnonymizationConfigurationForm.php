<?php

/**
 * @file
 * Contains Drupal\pos_anonymization\Form\AnonymizationConfigurationForm.
 */

namespace Drupal\pos_anonymization\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AnonymizationConfigurationForm.
 *
 * @package Drupal\pos_anonymization\Form
 */
class AnonymizationConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pos_anonymization.anonymizationconfiguration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'anonymization_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pos_anonymization.anonymizationconfiguration');
    if(!empty($config->get('last_anonymization_date'))) {
      $last_anonymization_date = new \DateTime('@' . $config->get('last_anonymization_date'));
      $last_anonymization_date->setTimezone(new \DateTimeZone('America/New_York'));
    }

    $form['manual_operation_6_months'] = [
      '#type' => 'details',
      '#title' => $this->t('Production - Manual Operation of the anonymization 
      process of records older than 6 months'),
      '#open' => TRUE,
    ];
    $form['manual_operation_6_months']['last_anonymization_date'] = [
      '#type' => 'item',
      '#title' => $this->t('Date and time of the last anonymization'),
      '#markup' => !empty($config->get('last_anonymization_date')) ? $last_anonymization_date->format('Y-m-d H:i') : NULL,
    ];
    $form['manual_operation_6_months']['frequency'] = [
      '#type' => 'item',
      '#title' => $this->t('Frequency'),
      '#markup' => $this->t('Anonymization occurs on every first day of each month.'),
    ];
    $form['manual_operation_6_months']['pii_check'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirm execution of PII anonymization'),
      '#description' => $this->t('Before clicking in the button to start the 
      anonymization please confirm the execution of the process by writing "anonymize" 
      in the field above.'),
    ];
    $form['manual_operation_6_months']['pii_button'] = [
      '#type' => 'submit',
      '#id' => 'edit-pii-button',
      '#value' => $this->t('Anonymize records older than 6 months'),
    ];

    $host = \Drupal::request()->getHost();
    if (!preg_match('/nia.nih.gov/', $host)) {
      $form['manual_operation_development'] = [
        '#type' => 'details',
        '#title' => $this->t('Development - Manual Operation of the anonymization process 
        of all records for development purposes'),
        '#open' => TRUE,
      ];
      $form['manual_operation_development']['development_check'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Confirm execution of PII anonymization for all records'),
        '#description' => $this->t('Before clicking in the button to start 
        the anonymization please confirm the execution of the process by writing "anonymize" 
        in the field above.'),
      ];
      $form['manual_operation_development']['development_button'] = [
        '#type' => 'submit',
        '#id' => 'edit-development-button',
        '#value' => $this->t('Anonymize all records for development purposes'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $triggering_element = $form_state->getTriggeringElement();
    $pii_check = $form_state->getValue('pii_check');
    $development_check = $form_state->getValue('development_check');
    if ($triggering_element['#id'] == 'edit-pii-button' && $pii_check != 'anonymize') {
      $form_state->setErrorByName('pii_check', $this->t('Need confirmation for this operation'));
    }
    if ($triggering_element['#id'] == 'edit-development-button' && $development_check != 'anonymize') {
      $form_state->setErrorByName('development_check', $this->t('Need confirmation for this operation'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#id'] == 'edit-pii-button') {
      _pos_anonymization_execute('6 months');
    }
    elseif ($triggering_element['#id'] == 'edit-development-button') {
      _pos_anonymization_execute('development');
    }
  }

}
