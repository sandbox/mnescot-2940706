<?php

/**
 * @file
 * Contains pos_anonymization.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\pos_anonymization\PosAnonymization;

/**
 * Implements hook_help().
 */
function pos_anonymization_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the pos_anonymization module.
    case 'help.page.pos_anonymization':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Generates fake data for anoymization purposes.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Executes the anonymization.
 */
function _pos_anonymization_execute($scope) {
  $anonymize = new PosAnonymization();
  $anonymize->execute($scope);
  $totals = $anonymize->getTotalAnonymized();
  $message = t('Number of anonymized Orders records: %anonymized', ['%anonymized' => $totals['orders']]);
  \Drupal::logger('pos_anonymization')->notice($message);
  drupal_set_message($message);
  $message = t('Number of anonymized Customer profiles records: %anonymized', ['%anonymized' => $totals['customers']]);
  \Drupal::logger('pos_anonymization')->notice($message);
  drupal_set_message($message);
  $message = t('Number of anonymized Shippings records: %anonymized', ['%anonymized' => $totals['shippings']]);
  \Drupal::logger('pos_anonymization')->notice($message);
  drupal_set_message($message);
  \Drupal::configFactory()->getEditable('pos_anonymization.anonymizationconfiguration')
    ->set('last_anonymization_date', time())
    ->save();
}


/**
 * Implements hook_cron().
 */
function pos_anonymization_cron() {
  $last_anonymization_date = \Drupal::config('pos_anonymization.anonymizationconfiguration')->get('last_anonymization_date');
  $d_last = date('YmdHis', $last_anonymization_date);
  $d_check = date('Ymd000000');
  if ($d_last < $d_check) {
    _pos_anonymization_execute('6 months');
  }
}