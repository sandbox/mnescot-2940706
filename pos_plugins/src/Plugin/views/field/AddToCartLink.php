<?php

/**
 * @file
 * Contains \Drupal\pos_plugins\Plugin\views\field\AddToCartLink.
 */

namespace Drupal\pos_plugins\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("add_to_cart_link")
 */
class AddToCartLink extends FieldPluginBase {

  private $path = NULL;
  private $profileId = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->path = \Drupal::service('path.current')->getPath();
    if (preg_match("/\/profile\//", $this->path)) {
      $path_args = explode('/', $this->path);
      $this->profileId = (int) (end($path_args));
      $a = 1;
    }
    $parameters = \Drupal::request()->query->all();
    $query = array();
    foreach ($parameters as $key => $value) {
      $query[] = $key . '=' . $value;
    }
    if ($query) {
      $this->path .= '?' . implode('&', $query);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
   // do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_alter_empty'] = array('default' => FALSE);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_entity;
    $url = Url::fromRoute('products.add_to_cart', [
      'destination' => $this->path,
      'product_id' => $entity->id(),
      'title' => $entity->getTitle(),
      'copy_limit' => $entity->getCopyLimit(),
      'sku' => $entity->getSku(),
      'profile_id' => $this->profileId,
    ]);
    $url->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button', 'button--small', 'button-add-to-cart'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 400]),
      ],
    ]);
    $output = array(
      '#type' => 'link',
      '#title' => $this->t('Add to cart'),
      '#url' => $url,
      '#attached' => array(
        'library' => array('publications_ordering_system/pos_scripts'),
      ),
    );
    return $output;
  }

}
