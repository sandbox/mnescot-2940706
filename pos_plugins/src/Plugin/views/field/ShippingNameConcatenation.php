<?php
/**
 * @file
 * Contains \Drupal\pos_plugins\Plugin\views\field\ShippingNameConcatenation.
 */

namespace Drupal\pos_plugins\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to concatenate name elements.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("shipping_name_concatenation")
 */
class ShippingNameConcatenation extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_entity;
    $result = $entity->get('first_name')->value;
    $result .= ' ';
    $result .= $entity->get('last_name')->value;
    return $result;
  }

}
