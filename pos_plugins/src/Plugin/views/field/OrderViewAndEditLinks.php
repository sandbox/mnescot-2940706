<?php
/**
 * @file
 * Contains \Drupal\pos_plugins\Plugin\views\field\OrderViewAndEditLinks.
 */

namespace Drupal\pos_plugins\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Field handler to generate links for viewing and editing an Order.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("order_view_edit_links")
 */
class OrderViewAndEditLinks extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_relationship_entities['order_id'];
    $param = [
      'id' => $entity->id(),
    ];
    $view_url = Url::fromRoute('pos_orders_page.view_order_content', $param);
    $view_url->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button', 'button--small'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 800]),
      ],
    ]);
    $current_path = \Drupal::service('path.current')->getPath();
    $destination = explode('/', $current_path);
    $destination = array_filter($destination);
    if (count($destination) > 1) {
      $destination = serialize($destination);
    }
    else {
      $destination = array_shift($destination);
    }

    $param = [
      'order_id' => $entity->id(),
      'destination_args' => $destination,
    ];
    $edit_url = Url::fromRoute('order.edit_with_destination', $param);
    $output = [
      [
        '#type' => 'link',
        '#title' => $this->t('View'),
        '#url' => $view_url,
        '#attached' => array(
          'library' => array('publications_ordering_system/pos_scripts'),
        ),
      ],
      [
        '#type' => 'link',
        '#markup' => '&nbsp;&nbsp;',
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Edit'),
        '#url' => $edit_url,
      ],
    ];
    return $output;
  }

}
