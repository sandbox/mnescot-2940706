<?php

/**
 * @file
 * Contains pos_plugins\pos_plugins.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Render\Markup;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\system\ActionConfigEntityInterface;

/**
* Implements hook_views_data().
*/
function pos_plugins_views_data() {

  $data['pos_products']['add_to_cart_link'] = array(
    'title' => t('Add to Cart link'),
    'help' => t('A link button to product ordering'),
    'field' => array(
      'id' => 'add_to_cart_link',
    ),
  );

  $data['pos_orders']['order_view_edit_links'] = array(
    'title' => t('Order view and edit links'),
    'field' => array(
      'title' => t('Order view and edit links'),
      'help' => t('Links to view and edit and Order.'),
      'id' => 'order_view_edit_links',
    ),
  );

  $data['pos_shippings']['shipping_name_concatenation'] = array(
    'title' => t('Name concatenation'),
    'field' => array(
      'title' => t('First and last names'),
      'help' => t('Concatenation of first and last names.'),
      'id' => 'shipping_name_concatenation',
    ),
  );

  $data['pos_items']['items_created_date'] = array(
    'title' => t('Items Created Date - Custom'),
    'filter' => array(
      'title' => t('Items Created Date - Custom'),
      'help' => 'Filters items by create date range',
      'field' => 'created',
      'id' => 'items_created_date',
    ),
  );

  return $data;
}
