<?php

/**
 * @file
 * Contains \Drupal\pos_orders_page\Controller\ViewOrder.
 */

namespace Drupal\pos_orders_page\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ViewOrder.
 *
 * @package Drupal\pos_orders_page\Controller
 */
class ViewOrder extends ControllerBase {

  /**
   * Content.
   *
   * @return array
   *   Return render array.
   */
  public function content($id) {
    $output = [
      '#theme' => 'complete_order_page',
      '#order' => \Drupal::service('pos_operations.order')->getOrder($id),
    ];
    return $output;
  }

}
