<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 7/3/17
 * Time: 1:25 PM
 */
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_pre_render().
 */
function publications_ordering_system_views_pre_render(ViewExecutable $view) {
  if ($view->id() == 'order_status') {
    $title = 'Orders Status - ';
    if ($args = $view->args) {
      $title .= \Drupal::service('pos_operations.status')->getStatusName($args[0]);
    }
    else {
      $title .= 'All';
    }
    $view->setTitle($title);
  }
}
