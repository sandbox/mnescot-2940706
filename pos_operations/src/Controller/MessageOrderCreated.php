<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Controller\MessageOrderCreated.
 */

namespace Drupal\pos_operations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class MessageOrderCreted.
 *
 * @package Drupal\pos_operations\Controller
 */
class MessageOrderCreated extends ControllerBase {
  /**
   * Displaymessage.
   *
   * @return array
   *   Return render array.
   */
  public function displayMessage($order_id) {
    $user = \Drupal::currentUser()->getRoles();
    $order = \Drupal::service('pos_operations.order')->getOrder($order_id);
    $customer_id = $order['customer_id'];
    $link = "";
    if (in_array("administrator", $user) || in_array("publications", $user)) {
      //Add link to add contact report
      //$link = "Please click ". Link::fromTextAndUrl(t("here"), Url::fromUri('internal:/order/'.$order_id,array()))->toString()." to create Contact Report";
      $link = "Please click ". Link::fromTextAndUrl(t("here"), Url::fromUri('internal:/node/add/contact_report?id='.$customer_id.'&orderid='.$order_id,array()))->toString()." to create Contact Report";
    }
    return [
      '#type' => 'markup',
      //'#markup' => $this->t('Thank you for submitting you order. It has been received and is being processed. You order number is: %order', ['%order' => $order_id]),
        '#markup' => $this->t('Thank you for submitting your order. It has been placed and is being processed. Your order number is: %order. Please make note of your order number. If you have any questions, please call 1-800-222-2225.<br /><br />'.$link, ['%order' => $order_id]),
    ];
  }

}
