<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Controller\MassSetStatus.
 */

namespace Drupal\pos_operations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class MassSetStatus.
 *
 * @package Drupal\pos_operations\Controller
 */
class MassSetStatus extends ControllerBase {
  /**
   * Execute.
   *
   * @return array
   *   Return render array.
   */
  public function execute($parameters) {

    $parameters = unserialize($parameters);

    \Drupal::service('pos_operations.order')->massStatusSetting($parameters['status'], $parameters['ids']);

    if ($parameters['status'] == 'shipping') {
      if ($parameters['batch_owner_id']) {
        $url = Url::fromRoute('order.in_progress_orders', ['batch_owner_id' => $parameters['batch_owner_id']]);
      }
      else {
        $url = Url::fromRoute('order.in_progress_orders_all_staff');
      }
    }
    elseif ($parameters['status'] == 'completed') {
      $url = Url::fromRoute('order.shipping_orders');
    }
    else {
      $url = Url::fromUserInput('/dashboard');
    }

    return new RedirectResponse($url->toString());
  }

}
