<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Form\ConfirmRemoveAllSessionsForm.
 */

namespace Drupal\pos_operations\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form to confirm deletion all POS sessions.
 */
class ConfirmRemoveAllSessionsForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('pos_operations.session')->clearAllSessions();
    drupal_set_message($this->t('All POS sessions were removed.'));
    $url = Url::fromRoute('pos_operations.sessions_configuration');
    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "confirm_delete_all_sessions_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('pos_operations.sessions_configuration');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Please confirm the removal off all POS sessions.');
  }

}