<?php

/**
 * @file
 * Contains Drupal\pos_anonymization\Form\OperationsConfigurationForm.
 */

namespace Drupal\pos_operations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class OperationsConfigurationForm.
 *
 * @package Drupal\pos_operations\Form
 */
class OperationsConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pos_operations.sessions_configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'operations_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $session_duration = $this
      ->config('pos_operations.sessions_configuration')
      ->get('session_duration');

    if (!$session_duration || $session_duration < 1) {
      $session_duration = 4;
    }

    $form['sessions_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Sessions Settings'),
      '#open' => TRUE,
    ];
    $form['sessions_settings']['session_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration'),
      '#description' => $this->t('Enter the maximum session duration in hours.'),
      '#default_value' => $session_duration,
      '#min' => 1,
      '#max' => 168,
      '#weight' => 10,
    ];

    $form['clear_sessions'] = [
      '#type' => 'details',
      '#title' => $this->t('Clear Sessions'),
      '#open' => TRUE,
    ];
    $form['clear_sessions']['clear_expired_sessions'] = [
      '#type' => 'submit',
      '#id' => 'clear-expired-sessions',
      '#value' => $this->t('Clear expired sessions'),
    ];
    $form['clear_sessions']['clear_all_sessions'] = [
      '#type' => 'submit',
      '#id' => 'clear-all-sessions',
      '#value' => $this->t('Clear all sessions'),
    ];
    $form['clear_sessions']['clear_description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b>Attention:</b> In order to clear all POS 
        sessions the site has to be in maintenance mode.'),
      '#prefix' => '<div class="description"><br />',
      '#suffix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);

  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('pos_operations.sessions_configuration')
      ->set('session_duration', $form_state->getValue('session_duration'))
      ->save();

    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#id'] == 'clear-expired-sessions') {
      \Drupal::service('pos_operations.session')->clearExpiredSessions();
    }
    elseif ($triggering_element['#id'] == 'clear-all-sessions') {
      if (\Drupal::state()->get('system.maintenance_mode')) {
        $url = Url::fromRoute('pos_operations.confirm_remove_all_sessions');
        $form_state->setRedirectUrl($url);
      }
      else {
        drupal_set_message($this->t('In order to perform the clear all POS 
        sessions the site has to be in maintenance mode.'), 'error');
      }
    }
    else {
      parent::submitForm($form, $form_state);
    }

  }

}
