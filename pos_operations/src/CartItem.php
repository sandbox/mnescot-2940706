<?php

/**
 * @file
 * Contains \Drupal\pos_operations\CartItem.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosCart;

/**
 * Class CartItem.
 *
 * @package Drupal\pos_operations
 */
class CartItem {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Gets the list of all cart items belonging to the sessions.
   *
   * @return array
   *   A list of all cart items.
   */
  public function getCartItems($review = FALSE) {
    $session_manager = \Drupal::service('pos_operations.session');
    $connection = Database::getConnection();
    $query = $connection->select('pos_cart', 'c');
    $query->join('pos_products', 'p', 'p.id = c.product_id');
    $query->addField('p', 'title');
    $query->addField('c', 'quantity');
    if (!$review) {
      $query->addField('c', 'id', 'item_id');
      $query->addField('c', 'product_id');
      $query->addField('c', 'sku');
      $query->addField('c', 'copy_limit');
    }
    $query->condition('c.session_id', $session_manager->getId(), '=');
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Remove the product from the cart.
   *
   * @param int $item_id
   *   Item ID.
   * @param int $product_id
   *   Product ID.
   * @param int $quantity
   *   Product quantity.
   *
   * @return bool/obj
   *   The result of the delete operations.
   */
  public function removeItem($item_id, $product_id, $quantity) {
    $session_manager = \Drupal::service('pos_operations.session');
    $connection = Database::getConnection();
    $query = $connection->delete('pos_cart');
    $query->condition('id', $item_id, '=');
    $query->condition('session_id', $session_manager->getId(), '=');
    if ($result = $query->execute()) {
      \Drupal::service('pos_operations.session')->checkSession();
      \Drupal::service('pos_operations.stock')->addToStock(
        $product_id,
        $quantity
      );
    }
    return $result;
  }

  /**
   * Adds a product to the cart.
   *
   * If there is already an item in the cart for the same product, it will be
   * updated with the sum of the quantities, but no new item will be created.
   *
   * @param int $product_id
   *   Product ID.
   * @param string $sku
   *   Product SKU.
   * @param int $quantity
   *   Product quantity.
   * @param int $copy_limit
   *   Product copy limit.
   * @param string $title
   *   Product title.
   *
   * @return int/obj
   *   The result of the save operations.
   */
  public function addItem($product_id, $sku, $quantity, $copy_limit, $title, $profile_id = NULL) {
    $result = NULL;
    if ($this->isItemAvailable($product_id, $title, $quantity)) {
      if ($item = $this->itemAlreadyExists($product_id)) {
        $total_quantity = $quantity + $item['quantity'];
        $result = $this->updateItem($item['id'], $total_quantity);
        $this->checkCopyLimit($total_quantity, $copy_limit, $title);
      }
      else {
        $cart = PosCart::create();
        $cart->setSessionId();
        $cart->setProductId($product_id);
        $cart->setSku($sku);
        $cart->setQuantity($quantity);
        $cart->setCopyLimit($copy_limit);
        $result = $cart->save();
        $this->checkCopyLimit($quantity, $copy_limit, $title);
      }
      if ($result) {
        \Drupal::service('pos_operations.session')->checkSession($profile_id);
        \Drupal::service('pos_operations.stock')->subtractFromStock(
          $product_id,
          $quantity
        );
      }
    }
    return $result;
  }

  /**
   * Check if the requested quantity is available.
   *
   * @param int $product_id
   *   Product ID.
   * @param string $title
   *   Item title.
   * @param int $quantity
   *   Requested quantity;.
   * @param int $quantity_anterior
   *   Previous quantity;.
   *
   * @return bool
   *   Whether requested quantity is available.
   */
  public function isItemAvailable($product_id, $title, $quantity, $quantity_anterior = 0) {
    $remaining_stock = \Drupal::service('pos_operations.stock')->getStock($product_id);
    $projected_stock = $remaining_stock - ($quantity - $quantity_anterior);
    if ($projected_stock < 0) {
      $remaining_stock += $quantity_anterior;
      drupal_set_message(t('Inventory of %title is currently low. Available quantity: 
      %remaining_stock].', ['%title' => $title, '%remaining_stock]' => $remaining_stock]), 'warning');
      $result = FALSE;
    }
    else {
      $result = TRUE;
    }
    return $result;
  }

  /**
   * Check whether the ordered quantity exceeds the copy limit..
   *
   * @param int $quantity
   *   Product quantity.
   * @param int $copy_limit
   *   Product copy limit.
   * @param string $title
   *   Product title.
   */
  private function checkCopyLimit($quantity, $copy_limit, $title) {
    if ($quantity > $copy_limit && $copy_limit > 0) {
      $default_config = \Drupal::config('pos_operations.settings');
      // @todo make messages to be customizable and yml changes doesn't reflect in the front-end.
      // yml file imported on module install time*
      //$message = $default_config->get('messages.copy_limit_exceeded.add_item');
      //$message = 'Inventory of "%title" is currently low. Quantities available are limited. We will review your order and respond to you with any questions.';
      $message = 'You have requested a quantity of product greater than the copy limit. We will make every attempt to ship the amount you requested, however you may receive a smaller amount based on supply. Thank you.';
      /*$placeholders = [
      '%quantity' => $quantity,
      '%copy_limit' => $copy_limit,
      '%title' => $title,
      ];*/
      $placeholders = [
          '%title' => $title,
      ];
      $message = str_replace(array_keys($placeholders), array_values($placeholders), $message);
      drupal_set_message($message, 'warning');
    }
  }

  /**
   * Updates items quantities.
   *
   * @param int $item_id
   *   Item ID.
   * @param int $quantity
   *   Item quantity .
   *
   * @return int/obj
   *   The result of the update operations.
   */
  public function updateItem($item_id, $quantity, $quantity_anterior = 0, $product_id = NULL) {
    $session_manager = \Drupal::service('pos_operations.session');
    $connection = Database::getConnection();
    $query = $connection->update('pos_cart');
    $query->fields(array(
      'quantity' => $quantity,
    ));
    $query->condition('id', $item_id, '=');
    $query->condition('session_id', $session_manager->getId(), '=');
    $result = $query->execute();
    if ($result && $quantity_anterior) {
      \Drupal::service('pos_operations.session')->checkSession();
      $quantity = $quantity_anterior - $quantity;
      \Drupal::service('pos_operations.stock')->addToStock(
        $product_id,
        $quantity
      );
    }
    return $result;
  }

  /**
   * Check is items already exists in the cart.
   *
   * @param int $product_id
   *   Product ID.
   *
   * @return bool
   *   Whether the item exists.
   */
  private function itemAlreadyExists($product_id) {
    $session_manager = \Drupal::service('pos_operations.session');
    $connection = Database::getConnection();
    $query = $connection->select('pos_cart', 'c');
    $query->addField('c', 'id');
    $query->addField('c', 'quantity');
    $query->condition('c.product_id', $product_id, '=');
    $query->condition('session_id', $session_manager->getId(), '=');
    if ($result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
      return array_shift($result);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Counts the items in the cart.
   *
   * @return int
   *   Number of items in the cart.
   */
  public function itemsCount() {
    $session_manager = \Drupal::service('pos_operations.session');
    $connection = Database::getConnection();
    $query = $connection->select('pos_cart', 'c');
    $query->addExpression('COUNT(*)', 'items_count');
    $query->condition('session_id', $session_manager->getId(), '=');
    if ($result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
      return $result[0]['items_count'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Remove all items from  the cart.
   */
  public function clearCart() {
    $session_manager = \Drupal::service('pos_operations.session');
    $connection = Database::getConnection();
    $query = $connection->delete('pos_cart');
    $query->condition('session_id', $session_manager->getId(), '=');
    $result = $query->execute();
    return $result;
  }

  /**
   * Check if there are items with quantity bigger than copy limit.
   *
   * @return bool
   *   True/False.
   */
  public function cartHasBulkItem() {
    $session_id = \Drupal::service('pos_operations.session')->getId();
    $connection = Database::getConnection();
    $query = "SELECT ID from pos_cart WHERE (session_id = '$session_id') AND (quantity > copy_limit) AND (copy_limit > 0)";
    $result = $connection->query($query);
    $result = $result->fetchAll();
    return $result;
  }

}
