<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Profile.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosCustomerProfiles;

/**
 * Class CartItem.
 *
 * @package Drupal\pos_operations
 */
class Profile {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Gets the city/state by zip code.
   *
   * @param int $zip
   *   ZIP CODE.
   *
   * @return array
   *   The zipcode table data.
   */
  public function getCityStateByZip($zip)
  {
    $connection = Database::getConnection();
    $query = $connection->select('ZIPCodes', 'z');
    $query->fields('z');
    //$query->condition('z.zipcode', $zip);
    $query->condition('z.ZipCode', "%" . $zip . "%", 'LIKE');
    //$query->range(0, 10);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
//ksm($result);
    return $result;
  }
  /**
   * Gets the profile with no formatted values.
   *
   * @param int $id
   *   Profile ID.
   *
   * @return array
   *   The profile values.
   */
  public function getRawProfile($id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_customer_profiles', 'p');
    $query->fields('p');
    $query->condition('p.id', $id);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
    return $result;
  }

  /**
   * Gets the profile with formatted values.
   *
   * @param int $id
   *   Profile ID.
   *
   * @return array
   *   The profile values.
   */
  public function getProfile($id) {
    $result = [];
    if ($result = $this->getRawProfile($id)) {
      $result['honorific'] = _pos_utils_get_term_name($result['honorific']);
      $result['how_heard'] = _pos_utils_get_term_name($result['how_heard']);
      $result['created'] = date('d-m-Y H:i:s', $result['created']);
      $result['changed'] = date('d-m-Y H:i:s', $result['changed']);
    }
    return $result;
  }


  /**
   * Get an Customer Profile as an entity object structure.
   *
   * @param string $id
   *   Customer Profile ID.
   *
   * @return string
   *   Customer Profile object.
   */
  public function getCustomerProfileEntity($id) {
    return PosCustomerProfiles::load($id);
  }


}
