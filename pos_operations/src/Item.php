<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Item.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosItems;

/**
 * Class CartItem.
 *
 * @package Drupal\pos_operations
 */
class Item {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Gets the Order items with no formatted values.
   *
   * @param int $id
   *   Order ID.
   *
   * @return array
   *   The items values.
   */
  public function getRawItems($id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_items', 'i');
    $query->fields('i');
    $query->condition('i.order_id', $id);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Gets the Order items with formatted values.
   *
   * @param int $id
   *   Order ID.
   *
   * @return array
   *   The items values.
   */
  public function getItems($id) {
    $result = $this->getRawItems($id);
    foreach ($result as &$item) {
      $item['product'] = \Drupal::service('pos_operations.product')->getProduct($item['product_id']);
      $item['created'] = date('d-m-Y H:i:s', $result['created']);
      $item['changed'] = date('d-m-Y H:i:s', $result['changed']);
    }
    return $result;
  }

  /**
   * Delete Item.
   *
   * @param int $id
   *   Item ID.
   *
   * @return object
   *   A Delete query object.
   */
  public function deleteItem($id) {
    $connection = Database::getConnection();
    $query = $connection->delete('pos_items');
    $query->condition('id', $id);
    $result = $query->execute();
    return $result;
  }

  /**
   * Delete Item to an Order.
   *
   * @param int $order_id
   *   Order ID.
   * @param int $product_id
   *   Product ID.
   * @param string $sku
   *   Product SKU.
   *
   * @return object/int
   *   The new Item entity;
   */
  public function addItem($order_id, $product_id, $sku) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_items', 'i');
    $query->addField('i', 'id');
    $query->condition('i.order_id', $order_id);
    $query->condition('i.product_id', $product_id);
    $data = $query->execute();
    $item = $data->fetchAll(\PDO::FETCH_ASSOC);
    if ($item) {
      $result = NULL;
    }
    else {
      $values = [
        'order_id' => $order_id,
        'product_id' => $product_id,
        'sku' => $sku,
        'quantity' => 0,
      ];
      $item_entity = PosItems::create($values);
      $result = $item_entity->save();
    }
    return $result;
  }

  /**
   * Updates the quantity of the item.
   *
   * @param int $order_id
   *   Order ID.
   * @param int $product_id
   *   Product ID.
   * @param int $quantity
   *   Quantity.
   *
   * @return int
   *   > 0 is the query was correctly executed.
   */
  public function updateQuantity($order_id, $product_id, $quantity) {
    $result = TRUE;
    $connection = Database::getConnection();
    $query = $connection->select('pos_items', 'i');
    $query->addField('i', 'quantity');
    $query->condition('i.order_id', $order_id);
    $query->condition('i.product_id', $product_id);
    $data = $query->execute();
    $item = $data->fetchAll(\PDO::FETCH_ASSOC);
    if ($delta = $item[0]['quantity'] - $quantity) {
      if ($item) {
        $query = $connection->update('pos_items');
        $query->fields(array(
          'quantity' => $quantity,
        ));
        $query->condition('order_id', $order_id);
        $query->condition('product_id', $product_id);
        if ($query->execute()) {
          \Drupal::service('pos_operations.stock')->addToStock(
            $product_id,
            $delta
          );
        }
        else {
          $result = FALSE;
        }
      }
      else {
        $result = FALSE;
      }
    }
    return $result;
  }


  public function getReportByState() {

    $connection = Database::getConnection();
    $query = $connection->select('pos_items', 'i');
    $query->addJoin('LEFT', 'pos_products', 'p', 'p.id = i.product_id');

    $query->addJoin('INNER', 'pos_orders', 'o', 'o.id = i.order_id');

    $query->addJoin('LEFT', 'pos_shippings', 's', 's.order_id = o.id');

    $query->addField('s', 'state');
    $query->addExpression('SUM(i.quantity)', 'quantity');
    $arr = getLastMonth(new \DateTime());
    // Filtering by date range.
    if ($arr[0] && $arr[1]) {
      $query->condition('o.created', [strtotime($arr[0]), strtotime($arr[1])], 'BETWEEN');
    }
    $query->groupBy('s.state');
    $query->orderBy('quantity', 'DESC');
    $query->range(0, 10);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $result;
  }



  /**
   * Check if there are items with quantity bigger than copy limit.
   *
   * @return bool
   *   True/False.
   */
  public function orderHasBulkItem($order_id) {
   $connection = Database::getConnection();
    $query = "SELECT pos_items.id, pos_items.quantity, pos_products.copy_limit FROM pos_items INNER JOIN pos_products ON pos_items.product_id = pos_products.id WHERE (pos_items.order_id = '$order_id') AND (pos_items.quantity > pos_products.copy_limit) AND (pos_products.copy_limit > 0)";
    $result = $connection->query($query);
    $result = $result->fetchAll();
    return $result;
  }

}
