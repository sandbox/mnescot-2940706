<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Product.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosProducts;

/**
 * Class CartItem.
 *
 * @package Drupal\pos_operations
 */
class Product {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Gets the product.
   *
   * @return array
   *   The product values.
   */
  public function getProduct($id) {
    $result = array();
    if ($product_entity = PosProducts::load($id)) {
      $result['id'] = $product_entity->id();
      $result['title'] = $product_entity->getTheValue('title');
      $result['description'] = $product_entity->getTheValue('description');
      $result['image'] = $product_entity->getTheValue('image');
      $result['file'] = $product_entity->getTheValue('file');
      $result['link_other_language'] = $product_entity->getTheValue('link_other_language');
      $result['topic'] = $product_entity->getTheValue('topics');
      $result['other_language'] = $product_entity->getTheValue('other_language');
      $result['stock'] = $product_entity->getTheValue('stock');
      $result['copy_limit'] = $product_entity->getTheValue('copy_limit');
      $result['sku'] = $product_entity->getTheValue('sku');
      $result['active'] = (bool) $product_entity->getTheValue('active');
    }
    return $result;
  }

  /**
   * Executes a query to gather data about products transactions.
   *
   * @param int $date_from
   *   Initial date for search range.
   * @param int $date_to
   *   End date for search range.
   * @param string $sku
   *   SKY to be searched for.
   * @param string $title
   *   Title to be searched for.
   *
   * @return array
   *   Gathered data.
   */
  public function getProductsTransaction($date_from = NULL, $date_to = NULL, $sku = NULL, $title = NULL) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_items', 'i');
    $query->addJoin('RIGHT', 'pos_products', 'p', 'p.id = i.product_id');
    $query->addField('p', 'sku');
    $query->addField('p', 'title');
    $query->addField('p', 'stock');
    $query->addField('p', 'copy_limit');
    $query->addExpression('COUNT(i.id)', 'transactions');
    $query->addExpression('SUM(i.quantity)', 'quantity');
    // Filtering by date range.
    if ($date_from && $date_to) {
      $query->condition('i.created', [$date_from, $date_to], 'BETWEEN');
    }
    elseif ($date_from) {
      $query->condition('i.created', $date_from, '>=');
    }
    elseif ($date_to) {
      $query->condition('i.created', $date_to, '<=');
    }
    // Filtering by SKU.
    if ($sku) {
      $query->condition('p.sku', "%$sku%", 'LIKE');
    }
    // Filtering by Title.
    if ($title) {
      $query->condition('p.title', "%$title%", 'LIKE');
    }
    $query->groupBy('p.id');
    $query->groupBy('p.sku');
    $query->groupBy('p.title');
    $query->groupBy('p.stock');
    $query->groupBy('p.copy_limit');
    $query->orderBy('quantity', 'DESC');
    $query->condition('p.hidden', FALSE);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }


  /**
   * Executes a query to gather data about products transactions.
   *
   * @param int $date_from
   *   Initial date for search range.
   * @param int $date_to
   *   End date for search range.
   * @param string $sku
   *   SKU to be searched for.
   * @param string $title
   *   Title to be searched for.
   *
   * @return array
   *   Gathered data.
   */
  public function getProductsList($sku = NULL, $title = NULL, $orderby = NULL, $direction = NULL) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_products', 'p');
    $query->addField('p', 'id');
    $query->addField('p', 'sku');
    $query->addField('p', 'title');
    $query->addField('p', 'copy_limit');

    // Filtering by SKU.
    if ($sku) {
      $query->condition('p.sku', "%$sku%", 'LIKE');
    }
    // Filtering by Title.
    if ($title) {
      $query->condition('p.title', "%$title%", 'LIKE');
    }
    $query->condition('p.hidden', FALSE);

    if($orderby) {
      $query->orderBy($orderby, $direction);
    }else{
      $query->orderBy('p.title', 'ASC');
    }
    //limit in the query
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(25);
    $result = $pager->execute()->fetchAll(\PDO::FETCH_ASSOC);
   // $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }


}
