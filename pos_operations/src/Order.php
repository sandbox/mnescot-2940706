<?php

/**
 * @file
 * Contains \Drupal\pos_operations\CartItem.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosSessions;
use Drupal\pos_entities\Entity\PosOrders;
use Drupal\pos_entities\Entity\PosItems;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class Order.
 *
 * @package Drupal\pos_operations
 */
class Order {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Creates a new Order and updates Session entity.
   *
   * @param object $session_entity
   *   Session Entity.
   * @param array $values
   *   Order fields values.
   *
   * @return int
   *   Order ID.
   */
  public function createNewOrder(&$session_entity, $values) {
    $order_entity = PosOrders::create();
    $order_entity->setStatus(0);
    $order_entity->setCustomerId($values['customer_id']);
    $order_entity->setEmail($values['email']);
    $order_entity->setCallerLanguage($values['caller_language']);
    $order_entity->setCallCenter($values['call_center']);
    $order_entity->setHowReceived($values['how_received']);
    $order_entity->setHowHeard($values['how_heard']);
    $order_entity->setJobTitle($values['job_title']);
    $order_entity->setOrganizationType($values['organization_type']);
    $order_entity->save();
    $order_id = $order_entity->id();
    $session_entity->setOrderId($order_id);
    $session_entity->save();
    return $order_id;
  }

  /**
   * Updates an Order.
   *
   * @param int $order_id
   *   Order ID.
   * @param array $values
   *   Order fields values.
   *
   * @return bool
   *   Flag indicating whether the Order was saved without errors.
   */
  public function updateOrder($order_id, $values) {
    $order_entity = PosOrders::load($order_id);
    $order_entity->setCustomerId($values['customer_id']);
    $order_entity->setEmail($values['email']);
    $order_entity->setCallerLanguage($values['caller_language']);
    $order_entity->setCallCenter($values['call_center']);
    $order_entity->setHowReceived($values['how_received']);
    $order_entity->setHowHeard($values['how_heard']);
    $order_entity->setJobTitle($values['job_title']);
    $order_entity->setOrganizationType($values['organization_type']);
    return $order_entity->save();
  }

  /**
   * Creates the order's items.
   *
   * @return bool/int
   *   Order ID if everything is OK or FALSE if fail.
   */
  public function createItems() {
    $session_id = \Drupal::service('pos_operations.session')->getId();
    $session_entity = PosSessions::load($session_id);
    $order_id = $session_entity->getOrderId();
    $items = \Drupal::service('pos_operations.cart_item')->getCartItems($review = FALSE);
    if ($session_entity && $items) {
      foreach ($items as $item) {
        $item['order_id'] = $order_id;
        $order_item = PosItems::create($item);
        if (!$order_item->save()) {
          $order_id = FALSE;
          break;
        }
      }

    }
    return $order_id;
  }

  /**
   * Sets the order status.
   *
   * @param int $order_id
   *   Order ID.
   * @param string $status
   *   Order status.
   * @param int $batch_owner_id
   *   Batch owner ID.
   * @param string $comment
   *   Comment explaining the reason of the status changing.
   *
   * @TODO: Transfer the adding of the timestamp to Historic services.
   */
  public function setOrderStatus($order_id, $status, $batch_owner_id = NULL, $comment = NULL) {
    $connection = Database::getConnection();
    $query = $connection->update('pos_orders');
    $fields = [
      'status' => $status,
    ];
    if ($status == 'bulk') {
      $fields['is_bulk'] = TRUE;
    }
    if ($batch_owner_id) {
      $fields['batch_owner_id'] = $batch_owner_id;
      $fields['batched'] = time();
    }
    $query->fields($fields);
    $query->condition('id', $order_id, '=');
    if ($query->execute()) {
      \Drupal::service('pos_operations.historic')->record($order_id, $status, $comment);
    }
  }

  /**
   * Sets the order status to pending.
   *
   * @param int $order_id
   *   Order ID.
   */
  public function setOrderStatusToPending($order_id) {
    $this->setOrderStatus($order_id, 'pending');
  }

  /**
   * Sets the order status to bulk.
   *
   * @param int $order_id
   *   Order ID.
   */
  public function setOrderStatusToBulk($order_id) {
    $this->setOrderStatus($order_id, 'bulk');
  }

  /**
   * Sets the order status to canceled.
   *
   * @param int $order_id
   *   Order ID.
   */
  public function setOrderStatusToCanceled($order_id) {
    $this->setOrderStatus($order_id, 'canceled');
  }


  /**
   * Sets the order status to returned.
   *
   * @param int $order_id
   *   Order ID.
   * @param string $comment
   *   Comment explaining the reason of the status changing.
   */
  public function setOrderStatusToReturned($order_id, $comment) {
    $this->setOrderStatus($order_id, 'returned', NULL, $comment);
  }

  /**
   * Set Order status to "in progress" and assign the batch to a user.
   *
   * @param int $order_id
   *   Order ID.
   * @param int $batch_owner_id
   *   Batch owner ID.
   */
  public function setOrderStatusToInProgress($order_id, $batch_owner_id = NULL) {
    $comment = \Drupal::service('pos_operations.historic')->getLastComment($order_id);
    $this->setOrderStatus($order_id, 'progress', $batch_owner_id, $comment);
  }

  /**
   * Gets the Order with no formatted values.
   *
   * @param int $id
   *   Order ID.
   *
   * @return array
   *   The Order values.
   */
  public function getRawOrder($id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->fields('o');
    $query->condition('o.id', $id);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
    return $result;
  }

  /**
   * Gets a complete info about the order and its items.
   *
   * @param int $order_id
   *   Order ID.
   *
   * @return array
   *   Info about the order and its items.
   */
  public function getOrder($order_id) {
    if (!$result = $this->getRawOrder($order_id)) {
      return [];
    }
    $result['caller_language'] = _pos_utils_get_term_name($result['caller_language']);
    $result['call_center'] = _pos_utils_get_term_name($result['call_center']);
    $result['how_received'] = _pos_utils_get_term_name($result['how_received']);
    $result['how_heard'] = _pos_utils_get_term_name($result['how_heard']);
    $result['job_title'] = _pos_utils_get_term_name($result['job_title']);
    $result['organization_type'] = _pos_utils_get_term_name($result['organization_type']);
    $result['status'] = $this->getStatusName($result['status']);
    $result['batched'] = date('m-d-Y H:i:s', $result['batched']);
    $result['created'] = date('m-d-Y H:i:s', $result['created']);
    $result['changed'] = date('m-d-Y H:i:s', $result['changed']);
    if ($result['customer_id']) {
      $result['profile'] = \Drupal::service('pos_operations.profile')->getProfile($result['customer_id']);
    }
    $result['shipping'] = \Drupal::service('pos_operations.shipping')->getShipping($order_id);
    $result['items'] = \Drupal::service('pos_operations.item')->getItems($order_id);
    $result['historic'] = \Drupal::service('pos_operations.historic')->getLastRecord($order_id);
    $result['historic']['comment'] = str_replace("\r\n", '<br>', $result['historic']['comment']);
    return $result;
  }

  /**
   * Gets all Orders with the given status.
   *
   * @param string $status
   *   Order status.
   * @param int $batch_owner_id
   *   Batch owner ID.
   *
   * @return array
   *   The Order values.
   */
  public function getRawOrderByStatus($status, $batch_owner_id = NULL, $isbulk = NULL) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->fields('o');
    $query->condition('o.status', $status);
    if(isset($isbulk))
    {
      $query->condition('o.is_bulk', $isbulk);
    }
    if ($batch_owner_id) {
      $query->condition('o.batch_owner_id', $batch_owner_id);
    }
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Gets all orders issues to a given customer.
   *
   * @param int $customer_id
   *   Customer ID.
   *
   * @return array
   *   List with all orders.
   */
  public function getOrderByProfile($customer_id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->addJoin('INNER', 'pos_order_status', 's', 's.id = o.status');
    $query->addField('o', 'id');
    $query->addField('o', 'created');
    $query->addField('o', 'changed');
    $query->addField('s', 'name', 'status');
    $query->condition('o.customer_id', $customer_id);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($result as &$order) {
      //Add link to the order id
      $link = Link::fromTextAndUrl(t($order['id']), Url::fromUri('internal:/order/'.$order['id'],array()))->toString();
      $order['id'] = $link;
      $order['created'] = date('m-d-Y H:i:s', $order['created']);
      $order['changed'] = date('m-d-Y H:i:s', $order['changed']);
    }
    return $result;
  }


  /**
   * Gets all Orders with the given status.
   *
   * @param string $status
   *   Order status.
   * @param int $batch_owner_id
   *   Batch owner ID.
   *
   * @return array
   *   Info about the order and their shipment.
   */
  public function getOrdersByStatus($status, $batch_owner_id = NULL, $isbulk = NULL) {
    $result = [];
    $orders = $this->getRawOrderByStatus($status, $batch_owner_id, $isbulk);
    foreach ($orders as &$order) {
      $order['status'] = $this->getStatusName($order['status']);
      $order['batched'] = date('m-d-Y H:i:s', $order['batched']);
      $order['created'] = date('m-d-Y H:i:s', $order['created']);
      $order['changed'] = date('m-d-Y H:i:s', $order['changed']);
      $order['shipping'] = \Drupal::service('pos_operations.shipping')->getShipping($order['id']);
      $result[] = $order;
    }
    return $result;
  }

  /**
   * Get the description of order's status.
   *
   * @param string $id
   *   ID of the status.
   *
   * @return string
   *   The description of the status.
   */
  private function getStatusName($id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_order_status', 's');
    $query->fields('s');
    $query->condition('s.id', $id);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
    return $result['name'];
  }

  /**
   * Get an Order as an entity object structure.
   *
   * @param string $id
   *   Order ID.
   *
   * @return string
   *   Order object.
   */
  public function getOrderEntity($id) {
    return PosOrders::load($id);
  }

  /**
   * Get all Order status.
   *
   * @return array
   *   List of all Order status.
   */
  public function getStatusAsSelectOptions() {
    $result = [];
    $connection = Database::getConnection();
    $query = $connection->select('pos_order_status', 's');
    $query->fields('s');
    $data = $query->execute();
    foreach ($data->fetchAll(\PDO::FETCH_ASSOC) as $item) {
      $result[$item['id']] = $item['name'];
    }
    return $result;
  }

  /**
   * Restore an Order that was canceled.
   *
   * @param string $order_id
   *   Order ID.
   */
  public function restoreCanceledOrder($order_id, $status, $comment = NULL) {
    foreach (\Drupal::service('pos_operations.item')->getItems($order_id) as $item) {
      \Drupal::service('pos_operations.stock')->subtractFromStock($item['product']['id'], $item['quantity']);
    }
    $this->setOrderStatus($order_id, $status, NULL, $comment);
  }

  /**
   * Restore an Order that was canceled.
   *
   * @param string $order_id
   *   Order ID.
   */
  public function approveBulkOrder($order_id, $status, $comment = NULL) {
    foreach (\Drupal::service('pos_operations.item')->getItems($order_id) as $item) {
      \Drupal::service('pos_operations.stock')->subtractFromStock($item['product']['id'], $item['quantity']);
    }
    $this->setOrderStatus($order_id, $status, NULL, $comment);
  }

  /**
   * Cancel an Order.
   *
   * @param string $order_id
   *   Order ID.
   *
   * @TODO: Review parameters. Is really necessary $status?
   */
  public function cancelOrder($order_id, $status, $comment = NULL) {
    foreach (\Drupal::service('pos_operations.item')->getItems($order_id) as $item) {
      \Drupal::service('pos_operations.stock')->addToStock($item['product']['id'], $item['quantity']);
    }
    $this->setOrderStatus($order_id, $status, NULL, $comment);
  }

  /**
   * Bulk Order do not decrease stock when bulk order created. Restore stock.
   *
   * @param string $order_id
   *   Order ID.
   *
   *
   */
  public function bulkOrder($order_id, $status, $comment = NULL) {
    foreach (\Drupal::service('pos_operations.item')->getItems($order_id) as $item) {
      \Drupal::service('pos_operations.stock')->addToStock($item['product']['id'], $item['quantity']);
    }
    $this->setOrderStatus($order_id, $status, NULL, $comment);
  }

  /**
   * Counts number of Orders under a specific status.
   *
   * @param string $status
   *   Order Status. If NULL all Orders will be included.
   *
   * @return int
   *   Amount of orders.
   */
  public function countOrders($status = NULL) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_orders', 'o');
    $query->addExpression('COUNT(o.id)', 'total');
    if ($status == 'checkout') {
      $query->condition('o.status', 0);
    }
    elseif ($status) {
      $query->condition('o.status', $status);
    }
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
    return $result['total'];
  }

  /**
   * Sets the status of multiple orders at once.
   *
   * @param string $status
   *   The new status.
   * @param array $ids
   *   The IDs of the Orders.
   */
  public function massStatusSetting($status = NULL, $ids = array()) {
    $connection = Database::getConnection();
    $query = $connection->update('pos_orders');
    $fields = ['status' => $status];
    $query->fields($fields);
    $query->condition('id', $ids, 'IN');
    if ($query->execute()) {
      foreach ($ids as $id) {
        \Drupal::service('pos_operations.historic')->record($id, $status);
      }
    }
  }

  /**
   * Set orders status to expired.
   *
   * @param array $ids
   *   The IDs of the Orders.
   */
  public function setOrdersToExpired($ids = array()) {
    $this->massStatusSetting('expired', $ids);
  }


}
