<?php

/**
 * @file
 * Contains \Drupal\pos_operations\StockActivity.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;

/**
 * Class StockActivity.
 *
 * @package Drupal\pos_operations
 */
class StockActivity {

  /**
   * Adds quantity to product's stock.
   *
   * @param int $product_id
   *   Product ID.
   * @param int $quantity
   *   Quantity being added.
   *
   * @return int/obj
   *   The result of the update operations.
   */
  public function addToStock($product_id, $quantity) {
    return $this->updateStock($product_id, $quantity);
  }

  /**
   * Subtracts quantity from product's stock.
   *
   * @param int $product_id
   *   Product ID.
   * @param int $quantity
   *   Quantity being subtracted.
   *
   * @return int/obj
   *   The result of the update operations.
   */
  public function subtractFromStock($product_id, $quantity) {
    $quantity *= -1;
    return $this->updateStock($product_id, $quantity);
  }

  /**
   * Updates stock quantity.
   *
   * @param int $product_id
   *   Product ID.
   * @param int $quantity
   *   Quantity being added or subtracted.
   *
   * @return int/obj
   *   The result of the update operations.
   */
  public function updateStock($product_id, $quantity) {
    $connection = Database::getConnection();
    $query = $connection->update('pos_products');
    $query->expression('stock', 'stock + :quantity', array(':quantity' => $quantity));
    $query->condition('id', $product_id, '=');
    return $query->execute();
  }

  /**
   * Gets the stock quantity.
   *
   * @param int $product_id
   *   Product ID.
   *
   * @return int
   *   Stock quantity.
   */
  public function getStock($product_id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_products', 'p');
    $query->addField('p', 'stock');
    $query->condition('id', $product_id, '=');
    if ($result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
      return $result[0]['stock'];
    }
    else {
      return NULL;
    }
  }

  /**
   * Returns to stock quantities of items that belong to expired orders.
   *
   * @param array $items
   *   Products IDs and quantities.
   */
  public function returnItemsFromExpiredOrders($items = array()) {
    foreach ($items as $item) {
      $this->addToStock($item['id'], $item['quantity']);
    }

  }

}
