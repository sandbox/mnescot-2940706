<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Status.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;

/**
 * Class Status.
 *
 * @package Drupal\pos_operations
 */
class Status {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Gets all status.
   *
   * @return array
   *   Status IDs and Names.
   */
  public function getStatusList() {
    $connection = Database::getConnection();
    $query = $connection->select('pos_order_status', 's');
    $query->fields('s');
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Get the status name.
   *
   * @param string $id
   *   Status ID;
   *
   * @return string
   *   Status name.
   */
  public function getStatusName($id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_order_status', 's');
    $query->addField('s', 'name');
    $query->condition('id', $id);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $result[0]['name'];
  }

}
