<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Session.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosSessions;
use DateTime;

/**
 * Class CartItem.
 *
 * @package Drupal\pos_operations
 */
class Session {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Get the session id specific for POS transactions.
   *
   * @return string
   *   Session ID.
   */
  public function getId() {
    $result = NULL;
    if (\Drupal::currentUser()->isAnonymous()) {
      if (!isset($_SESSION['pos_session_id'])) {
        $_SESSION['pos_session_id'] = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(43 / strlen($x)))), 1, 42);
      }
      $result = $_SESSION['pos_session_id'];
    }
    else {
      $result = \Drupal::service('session_manager')->getId();
    }
    return $result;
  }

  /**
   * Checks if session has a record.
   *
   * @param int $customer_id
   *   Customer ID.
   *
   *   If it has, updates the changed date.
   *   If it hasn't, creates one.
   */
  public function checkSession($customer_id = 0) {
    $session_id = $this->getId();
    $entity = PosSessions::load($session_id);
    if ($entity) {
      $entity->setChangedTime(time());
      $entity->save();
    }
    else {
      $entity = PosSessions::create();
      $entity->setCustomerId($customer_id);
      $entity->save();
    }
  }

  /**
   * Remove the the session id specific for POS transactions.
   */
  public function clearSession() {
    $session_id = $this->getId();
    $connection = Database::getConnection();
    $query = $connection->delete('pos_sessions');
    $query->condition('id', $session_id, '=');
    $result = $query->execute();
    \Drupal::entityTypeManager()->getStorage('pos_sessions')->resetCache();
    return $result;
  }

  /**
   * Check whether the session is still on.
   *
   * @return bool
   *   Whether the session is still on.
   */
  public function sessionIsOff() {
    $session_id = $this->getId();
    if ($entity = PosSessions::load($session_id)) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Counts number of living sessions.
   *
   * @return int
   *   Amount of sessions.
   */
  public function countSessions() {
    $connection = Database::getConnection();
    $query = $connection->select('pos_sessions', 's');
    $query->addExpression('COUNT(s.id)', 'total');
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
    return $result['total'];
  }

  /**
   * Get rid of old sessions.
   */
  public function clearExpiredSessions() {
    $session_duration = \Drupal::config('pos_operations.sessions_configuration')->get('session_duration');
    if (!$session_duration || $session_duration < 1) {
      $session_duration = 4;
    }
    $date = new DateTime();
    $date->modify('-' . $session_duration . ' hours');
    $date_limit = $date->getTimestamp();
    $this->executeClearSessions($date_limit);
  }

  /**
   * Get rid of all sessions.
   */
  public function clearAllSessions() {
    $this->executeClearSessions();
  }

  /**
   * Common code for clearing POS sessions execution.
   *
   * @param int $date_limit
   *   Date limit for the sessions.
   */
  private function executeClearSessions($date_limit = 0) {
    if ($initiated_orders = $this->getInitiatedOrders($date_limit)) {
      $items_to_be_returned = $this->getAllItemsToBeReturned($initiated_orders);
      \Drupal::service('pos_operations.order')->setOrdersToExpired($initiated_orders);
      \Drupal::service('pos_operations.stock')->returnItemsFromExpiredOrders($items_to_be_returned);
    }
    $this->removeSessions($date_limit);
    $this->removeCartItems();
  }

  /**
   * Remove the sessions.
   *
   * @param int $date_limit
   *   If provided, only sessions older then the $date_limit will be removed.
   */
  private function removeSessions($date_limit = 0) {
    $connection = Database::getConnection();
    $query = $connection->delete('pos_sessions');
    if ($date_limit) {
      $query->condition('changed', $date_limit, '<');
    }
    $query->execute();
  }

  /**
   * Remove all Cart items that became orphan after the removal of the Sessions.
   */
  public function removeCartItems() {
    $connection = Database::getConnection();
    $query = "DELETE c FROM pos_cart AS c LEFT JOIN pos_sessions AS s ON s.id = c.session_id WHERE s.id IS NULL";
    $connection->query($query);
  }

  /**
   * Get all orders that were initiated by sessions.
   *
   * @param int $date_limit
   *   If provided, only sessions older then the $date_limit will be pulled.
   *
   * @return array
   *   Orders IDs.
   */
  private function getInitiatedOrders($date_limit = 0) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_sessions', 's');
    $query->addField('s', 'order_id');
    if ($date_limit) {
      $query->condition('changed', $date_limit, '<');
    }
    $query->condition('order_id', NULL, 'IS NOT NULL');
    $result = $query->execute()->fetchAllKeyed();
    return array_keys($result);
  }

  /**
   * Get items in the initiated orders.
   *
   * @param array $initiated_orders
   *   The orders IDs.
   *
   * @return array
   *   Items IDs.
   */
  private function getAllItemsToBeReturned($initiated_orders) {
    $result = [];
    foreach ($initiated_orders as $order_id) {
      foreach (\Drupal::service('pos_operations.item')->getRawItems($order_id) as $item) {
        $result[] = [
          'id' => $item['product_id'],
          'quantity' => $item['quantity'],
        ];
      }
    }
    return $result;
  }

  /**
   * Get the Order ID of the current session.
   *
   * @return int
   *   The Order id or false.
   */
  public function getSessionOrderId() {
    $result = FALSE;
    $session_id = $this->getId();
    $connection = Database::getConnection();
    $query = $connection->select('pos_sessions', 's');
    $query->condition('s.id', $session_id);
    $query->addField('s', 'order_id');
    if ($result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
      $result = $result[0]['order_id'];
    }
    return $result;
  }

}
