<?php
/**
 * @file
 * Contains \Drupal\pos_operations\Shipping.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Condition;
use Drupal\pos_entities\Entity\PosShippings;
use Drupal;

class Shipping {
  /**
   * Creates or updates shipping ifo for a given order.
   *
   * @param array $values
   *   Order and Shipping values.
   */
  public function updateShipping($values) {
    $shipping_values = array(
      'session_id' => $session_id = \Drupal::service('pos_operations.session')->getId(),
      'email' => $values['email'],
      'phone' => $values['phone'],
      'phoneext' => $values['phoneext'],
//      'customer_id' => $values['xxxxxx'],
      'order_id' => $values['order_id'],
      'first_name' => $values['first_name'],
      'last_name' => $values['last_name'],
      'honorific' => $values['honorific'],
      'organization' => $values['organization'],
      'address_1' => $values['address_1'],
      'address_2' => $values['address_2'],
      'city' => $values['city'],
      'state' => $values['state'],
      'zip' => $values['zip'],
    );
    $query = Drupal::service('entity.query')
      ->get('pos_shippings')
      ->condition('order_id', $values['order_id']);
    if ($shipping_id = $query->execute()) {
      $shipping_id = array_shift($shipping_id);
      $shipping_entity = PosShippings::load($shipping_id);
    }
    else {
      $shipping_entity = PosShippings::create();
    }
    foreach ($shipping_values as $key => $value) {
      $shipping_entity->set($key, $value);
    }
    $shipping_entity->save();
  }

  /**
   * Sets a query condition to search for a given ID.
   *
   * @param int $id
   *   The shipping ID.
   *
   * @return array
   *   An associative array with the shipping info.
   */
  public function getShippingById($id) {
    $condition = new Condition('AND');
    $condition->condition('id', $id);
    $result = $this->getQueryResult($condition);
    return $result;
  }

  /**
   * Sets a query condition to search for a given order ID.
   *
   * @param int $order_id
   *   The order ID.
   *
   * @return array
   *   An associative array with the shipping info.
   */
  public function getShippingByOrderId($order_id) {
    $condition = new Condition('AND');
    $condition->condition('order_id', $order_id);
    $result = $this->getQueryResult($condition);
    return $result;
  }

  /**
   * Sets a query condition to search for a given customer ID.
   *
   * @param int $customer_id
   *   The customer ID.
   *
   * @return array
   *   An associative array with the shipping info.
   */
  public function getShippingByCustomerId($customer_id) {
    $condition = new Condition('AND');
    $condition->condition('customer_id', $customer_id);
    $result = $this->getQueryResult($condition);
    return $result;
  }

  /**
   * Executes a query in the Shippings entity.
   *
   * @param Drupal\Core\Database\Query\Condition $condition
   *   Query condition.
   *
   * @return array
   *   An associative array with the shipping info.
   */
  private function getQueryResult(Drupal\Core\Database\Query\Condition $condition) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_shippings', 's');
    $query->fields('s');
    $query->condition($condition);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    $result = array_shift($result);
    return $result;
  }

  /**
   * Gets the shipping with formatted values.
   *
   * @param int $order_id
   *   Shipping ID.
   *
   * @return array
   *   The shipping values.
   */
  public function getShipping($order_id) {
    $result = $this->getShippingByOrderId($order_id);
    $result['honorific'] = _pos_utils_get_term_name($result['honorific']);
    $result['created'] = date('d-m-Y H:i:s', $result['created']);
    $result['changed'] = date('d-m-Y H:i:s', $result['changed']);
    return $result;
  }


  /**
   * Get an Shipping as an entity object structure.
   *
   * @param string $id
   *   Shipping ID.
   *
   * @return string
   *   Shipping object.
   */
  public function getShippingEntity($order_id) {
    $entity_id = \Drupal::entityQuery('pos_shippings')
      ->condition('order_id', $order_id)
      ->execute();
    $entity_id = array_shift($entity_id);
    return PosShippings::load($entity_id);
  }


}