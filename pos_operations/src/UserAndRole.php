<?php

/**
 * @file
 * Contains \Drupal\pos_operations\UserAndRole.
 */

namespace Drupal\pos_operations;

use Drupal\user\Entity\User;

/**
 * Class Status.
 *
 * @package Drupal\pos_operations
 */
class UserAndRole {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Gets all users that belongs to the warehouse.
   *
   * @return array
   *   List of the users.
   */
  public function getWarehouseStaff() {
    $config = \Drupal::config('pos_roles.settings');
    $warehouse_staff_name = $config->get('warehouse_staff.name');
    $ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', $warehouse_staff_name)
      ->execute();
    $result = User::loadMultiple($ids);
    return $result;
  }

  /**
   * Gets all users that belongs to the warehouse formatted as select options.
   *
   * @return array
   *   List of the users.
   */
  public function getWarehouseStaffAsSelectOptions() {
    $result = [];
    $users = $this->getWarehouseStaff();
    foreach ($users as $uid => $user) {
      $alias = $user->field_alias->getValue();
      $result[$uid] = $alias[0]['value'];
    }
    return $result;
  }

  /**
   * Gets the name of the batch owner.
   *
   * @param string $batch_owner_id
   *   ID of the batch owner.
   *
   * @return string
   *   Batch owner's name.
   */
  public function getBatchOwnerName($batch_owner_id) {
    if ($owner = User::load($batch_owner_id)) {
      $result = $owner->field_alias->getValue();
      return $result[0]['value'];
    }
    else {
      return NULL;
    }

  }

  /**
   * Gets the name of the batch owner.
   *
   * @param string NA
   *
   * @return string
   *   Current user's name.
   */
  public function getCurrentUserName() {
    if ($user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id())) {
     // retrieve field data from that user
      $name = $user->get('name')->value;
      $uid= $user->get('uid')->value;
      return $name;
    }
    else {
      return NULL;
    }

  }

}
