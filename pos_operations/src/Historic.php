<?php

/**
 * @file
 * Contains \Drupal\pos_operations\Historic.
 */

namespace Drupal\pos_operations;

use Drupal\Core\Database\Database;
use Drupal\pos_entities\Entity\PosHistoric;

/**
 * Class Status.
 *
 * @package Drupal\pos_operations
 */
class Historic {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Creates a historic record.
   *
   * @param int $order_id
   *   Order ID.
   * @param string $status
   *   Order Status.
   * @param string $comment
   *   Comment.
   */
  public function record($order_id, $status, $comment = NULL) {
    $last_record = $this->getLastRecord($order_id);
    if ($status != $last_record['status']) {
      if ($comment) {
        $curr_user_name = \Drupal::service('pos_operations.user_and_role')->getCurrentUserName();
        $comment .= "\r\n" . $curr_user_name." ". date('l\, jS \of F Y\, h:i:s A');
      }
      $historic = PosHistoric::create();
      $historic->setOrderId($order_id);
      $historic->setStatus($status);
      $historic->setComment($comment);
      $historic->save();
    }
  }

  /**
   * Get the last record of an Order.
   *
   * @param int $order_id
   *   Order ID.
   *
   * @return array
   *   Array containing Status and Comment.
   */
  public function getLastRecord($order_id) {
    $connection = Database::getConnection();
    $query = $connection->select('pos_historic', 'h');
    $query->addField('h', 'status');
    $query->addField('h', 'comment');
    $query->condition('h.order_id', $order_id);
    $query->orderBy('h.created', 'DESC');
    $query->range(0, 1);
    $data = $query->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return array_shift($result);
  }

  /**
   * Get the last comment of an Order.
   *
   * @param int $order_id
   *   Order ID.
   *
   * @return string
   *   Last comment.
   */
  public function getLastComment($order_id) {
    if ($record = $this->getLastRecord($order_id)) {
      return $record['comment'];
    }
    else {
      return NULL;
    }
  }

}
