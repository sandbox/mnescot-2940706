<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\ConfirmSetStatus.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class ConfirmSetStatus.
 *
 * @package Drupal\pos_forms\Form
 */
class ConfirmSetStatus extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_set_status';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $parameters = NULL) {

    $url = Url::fromRoute('pos_operations.mass_set_status_execute', ['parameters' => $parameters]);
    $form['#action'] = $url->toString();

    $parameters = unserialize($parameters);

    $form['orders_ids'] = [
      '#type' => 'markup',
      '#markup' => implode(', ', $parameters['ids']),
      '#prefix' => '<div id="pos-order-ids-list">',
      '#suffix' => '</div>',
    ];
    $form['confirm'] = [
      '#type' => 'button',
      '#value' => $this->t('Confirm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
