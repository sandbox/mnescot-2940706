<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\ReturnedOrders.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Class ReturnedOrders.
 *
 * @package Drupal\pos_forms\Form
 */
class ReturnedOrders extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'returned_orders';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = $this->t('Returned Orders');

    $header = [
      'id' => $this->t('Order ID'),
      'name' => $this->t('Name'),
      'view' => $this->t('View'),
      'edit' => $this->t('Edit'),
      'created' => $this->t('Created'),
      'changed' => $this->t('Changed'),
    ];

    $options = [];

    foreach (\Drupal::service('pos_operations.order')->getOrdersByStatus('returned') as $order) {

      $param = [
        'id' => $order['id'],
      ];
      $view_url = Url::fromRoute('pos_orders_page.view_order_content', $param);
      $view_url->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);

      $current_path = \Drupal::service('path.current')->getPath();
      $destination = explode('/', $current_path);
      $destination = array_filter($destination);
      if (count($destination) > 1) {
        $destination = serialize($destination);
      }
      else {
        $destination = array_shift($destination);
      }
      $param = [
        'order_id' => $order['id'],
        'destination_args' => $destination,
      ];
      $edit_url = Url::fromRoute('order.edit_with_destination', $param);

      $options[$order['id']] = [
        'id' => $order['id'],
        'name' => $order['shipping']['first_name'] . chr(32) . $order['shipping']['last_name'],
        'view' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'View',
            '#title_display' => 'invisible',
            '#url' => $view_url,
          ],
        ],
        'edit' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'Edit',
            '#title_display' => 'invisible',
            '#url' => $edit_url,
          ],
        ],
        'created' => $order['created'],
        'changed' => $order['changed'],
      ];
    }

    $form['orders_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No Orders found for this status.'),
      '#weight' => 10,
    );

    $form['print'] = [
      '#type' => 'detail',
      '#weight' => 95,
    ];

    $form['print']['invoice'] = [
      '#type' => 'submit',
      '#name' => 'print_invoice',
      '#value' => $this->t('Print Invoice'),
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_orders = $this->getSelectedOrderIds($form_state);
    \Drupal::service('pos_operations.generate_pdf')->invoice($selected_orders, TRUE);
  }

  /**
   * Extracts the IDs of the Orders selected in the tableselect.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   Selected IDs.
   */
  private function getSelectedOrderIds(FormStateInterface $form_state) {
    $result = [];
    foreach ($form_state->getValue('orders_table') as $order_id => $checked) {
      if ($checked) {
        $result[] = $order_id;
      }
    }
    return $result;
  }

}
