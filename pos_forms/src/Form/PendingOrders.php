<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\PendingOrders.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Class PendingOrders.
 *
 * @package Drupal\pos_forms\Form
 */
class PendingOrders extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pending_orders';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = $this->t('Pending Orders');

    $header = [
      'id' => $this->t('Order ID'),
      'name' => $this->t('Name'),
      'view' => $this->t('View'),
      'edit' => $this->t('Edit'),
      'created' => $this->t('Created'),
      'changed' => $this->t('Changed'),
      'bulk' => $this->t('Is this a bulk order?'),
    ];

    $options = [];
    $bulk = \Drupal::request()->query->get('bulk');

    foreach (\Drupal::service('pos_operations.order')
               ->getOrdersByStatus('pending', NULL, $bulk) as $order) {
      $param = [
        'id' => $order['id'],
      ];
      $view_url = Url::fromRoute('pos_orders_page.view_order_content', $param);
      $view_url->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);

      $current_path = \Drupal::service('path.current')->getPath();
      $destination = explode('/', $current_path);
      $destination = array_filter($destination);
      if (count($destination) > 1) {
        $destination = serialize($destination);
      }
      else {
        $destination = array_shift($destination);
      }
      $param = [
        'order_id' => $order['id'],
        'destination_args' => $destination,
      ];
      $edit_url = Url::fromRoute('order.edit_with_destination', $param);

      $options[$order['id']] = [
        'id' => $order['id'],
        'name' => $order['shipping']['first_name'] . chr(32) . $order['shipping']['last_name'],
        'view' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'View',
            '#title_display' => 'invisible',
            '#url' => $view_url,
          ],
        ],
        'edit' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'Edit',
            '#title_display' => 'invisible',
            '#url' => $edit_url,
          ],
        ],
        'created' => $order['created'],
        'changed' => $order['changed'],
        'bulk' => (isset($order['is_bulk']) && $order['is_bulk'] == 1 ? 'yes' : 'no'),
      ];
    }

    $form['orders_table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No Order found for this status.'),
      '#weight' => 10,
    ];

    $staff = \Drupal::service('pos_operations.user_and_role')
      ->getWarehouseStaffAsSelectOptions();

    $staff = ['_none' => '- Select -'] + $staff;
    $form['staff'] = [
      '#type' => 'select',
      '#title' => $this->t('Assign batch to'),
      '#options' => $staff,
      '#weight' => 20,
    ];

    $bulk_options = ['_none' => '- Select -'];
    $bulk_options = $bulk_options + ['1' => 'Yes'];
    $bulk_options = $bulk_options + ['0' => 'No'];

    $form['bulk'] = [
      '#type' => 'select',
      '#name' => 'bulk',
      '#title' => $this->t('Is Bulk?'),
      '#options' => $bulk_options,
      '#weight' => 1,
      '#default_value' => (isset($bulk) ? $bulk : '_none'),
      '#prefix' => ' <div class="bulk_filter"',
      '#suffix' => ' </div>',
    ];

    $form['execute'] = [
      '#type' => 'submit',
      '#name' => 'reassign',
      '#value' => $this->t('Assign'),
      '#weight' => 90,
    ];

    $form['filter'] = [
      '#type' => 'submit',
      '#name' => 'filter',
      '#value' => $this->t('Search'),
      '#weight' => 2,

    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    switch ($triggering_element['#name']) {
      case 'reassign':
        $staff = $form_state->getValue('staff');
        if ($staff != '_none') {
          foreach ($form_state->getValue('orders_table') as $order_id => $checked) {
            if ($checked) {
              \Drupal::service('pos_operations.order')
                ->setOrderStatusToInProgress($order_id, $form_state->getValue('staff'));
            }
          }
        }
        else {
          drupal_set_message(t('"Assign batch to" is a required field'), 'error');
        }
        break;

      case 'filter':
        if ($form_state->getValue('bulk') == '_none') {
          $url = Url::fromUri('internal:/pending-orders');
        }
        else {
          $url = Url::fromUri('internal:/pending-orders' . '?bulk=' . $form_state->getValue('bulk'));
        }
        $form_state->setRedirectUrl($url);
        break;

    }

    return [];
  }


}
