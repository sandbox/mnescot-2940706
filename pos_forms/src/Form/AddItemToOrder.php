<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\AddItemToOrder.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;


/**
 * Class AddItemToOrder.
 *
 * @package Drupal\pos_forms\Form
 */
class AddItemToOrder extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_item_to_order';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $param = \Drupal::request()->query->all();

    $form['search_for'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search for'),
      '#description' => $this->t('Will be searched in SKU and title'),
      '#maxlength' => 64,
    ];

    $form['search_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#submit' => array('::addResults'),
      '#ajax' => [
        'callback' => '::searchButtonCallback',
        'wrapper' => 'results-wrapper',
      ],
    ];

    $form['results_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'results-wrapper'],
    ];

    if ($results_rows = $form_state->get('results_rows')) {
      foreach ($results_rows as $result_row) {
        $row_container_name = 'row_container_' . $result_row['id'];
        $form['results_container'][$row_container_name] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['customer-search-results-row-wrapper']],
        ];
        $form['results_container'][$row_container_name]['choose_button_' . $result_row['id']] = [
          '#type' => 'submit',
          '#name' => 'choose-button-' . $result_row['id'],
          '#value' => $this->t('Choose'),
          '#parameter' => [
            'order_id' => $param['order_id'],
            'product_id' => $result_row['id'],
            'sku' => $result_row['sku'],
            'title' => $result_row['title'],
          ],
        ];
        $form['results_container'][$row_container_name]['sku_' . $result_row['id']] = [
          '#type' => 'markup',
          '#markup' => $result_row['sku'],
          '#prefix' => '<span class="customer-search-results-field-wrapper">',
          '#suffix' => '</span>',
        ];
        $form['results_container'][$row_container_name]['title_' . $result_row['id']] = [
          '#type' => 'markup',
          '#markup' => $result_row['title'],
          '#prefix' => '<span class="customer-search-results-field-wrapper">',
          '#suffix' => '</span>',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (preg_match('/choose-button-/', $triggering_element['#name'])) {
      $order_id = $triggering_element['#parameter']['order_id'];
      $product_id = $triggering_element['#parameter']['product_id'];
      $sku = $triggering_element['#parameter']['sku'];
      $title = $triggering_element['#parameter']['title'];
      $result = \Drupal::service('pos_operations.item')->addItem($order_id, $product_id, $sku);
      if ($result) {
        drupal_set_message($this->t('Item %title added to Order %order_id', [
          '%title' => $title,
          '%order_id' => $order_id,
        ]));
      }
      elseif ($result === NULL) {
        drupal_set_message($this->t('Item %title already exists in Order %order_id', [
          '%title' => $title,
          '%order_id' => $order_id,
        ]), 'warning');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addResults(array &$form, FormStateInterface $form_state) {
    $search_for = trim($form_state->getValue('search_for'));
    $results = NULL;
    if (!empty($search_for)) {
      $results = $this->getResults($search_for);
    }
    $form_state->set('results_rows', $results);
    $form_state->setRebuild();
  }

  /**
   * Implements the submit handler for the ajax call.
   *
   * @param array $form
   *   Render array representing from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array
   *   Form element array.
   */
  public function searchButtonCallback(array &$form, FormStateInterface $form_state) {
    return $form['results_container'];
  }

  /**
   * Search for customer based on a search term.
   *
   * @param string $search_for
   *   Search term.
   *
   * @return string
   *   Rendered table with the search results.
   */
  private function getResults($search_for) {
    $search_for = '%' . $search_for . '%';
    $connection = Database::getConnection();
    $query = $connection->select('pos_products', 'p');
    $query->addField('p', 'id');
    $query->addField('p', 'sku');
    $query->addField('p', 'title');
    $or = $query->orConditionGroup();
    $or->condition('p.sku', $search_for, 'LIKE');
    $or->condition('p.title', $search_for, 'LIKE');
    $query->condition($or);
    $data = $query->execute();
    $profiles = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $profiles;
  }


}
