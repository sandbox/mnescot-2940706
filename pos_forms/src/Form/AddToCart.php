<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\AddToCart.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AddToCart.
 *
 * @package Drupal\pos_forms\Form
 */
class AddToCart extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_to_cart';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $param = \Drupal::request()->query->all();
    $form['product_id'] = array(
      '#type' => 'hidden',
      '#value' => $param['product_id'],
    );
    $form['profile_id'] = array(
      '#type' => 'hidden',
      '#value' => $param['profile_id'],
    );
    $form['sku'] = array(
      '#type' => 'hidden',
      '#value' => $param['sku'],
    );
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => $param['title'],
      '#value' => $param['title'],
    );
    $form['copy_limit'] = array(
      '#type' => 'item',
      '#title' => $this->t('Copy limit:'),
      '#markup' => $param['copy_limit'],
      '#value' => $param['copy_limit'],
    );
    $form['quantity'] = array(
      '#type' => 'number',
      '#title' => $this->t('Quantity'),
      '#description' => $this->t('Enter product quantity'),
      '#default_value' => 1,
    );
    $form['add_to_cart'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Add to Cart'),
    );
    if (\Drupal::currentUser()->isAnonymous() && !in_array($_SERVER['HTTP_HOST'], ['www.pos.local', 'www.pos.dev'])) {
      $form['captcha'] = [
        '#type' => 'captcha',
        '#captcha_type' => 'recaptcha/reCAPTCHA',
      ];
      $form['#attached']['library'][] = 'pos_forms/google_recaptcha';
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('pos_operations.cart_item')->addItem(
      $form_state->getValue('product_id'),
      $form_state->getValue('sku'),
      $form_state->getValue('quantity'),
      $form_state->getValue('copy_limit'),
      $form_state->getValue('title'),
      $form_state->getValue('profile_id')
    );
  }

}
