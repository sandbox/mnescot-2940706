<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\ReviewOrder.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ReviewOrder.
 *
 * @package Drupal\pos_forms\Form
 */
class ReviewOrder extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'review_order';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $order_id = NULL) {

    $form['#cache'] = ['max-age' => 0];

    $order = \Drupal::service('pos_operations.order')
      ->getRawOrder($order_id);

    $session_order_id = \Drupal::service('pos_operations.session')->getSessionOrderId();

    // If order id is different from the one assigned to the session it belongs
    // to another cart, so it can be a hacking.
    // If order has status it belongs to another cart, so it can be a hacking.
    if (
      $order_id != $session_order_id ||
      (bool) $order['status'] ||
      \Drupal::service('pos_operations.session')->sessionIsOff()
    ) {
      $form['expired'] = [
        '#id' => 'item-expired',
        '#type' => 'markup',
        '#markup' => $this->t('This form has expired.'),
      ];
      return $form;
    }

    $header = [
      $this->t('Title'),
      $this->t('Quantity'),
    ];

    $form['cart_items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cart Items'),
    ];
    $form['cart_items_fieldset']['cart'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => \Drupal::service('pos_operations.cart_item')
        ->getCartItems($review = TRUE),
    ];

    $shipping = \Drupal::service('pos_operations.shipping')
      ->getShippingByOrderId($order_id);
    $form_state->set('shipping_id', $shipping['id']);

    $form['contact_info_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact Information'),
    ];
    $form['contact_info_fieldset']['email'] = [
      '#type' => 'items',
      '#markup' => $this->buildcontactMarkup($shipping),
    ];

    $form['shipping_info_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Shipping Information'),
    ];
    $form['shipping_info_fieldset']['Address'] = [
      '#type' => 'items',
      '#title' => $this->t('Email Address'),
      '#markup' => $this->buildAddressMarkup($shipping),
    ];


    //Vera
    $hide_fields = \Drupal::currentUser()->isAnonymous();// Check if Anonymous user...Hide Admin fields
    if (!$hide_fields) {
      $order = \Drupal::service('pos_operations.order')->getOrder($order_id);
      $form['order_details_fieldset'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Order Details'),
      ];
      $form['order_details_fieldset']['job_title'] = [
        '#type' => 'items',
        '#markup' => $this->buildOrderDetailstMarkup($order),
      ];
    }

    $form['place_order'] = [
      '#id' => 'place-order-button',
      '#name' => 'place_order',
      '#type' => 'submit',
      '#value' => $this->t('Place the Order'),
      '#weight' => 90,
    ];
    $form['go_back'] = [
      '#id' => 'go-back_button',
      '#name' => 'go_back',
      '#type' => 'submit',
      '#value' => $this->t('Go Back'),
      '#weight' => 92,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    switch ($triggering_element['#name']) {
      case 'place_order':
        if ($order_id = \Drupal::service('pos_operations.order')
          ->createItems()) {
          if (\Drupal::service('pos_operations.cart_item')->cartHasBulkItem()) {
            \Drupal::service('pos_operations.order')
              ->setOrderStatusToBulk($order_id);
            $is_anonymous = \Drupal::currentUser()
              ->isAnonymous();// Check if Anonymous user...Hide Admin fields
            if ($is_anonymous) {
              //handling bulk orders created by anon user
              \Drupal::service('pos_operations.order')
                ->bulkOrder($order_id, 'bulk', $comment = NULL);
            }
            else {
              \Drupal::service('pos_operations.order')
                ->setOrderStatusToPending($order_id);
            }
          }
          else {
            \Drupal::service('pos_operations.order')
              ->setOrderStatusToPending($order_id);
          }
          \Drupal::service('pos_operations.cart_item')->clearCart();
          \Drupal::service('pos_operations.session')->clearSession();
          $form_state->setRedirect('pos_operations.message_order_created', ['order_id' => $order_id]);
        }
        break;

      case 'go_back':
        $form_state->setRedirect('checkout.info', [
          'info_type' => 'shipping',
          'info_id' => $form_state->get('shipping_id'),
        ]);
        break;

      default:
        // Do nothing.
    }
  }

  /**
   * Build the markup for order details information.
   *
   * @param array $order
   *   Order Details information.
   *
   * @return string
   *   HTML containing the order detail information.
   */
  private function buildOrderDetailstMarkup($order) {
    $result = '';
    $result .= $order['call_center'] . '<br />';
    $result .= $order['how_received'] . '<br />';
    $result .= $order['caller_language'] . '<br />';

    return $result;
  }

  /**
   * Build the markup for contact information.
   *
   * @param array $shipping
   *   Shipping information.
   *
   * @return string
   *   HTML containing the contact information.
   */
  private function buildcontactMarkup($shipping) {
    $result = '';
    $result .= $shipping['email'] . '<br />';
    $result .= $shipping['phone'] . '<br />';
    $result .= $shipping['phoneext'] . '<br />';
    return $result;
  }


  /**
   * Build the markup for address.
   *
   * @param array $shipping
   *   Shipping information.
   *
   * @return string
   *   HTML containing the address information.
   */
  private function buildAddressMarkup($shipping) {
    $result = '';
    $result .= $shipping['first_name'] . chr(32) . $shipping['last_name'] . '<br />';
    $result .= $shipping['address_1'] . '<br />';
    $result .= ($shipping['address_2']) ? $shipping['address_2'] . '<br />' : NULL;
    $result .= $shipping['city'] . ', ' . $shipping['state'] . chr(32) . $shipping['zip'] . '<br />';
    return $result;
  }

}
