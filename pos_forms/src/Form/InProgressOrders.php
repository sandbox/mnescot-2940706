<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\InProgressOrders.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;


/**
 * Class InProgressOrders.
 *
 * @package Drupal\pos_forms\Form
 */
class InProgressOrders extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'in_progress_orders';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $batch_owner_id = NULL) {

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $form['batch_owner_id'] = [
      '#type' => 'hidden',
      '#value' => $batch_owner_id,
    ];

    if ($batch_owner_id) {
      $batch_owner_name = \Drupal::service('pos_operations.user_and_role')->getBatchOwnerName($batch_owner_id);
    }
    else {
      $batch_owner_name = 'All Warehouse staff';
    }

    $form['#title'] = $this->t('In Progress Orders assigned to %name', ['%name' => $batch_owner_name]);


    $header = [
      'id' => $this->t('Order ID'),
      'name' => $this->t('Name'),
      'view' => $this->t('View'),
      'edit' => $this->t('Edit'),
      'created' => $this->t('Created'),
      'batched' => $this->t('Batched'),
    ];

    if (!$batch_owner_id) {
      $header['batch_owner'] = $this->t('Batch Owner');
    }

    $staff = \Drupal::service('pos_operations.user_and_role')->getWarehouseStaffAsSelectOptions();

    $options = [];

    foreach (\Drupal::service('pos_operations.order')->getOrdersByStatus('progress', $batch_owner_id) as $order) {
      $param = [
        'id' => $order['id'],
      ];
      $view_url = Url::fromRoute('pos_orders_page.view_order_content', $param);
      $view_url->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);

      $current_path = \Drupal::service('path.current')->getPath();
      $destination = explode('/', $current_path);
      $destination = array_filter($destination);
      if (count($destination) > 1) {
        $destination = serialize($destination);
      }
      else {
        $destination = array_shift($destination);
      }
      $param = [
        'order_id' => $order['id'],
        'destination_args' => $destination,
      ];
      $edit_url = Url::fromRoute('order.edit_with_destination', $param);

      $options[$order['id']] = [
        'id' => $order['id'],
        'name' => $order['shipping']['first_name'] . chr(32) . $order['shipping']['last_name'],
        'view' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'View',
            '#title_display' => 'invisible',
            '#url' => $view_url,
          ],
        ],
        'edit' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'Edit',
            '#title_display' => 'invisible',
            '#url' => $edit_url,
          ],
        ],
        'created' => $order['created'],
        'batched' => $order['batched'],
      ];

      if (!$batch_owner_id) {
        if ($order['batch_owner_id']) {
          $in_progress_url = Url::fromRoute('order.in_progress_orders', ['batch_owner_id' => $order['batch_owner_id']]);
          $options[$order['id']]['batch_owner'] = [
            'data' => [
              '#type' => 'link',
              '#title' => $staff[$order['batch_owner_id']],
              '#title_display' => 'invisible',
              '#url' => $in_progress_url,
            ],
          ];
        }
        else {
          $options[$order['id']]['batch_owner'] = $this->t('- None -');
        }
      }

    }

    $form['orders_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No Order found for this batch owner.'),
      '#weight' => 10,
    );

    $staff = ['_none' => '- Select -'] + $staff;
    unset($staff[$batch_owner_id]);
    $form['staff'] = [
      '#type' => 'select',
      '#name' => 'staff',
      '#title' => $this->t('Reassign batch to'),
      '#options' => $staff,
      '#weight' => 20,
    ];

    $form['buttons'] = [
      '#type' => 'container',
      '#weight' => 80,
    ];

    $form['buttons']['reassign'] = [
      '#type' => 'submit',
      '#name' => 'reassign',
      '#value' => $this->t('Reassign'),
      '#weight' => 10,
    ];

    $form['buttons']['set_ready_to_ship'] = [
      '#type' => 'submit',
      '#name' => 'set_ready_to_ship',
      '#value' => $this->t('Set orders as %label', ['%label' => \Drupal::service('pos_operations.status')->getStatusName('shipping')]),
      '#ajax' => ['callback' => '::openConfirmModal'],
      '#weight' => 15,
    ];

    $form['buttons']['print'] = [
      '#type' => 'detail',
      '#weight' => 20,
    ];

    $form['buttons']['print']['invoice'] = [
      '#type' => 'submit',
      '#name' => 'print_invoice',
      '#value' => $this->t('Print Invoice'),
      '#weight' => 10,
    ];

    $form['buttons']['print']['labels'] = [
      '#type' => 'submit',
      '#name' => 'print_labels',
      '#value' => $this->t('Print Labels'),
      '#weight' => 10,
    ];

    $form['batches_owners'] = [
      '#type' => 'details',
      '#title' => $this->t('Batches Owners'),
      '#open' => TRUE,
      '#weight' => 90,
    ];

    $form['batches_owners']['temp'] = $this->buidBatchesOwners();

    return $form;
  }


  /**
   * Implements the submit handler for the ajax call.
   *
   * @param array $form
   *   Render array representing from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Array of ajax commands to execute on submit of the modal form.
   */
  public function openConfirmModal(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($selected_orders = $this->getSelectedOrderIds($form_state)) {
      $parameters = [
        'status' => 'shipping',
        'ids' => $selected_orders,
        'batch_owner_id' => $form_state->getValue('batch_owner_id'),
      ];
      $content = \Drupal::formBuilder()->getForm('\Drupal\pos_forms\Form\ConfirmSetStatus', serialize($parameters));
      $title = $this->t('Please confirm the changing of status to %label for these orders:', ['%label' => \Drupal::service('pos_operations.status')->getStatusName('shipping')]);
      $options = array(
        'dialogClass' => 'popup-dialog-class',
        'width' => '300',
        'height' => '300',
      );
      $response->addCommand(new OpenModalDialogCommand($title, $content, $options));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $selected_orders = $this->getSelectedOrderIds($form_state);
    switch ($triggering_element['#name']) {
      case 'reassign':
        $batch_owner_id = $form_state->getValue('staff');
        if ($batch_owner_id != '_none') {
          foreach ($selected_orders as $order_id) {
            \Drupal::service('pos_operations.order')->setOrderStatusToInProgress($order_id, $batch_owner_id);
          }
        }
        break;

      case 'print_labels':
        \Drupal::service('pos_operations.generate_pdf')->labels($selected_orders);
        break;

      case 'print_invoice':
        \Drupal::service('pos_operations.generate_pdf')->invoice($selected_orders);
        break;
    }

    return [];
  }

  /**
   * Extracts the IDs of the Orders selected in the tableselect.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   Selected IDs.
   */
  private function getSelectedOrderIds(FormStateInterface $form_state) {
    $result = [];
    foreach ($form_state->getValue('orders_table') as $order_id => $checked) {
      if ($checked) {
        $result[] = $order_id;
      }
    }
    return $result;
  }

  /**
   * Gets the output of Views: Batches Owners Group (Orders).
   *
   * @return array
   *   Render array with the Views result.
   */
  private function buidBatchesOwners() {
    $view = Views::getView('batches_owners_group');
    $view->setDisplay('block_1');
    return $view->render();
  }


}
