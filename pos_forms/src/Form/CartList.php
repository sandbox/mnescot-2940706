<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\CartList.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Class CartList.
 *
 * @package Drupal\pos_forms\Form
 */
class CartList extends FormBase {

  private $copy_limit_message = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $default_config = \Drupal::config('pos_operations.settings');
    $this->copy_limit_message = $default_config->get('messages.copy_limit_exceeded.show_cart');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cart_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $profile_id = NULL) {

    if ($profile_id) {
      $form['profile_id'] = array(
        '#type' => 'hidden',
        '#value' => $profile_id,
      );
    }

    if ($products = \Drupal::service('pos_operations.cart_item')->getCartItems()) {
      $this->buildCartItemsForm($form, $form_state, $products);
    }
    else {
      $form['no_items_message'] = array(
        '#type' => 'markup',
        '#markup' => $this->t('There are no items in the Cart.'),
      );
    }

    $form['#cache'] = ['max-age' => 0];

    return $form;
  }

  public function buildCartItemsForm(array &$form, FormStateInterface $form_state, $products) {

    foreach ($products as $product) {
      $fieldset_name = 'item_' . $product['item_id'];
      $form[$fieldset_name] = array(
        '#type' => 'fieldset',
      );
      $form[$fieldset_name]['cart-item-id_' . $product['item_id']] = array(
        '#type' => 'hidden',
        '#value' => $product['item_id'],
      );
      $form[$fieldset_name]['cart-product-id_' . $product['item_id']] = array(
        '#type' => 'hidden',
        '#value' => $product['product_id'],
      );
      $form[$fieldset_name]['cart-quantity-anterior_' . $product['item_id']] = array(
        '#type' => 'hidden',
        '#value' => $product['quantity'],
      );
      $form[$fieldset_name]['cart-title_' . $product['item_id']] = array(
        '#type' => 'item',
        '#markup' => $product['title'],
        '#value' => $product['title'],
      );
      $form[$fieldset_name]['cart-quantity_' . $product['item_id']] = array(
        '#type' => 'number',
        '#title' => 'Quantity',
        '#default_value' => $product['quantity'],
      );
      $form[$fieldset_name]['cart-message-fieldset_' . $product['item_id']] = $this->checkCopyLimit($product);
      $url = Url::fromRoute('products.remove_from_cart', [
        'destination' => \Drupal::service('path.current')->getPath(),
        'item_id' => $product['item_id'],
        'product_id' => $product['product_id'],
        'title' => $product['title'],
        'quantity' => $product['quantity'],
      ]);

      $form[$fieldset_name]['update'] = array(
        '#id' => 'update_button',
        '#type' => 'submit',
        '#value' => $this->t('Update Quantity'),
      );

      $url->setOptions([
        'attributes' => [
          'class' => [
            'use-ajax',
            'button',
            'button--small',
            'button-add-to-cart',
          ],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 400]),
        ],
      ]);
      $form[$fieldset_name]['remove_' . $product['item_id']] = array(
        '#type' => 'link',
        '#title' => $this->t('Remove'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['js-form-submit', 'form-submit', 'btn-default btn'],
        ],
      );
    }


    $form['checkout'] = array(
      '#id' => 'checkout_button',
      '#type' => 'submit',
      '#value' => $this->t('Checkout'),
    );

    $form['#attached']['library'][] = 'publications_ordering_system/pos_scripts';

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (@$triggering_element['#id'] == 'update_button') {
      $this->updateCartItems($form_state);
    }
    elseif (@$triggering_element['#id'] == 'checkout_button') {
      if ($profile_id = $form_state->getValue('profile_id')) {
        $form_state->setRedirect('checkout.info', ['info_type' => 'profile', 'info_id' => $profile_id]);
      }
      else {
        $form_state->setRedirect('checkout');
      }
    }
  }

  /**
   * Updates the cart items.
   *
   * @param FormStateInterface $form_state
   *   FormStateInterface object.
   */
  private function updateCartItems(FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      $identifier = explode('_', $key);
      if ($identifier[0] == 'cart-item-id') {
        $quantity = $form_state->getValue('cart-quantity_' . $value);
        $quantity_anterior = $form_state->getValue('cart-quantity-anterior_' . $value);
        $product_id = $form_state->getValue('cart-product-id_' . $value);
        $title = $form_state->getValue('cart-title_' . $value);
        if (\Drupal::service('pos_operations.cart_item')->isItemAvailable($product_id, $title, $quantity, $quantity_anterior)) {
          \Drupal::service('pos_operations.cart_item')->updateItem($value, $quantity, $quantity_anterior, $product_id);
        }
      }
    }
  }

  /**
   * Check whether the ordered quantity exceeds the copy limit.
   *
   * @param array $product
   *   Associative array containing a MySQL result row.
   *
   * @return array
   *   NULL or form item.
   */
  private function checkCopyLimit($product) {
    $result = NULL;
    if ($product['quantity'] > $product['copy_limit'] && $product['copy_limit'] > 0) {
      $placeholders = [
        '%quantity' => $product['quantity'],
        '%copy_limit' => $product['copy_limit'],
        '%title' => $product['copy_title'],
      ];
      $result = [
        '#type' => 'fieldset',
        '#attributes' => ['class' => ['cart-copy-limit-message']],
        'cart-message_' . $product['item_id'] => [
          '#type' => 'item',
          '#markup' => str_replace(array_keys($placeholders), array_values($placeholders), $this->copy_limit_message),
        ],
      ];
    }
    return $result;
  }

}
