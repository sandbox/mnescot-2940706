<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\ShippingOrders.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;

/**
 * Class ShippingOrders.
 *
 * @package Drupal\pos_forms\Form
 */
class ShippingOrders extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'returned_orders';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $form['#title'] = $this->t('Ready for Shipping Orders');

    $header = [
      'id' => $this->t('Order ID'),
      'name' => $this->t('Name'),
      'view' => $this->t('View'),
      'edit' => $this->t('Edit'),
      'created' => $this->t('Created'),
      'changed' => $this->t('Changed'),
    ];

    $options = [];

    foreach (\Drupal::service('pos_operations.order')->getOrdersByStatus('shipping') as $order) {

      $param = [
        'id' => $order['id'],
      ];
      $view_url = Url::fromRoute('pos_orders_page.view_order_content', $param);
      $view_url->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);

      $current_path = \Drupal::service('path.current')->getPath();
      $destination = explode('/', $current_path);
      $destination = array_filter($destination);
      if (count($destination) > 1) {
        $destination = serialize($destination);
      }
      else {
        $destination = array_shift($destination);
      }
      $param = [
        'order_id' => $order['id'],
        'destination_args' => $destination,
      ];
      $edit_url = Url::fromRoute('order.edit_with_destination', $param);

      $options[$order['id']] = [
        'id' => $order['id'],
        'name' => $order['shipping']['first_name'] . chr(32) . $order['shipping']['last_name'],
        'view' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'View',
            '#title_display' => 'invisible',
            '#url' => $view_url,
          ],
        ],
        'edit' => [
          'data' => [
            '#type' => 'link',
            '#title' => 'Edit',
            '#title_display' => 'invisible',
            '#url' => $edit_url,
          ],
        ],
        'created' => $order['created'],
        'changed' => $order['changed'],
      ];
    }

    $form['orders_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No Orders found for this status.'),
      '#weight' => 10,
    );

    $form['buttons'] = [
      '#type' => 'detail',
      '#weight' => 95,
    ];

    $form['buttons']['set_completed'] = [
      '#type' => 'submit',
      '#name' => 'set_completed',
      '#value' => $this->t('Set orders as %label', ['%label' => \Drupal::service('pos_operations.status')->getStatusName('completed')]),
      '#ajax' => ['callback' => '::openConfirmModal'],
      '#weight' => 15,
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements the submit handler for the ajax call.
   *
   * @param array $form
   *   Render array representing from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Array of ajax commands to execute on submit of the modal form.
   */
  public function openConfirmModal(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($selected_orders = $this->getSelectedOrderIds($form_state)) {
      $parameters = [
        'status' => 'completed',
        'ids' => $selected_orders,
        'batch_owner_id' => FALSE,
      ];
      $content = \Drupal::formBuilder()->getForm('\Drupal\pos_forms\Form\ConfirmSetStatus', serialize($parameters));
      $title = $this->t('Please confirm the changing of status to %label for these orders:', ['%label' => \Drupal::service('pos_operations.status')->getStatusName('completed')]);
      $options = array(
        'dialogClass' => 'popup-dialog-class',
        'width' => '300',
        'height' => '300',
      );
      $response->addCommand(new OpenModalDialogCommand($title, $content, $options));
    }
    return $response;
  }

  /**
   * Extracts the IDs of the Orders selected in the tableselect.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   Selected IDs.
   */
  private function getSelectedOrderIds(FormStateInterface $form_state) {
    $result = [];
    foreach ($form_state->getValue('orders_table') as $order_id => $checked) {
      if ($checked) {
        $result[] = $order_id;
      }
    }
    return $result;
  }

}
