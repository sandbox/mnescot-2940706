<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\ConfirmRemoveCartItem.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfirmRemoveCartItem.
 *
 * @package Drupal\pos_forms\Form
 */
class ConfirmRemoveCartItem extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_remove_cart_item';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $param = \Drupal::request()->query->all();
    $form['item_id'] = array(
      '#type' => 'hidden',
      '#value' => $param['item_id'],
    );
    $form['product_id'] = array(
      '#type' => 'hidden',
      '#value' => $param['product_id'],
    );
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => $param['title'],
    );
    $form['quantity'] = array(
      '#type' => 'item',
      '#title' => 'Quantity',
      '#markup' => $param['quantity'],
      '#value' => $param['quantity'],
    );
    $form['confirm'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Remove'),
      '#description' => $this->t('Please confirm the removal of this item'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('pos_operations.cart_item')->removeItem(
      $form_state->getValue('item_id'),
      $form_state->getValue('product_id'),
      $form_state->getValue('quantity')
    );
  }

}
