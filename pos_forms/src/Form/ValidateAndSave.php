<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 3/29/17
 * Time: 3:02 PM
 */

namespace Drupal\pos_forms\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;

trait ValidateAndSave {

  private $entity = NULL;
  private $entityTitle = NULL;
  private $redirectRoute = NULL;

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $entity_fields = $this->entity->getFields();
    foreach (array_keys($entity_fields) as $field_name) {
      if ($value = $form_state->getValue($field_name, NULL)) {
        $this->entity->set($field_name, $value);
      }
    }
    $this->entity->set('user_id', $user->id());
    $status = $this->entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label %title.', [
          '%label' => $this->entity->label(),
          '%title' => $this->entityTitle,
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label %title.', [
          '%label' => $this->entity->label(),
          '%title' => $this->entityTitle,
        ]));
    }
    $form_state->setRedirect($this->redirectRoute);

    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_fields = $form_state->getValues();
    unset($form_fields['submit']);
    unset($form_fields['form_build_id']);
    unset($form_fields['form_token']);
    unset($form_fields['form_id']);
    unset($form_fields['op']);
    $user_input = $form_state->getUserInput();
    foreach (array_keys($form_fields) as $field_name) {
      if (isset($user_input[$field_name][0]['value'])) {
        $value = $user_input[$field_name][0]['value'];
      }
      elseif (isset($user_input[$field_name][0]['target_id'])) {
        $value = $user_input[$field_name][0]['target_id'];
      }
      elseif (isset($user_input[$field_name])) {
        $value = $user_input[$field_name];
      }
      else {
        $value = NULL;
      }
      $form_state->setValue($field_name, Xss::filter($value));
    }
  }
}