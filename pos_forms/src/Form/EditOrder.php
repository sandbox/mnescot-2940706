<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\EditOrder.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class EditOrder.
 *
 * @package Drupal\pos_forms\Form
 */
class EditOrder extends FormBase {

  private $orderEntity = NULL;
  private $customerProfileEntity = NULL;
  private $shippingEntity = NULL;
  private $itemsValues = array();
  private $tableselectItems = array();

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edit_order';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $order_id = NULL, $destination_args = NULL) {

    $form['#cache'] = ['max-age' => 0];

    if ($destination_path = unserialize($destination_args)) {
      $destination_path = implode('/', $destination_path);
    }
    else {
      $destination_path = $destination_args;
    }

    $form['#title'] = $this->t('Editing Order %order_id', ['%order_id' => $order_id]);

    $form['custom_title'] = array(
      '#type' => 'markup',
      '#markup' => "<h2>Editing Order $order_id</h2>",
    );

    $this->orderEntity = \Drupal::service('pos_operations.order')->getOrderEntity($order_id);
    $original_status = $this->orderEntity->getValue('status');

    $disable_fields = in_array($original_status, ['canceled', 'completed']);

    $form['disable_fields'] = [
      '#type' => 'hidden',
      '#value' => $disable_fields,
    ];

    $form['order_id'] = array(
        '#id' => 'edit-order-id',
        '#type' => 'hidden',
        '#field_name' => 'order_id',
        '#value' => $order_id,
    );

    // Order Fields.
    $form['status'] = [
      '#type' => 'select',
      '#options' => \Drupal::service('pos_operations.order')->getStatusAsSelectOptions(),
      '#default_value' => $original_status,
      '#title' => $this->t('Status'),
      '#weight' => 10,
    ];
    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment'),
      '#default_value' => \Drupal::service('pos_operations.historic')->getLastComment($order_id),
      '#weight' => 12,
    ];
    $form['save_1'] = [
      '#type' => 'submit',
      '#name' => 'save_1',
      '#value' => $this->t('Update Order'),
      '#parameter' => [
        'destination' => $destination_path,
      ],
      '#weight' => 15,
    ];
    $form['cancel_1'] = [
      '#type' => 'submit',
      '#name' => 'cancel_1',
      '#value' => $this->t('Cancel'),
      '#parameter' => [
        'destination' => $destination_path,
      ],
      '#weight' => 17,
    ];

    $this->buildItems($form, $order_id, $disable_fields);

    $this->buildOrderDetails($form, $disable_fields);

    if ($customer_id = $this->orderEntity->getValue('customer_id')) {
      $this->buildCustomerProfile($form, $customer_id);
    }

    $this->buildShipping($form, $order_id, $customer_id, $disable_fields);

    $form['save_2'] = [
      '#type' => 'submit',
      '#name' => 'save_2',
      '#value' => $this->t('Update Order'),
      '#parameter' => [
        'destination' => $destination_path,
      ],
      '#weight' => 95,
    ];
    $form['cancel_2'] = [
      '#type' => 'submit',
      '#name' => 'cancel_2',
      '#value' => $this->t('Cancel'),
      '#parameter' => [
        'destination' => $destination_path,
      ],
      '#weight' => 98,
    ];

    $form['#validate'][] = '::editorderFormValidation';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function editorderFormValidation(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);

    $input_values = $form_state->getUserInput();

    $disable_fields = in_array($input_values['status'], ['canceled', 'completed','returned', 'returned_canceled']);
    if(!$disable_fields) {
      if (isset($input_values['email'])) {
        $email = trim($input_values['email']);
        if (\Drupal::service('email.validator')->isValid($email)) {
          $form_state->setValue('email', $email);
        } else {
          $form_state->setErrorByName('email', $this->t('Invalid email.'));
        }
      }

      $zip = trim($input_values['shipping_zip']);
      if (!validateUSAZip($zip)) {
        $form_state->setErrorByName('zip', $this->t('Invalid zip. Make sure you\'ve entered the ZIP code in either 5-digit or 9-digit format, for example 12345 or 12345-6789.'));
      }

      $phone = trim($input_values['shipping_phone']);
      if (!validatePhone($phone)) {
        $form_state->setErrorByName('phone', $this->t('Invalid phone. Enter numeric values only.'));
      }

      $phoneext = trim($input_values['shipping_phoneext']);
      if (!validatePhoneExt($phoneext)) {
        $form_state->setErrorByName('phoneext', $this->t('Invalid Extension. Enter numeric values only.'));
      }
    }

  }


  /**
   * Ajax callback for textfield "Zip Code".
   *
   * @param array $form
   *   The $form_state object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */

  /*
  public function zipAutocompleteShipping(array $form, FormStateInterface $form_state)
  {
    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();

    $zipcode = $form_state->getValue('shipping_zip');

    $city_state = \Drupal::service('pos_operations.profile')->getCityStateByZip($zipcode);
    if($city_state) {
      $color = 'green';
      $city = $city_state['City'];
      $state = $city_state['State'];
      $zip = $city_state['ZipCode'];

      $ajax_response->addCommand(new InvokeCommand('#edit-shipping-city', 'val', [$city]));
      $ajax_response->addCommand(new InvokeCommand('#edit-shipping-state', 'val', [$state]));
      $ajax_response->addCommand(new InvokeCommand('#edit-shipping-zip', 'val', [$zip]));
      $ajax_response->addCommand(new HtmlCommand('#shipping-zip-code', ''));
      $ajax_response->addCommand(new InvokeCommand('#shipping-zip-code', 'css', array('color', $color)));
    }else{
      $color = 'red';
      $ajax_response->addCommand(new HtmlCommand('#shipping-zip-code', 'Invalid Shipping Zip Code'));
      // Add a command, InvokeCommand, which allows for custom jQuery commands.
      // In this case, we alter the color of the description.
      $ajax_response->addCommand(new InvokeCommand('#shipping-zip-code', 'css', array('color', $color)));
    }

    // Return the AjaxResponse Object.
    return $ajax_response;
  }
*/

  /**
   * Adds fields for the Order.
   *
   * @param array $form
   *   Form structure.
   */
  private function buildOrderDetails(&$form, $disable_fields = FALSE) {
    $form['order_details'] = array(
      '#type' => 'fieldset',
      '#title' => t('Order Details'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array(
        'class' => array(
          'fieldset-order_details',
        ),
      ),
      '#weight' => 38,
    );
    $owner = $this->orderEntity->getOwner();
    $form['order_details']['user_id'] = [
      '#type' => 'entity_autocomplete',
      '#field_name' => 'user_id',
      '#target_type' => 'user',
      '#default_value' => $owner->isAnonymous() ? NULL : $owner,
      '#selection_settings' => ['include_anonymous' => TRUE],
      '#title' => $this->t('Created by'),
      '#weight' => 20,
    ];
    $form['order_details']['caller_language'] = [
        '#type' => 'select',
        '#field_name' => 'caller_language',
        '#options' => _pos_utils_get_vocabulary_as_select_options('language'),
        '#default_value' => $this->orderEntity->getValue('caller_language'),
        '#title' => $this->t('Caller Language'),
        '#weight' => 23,
    ];
    $form['order_details']['call_center'] = [
      '#type' => 'select',
      '#field_name' => 'call_center',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_call_center'),
      '#default_value' => $this->orderEntity->getValue('call_center'),
      '#title' => $this->t('Call Center'),
      '#weight' => 25,
    ];
    $form['order_details']['how_received'] = [
      '#type' => 'select',
      '#field_name' => 'how_received',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_how_received'),
      '#default_value' => $this->orderEntity->getValue('how_received'),
      '#title' => $this->t('How Received'),
      '#weight' => 30,
    ];

    $form['order_details']['how_heard'] = array(
      '#type' => 'select',
      '#field_name' => 'how_heard',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_how_heard'),
      '#default_value' => $this->orderEntity->getValue('how_heard'),
      '#title' => $this->t('How Heard'),
      '#weight' => 60,
    );

    $form['order_details']['caller_language'] = array(
        '#type' => 'select',
        '#field_name' => 'caller_language',
        '#options' => _pos_utils_get_vocabulary_as_select_options('language'),
        '#default_value' => $this->orderEntity->getValue('caller_language'),
        '#title' => $this->t('Caller Language'),
        '#weight' => 60,
    );
    
    // @TODO: Make job title autocomplete.
    $form['order_details']['job_title'] = array(
      '#type' => 'textfield',
      '#field_name' => 'job_title',
      '#title' => $this->t('Job Title?'),
      '#default_value' => $this->getJobTitleName($this->orderEntity->getValue('job_title')),
      '#maxlength' => 254,
      '#weight' => 65,
    );
    
    $form['order_details']['organization_type'] = array(
      '#type' => 'select',
      '#field_name' => 'organization_type',
      '#options' => _pos_utils_get_vocabulary_as_select_options('organization_type'),
      '#default_value' => $this->orderEntity->getValue('organization_type'),
      '#title' => $this->t('Organization'),
      '#weight' => 70,
    );

    foreach ($form['order_details'] as &$element) {
      if (!is_object($element) && isset($element['#field_name'])) {
        $element['#disabled'] = $disable_fields;
      }
    }
  }

  /**
   * Adds the fields for customer profile.
   *
   * @param array $form
   *   Form structure.
   * @param int $customer_id
   *   Customer Profile ID.
   */
  private function buildCustomerProfile(&$form, $customer_id) {
    $form['customer_profile'] = array(
      '#type' => 'fieldset',
      '#title' => t('Customer Profile'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array(
        'class' => array(
          'fieldset-profile',
        ),
      ),
      '#weight' => 40,
    );
    $form['customer_profile']['profile_id'] = array(
      '#id' => 'edit-profile-id',
      '#type' => 'hidden',
      '#field_name' => 'id',
    );
    $form['customer_profile']['profile_email'] = array(
      '#id' => 'edit-profile-email',
      '#type' => 'textfield',
      '#field_name' => 'email',
      '#title' => $this->t('Email'),
      '#weight' => 10,
    );
    $form['customer_profile']['profile_phone'] = array(
        '#id' => 'edit-profile-phone',
        '#type' => 'textfield',
        '#field_name' => 'phone',
        '#title' => $this->t('Phone'),
        '#weight' => 12,
    );

    $form['customer_profile']['profile_phoneext'] = array(
        '#id' => 'edit-profile-phoneext',
        '#type' => 'textfield',
        '#field_name' => 'phoneext',
        '#title' => $this->t('Extension'),
        '#weight' => 12,
    );

    $form['customer_profile']['profile_first_name'] = array(
      '#id' => 'edit-profile-first-name',
      '#type' => 'textfield',
      '#field_name' => 'first_name',
      '#title' => $this->t('First name'),
      '#weight' => 15,
    );
    $form['customer_profile']['profile_last_name'] = array(
      '#id' => 'edit-profile-last-name',
      '#type' => 'textfield',
      '#field_name' => 'last_name',
      '#title' => $this->t('Last name'),
      '#weight' => 20,
    );
    $form['customer_profile']['profile_honorific'] = array(
      '#id' => 'edit-profile-honorific',
      '#type' => 'select',
      '#field_name' => 'honorific',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_honorific'),
      '#title' => $this->t('Title'),
      '#weight' => 25,
    );
    $form['customer_profile']['profile_organization'] = array(
      '#id' => 'edit-profile-organization',
      '#type' => 'textfield',
      '#field_name' => 'organization',
      '#title' => $this->t('Organization'),
      '#weight' => 25,
    );
    $form['customer_profile']['profile_address_1'] = array(
      '#id' => 'edit-profile-address-1',
      '#type' => 'textfield',
      '#field_name' => 'address_1',
      '#title' => $this->t('Address 1'),
      '#weight' => 30,
    );
    $form['customer_profile']['profile_address_2'] = array(
      '#id' => 'edit-profile-address-2',
      '#type' => 'textfield',
      '#field_name' => 'address_2',
      '#title' => $this->t('Address 2'),
      '#weight' => 35,
    );
    $form['customer_profile']['profile_city'] = array(
      '#id' => 'edit-profile-city',
      '#type' => 'textfield',
      '#field_name' => 'city',
      '#title' => $this->t('City'),
      '#weight' => 40,
    );
    $form['customer_profile']['profile_state'] = array(
      '#id' => 'edit-profile-state',
      '#type' => 'select',
      '#field_name' => 'state',
      '#options' => _pos_utils_us_states(),
      '#title' => $this->t('State'),
      '#weight' => 45,
    );

    $form['customer_profile']['profile_zip'] = array(
      '#id' => 'edit-profile-zip',
      '#type' => 'textfield',
      '#field_name' => 'zip',
      '#title' => $this->t('Zip'),
      '#weight' => 50,
    );

    $fields_ids = array();
    $this->customerProfileEntity = \Drupal::service('pos_operations.profile')->getCustomerProfileEntity($customer_id);
    $form['customer_profile']['email']['#value'] = $this->customerProfileEntity->getValue('email');
    foreach ($form['customer_profile'] as &$element) {
      if (!is_object($element) && isset($element['#field_name'])) {
        $element_value = $this->customerProfileEntity->getValue($element['#field_name']);
        $element['#value'] = $element_value;
        if ($element['#field_name'] != 'id') {
          $element['#disabled'] = TRUE;
        }
        $fields_ids[$element['#field_name']] = $element['#id'];
      }
    }
    $url = Url::fromRoute('checkout.choose_another_customer', ['fields_ids' => serialize($fields_ids)]);
    $url->setOptions([
      'attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'button--small',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 600]),
      ],
    ]);
    $form['customer_profile']['profile_change_email'] = array(
      '#id' => 'change-email-button',
      '#type' => 'link',
      '#title' => $this->t('Choose another customer'),
      '#url' => $url,
      '#weight' => 5,
    );

  }

  /**
   * Adds the fields for shipping.
   *
   * @param array $form
   *   Form structure.
   * @param int $order_id
   *   Order ID.
   * @param int $customer_id
   *   Customer profile ID.
   */
  private function buildShipping(&$form, $order_id, $customer_id = NULL, $disable_fields = FALSE) {
    $form['shipping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Shipping'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array(
        'class' => array(
          'fieldset-shipping',
        ),
      ),
      '#weight' => 45,
    );
    $form['shipping']['shipping_customer_id'] = array(
      '#id' => 'edit-shipping-id',
      '#type' => 'hidden',
      '#default_value' => $customer_id,
      '#field_name' => 'id',
    );
    $form['shipping']['shipping_email'] = array(
      '#id' => 'edit-shipping-email',
      '#type' => 'textfield',
      '#field_name' => 'email',
      '#title' => $this->t('Email'),
      '#maxlength' => 254,
      '#weight' => 10,
      '#required' => TRUE,
    );
    $form['shipping']['shipping_phone'] = array(
      '#id' => 'edit-shipping-phone',
      '#type' => 'textfield',
      '#field_name' => 'phone',
      '#title' => $this->t('Phone'),
      '#maxlength' => 50,
      '#weight' => 12,
      '#required' => TRUE,
    );

    $form['shipping']['shipping_phoneext'] = array(
        '#id' => 'edit-shipping-phoneext',
        '#type' => 'textfield',
        '#field_name' => 'phoneext',
        '#title' => $this->t('Extension'),
        '#maxlength' => 50,
        '#weight' => 12,
        '#required' => FALSE,
    );

    $form['shipping']['shipping_first_name'] = array(
      '#id' => 'edit-shipping-first-name',
      '#type' => 'textfield',
      '#field_name' => 'first_name',
      '#title' => $this->t('First name'),
      '#maxlength' => 50,
      '#weight' => 15,
      '#required' => TRUE,
    );
    $form['shipping']['shipping_last_name'] = array(
      '#id' => 'edit-shipping-last-name',
      '#type' => 'textfield',
      '#field_name' => 'last_name',
      '#title' => $this->t('Last name'),
      '#maxlength' => 50,
      '#weight' => 20,
      '#required' => TRUE,
    );
    $form['shipping']['shipping_honorific'] = array(
      '#id' => 'edit-shipping-honorific',
      '#type' => 'select',
      '#field_name' => 'honorific',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_honorific'),
      '#title' => $this->t('Title'),
      '#weight' => 25,
    );
    $form['shipping']['shipping_organization'] = array(
      '#id' => 'edit-shipping-organization',
      '#type' => 'textfield',
      '#field_name' => 'organization',
      '#title' => $this->t('Organization'),
      '#maxlength' => 125,
      '#weight' => 25,
    );
    $form['shipping']['shipping_address_1'] = array(
      '#id' => 'edit-shipping-address-1',
      '#type' => 'textfield',
      '#field_name' => 'address_1',
      '#title' => $this->t('Address 1'),
      '#maxlength' => 254,
      '#weight' => 30,
      '#required' => TRUE,
    );
    $form['shipping']['shipping_address_2'] = array(
      '#id' => 'edit-shipping-address-2',
      '#type' => 'textfield',
      '#field_name' => 'address_2',
      '#title' => $this->t('Address 2'),
      '#maxlength' => 254,
      '#weight' => 35,
    );
    $form['shipping']['shipping_city'] = array(
      '#id' => 'edit-shipping-city',
      '#type' => 'textfield',
      '#field_name' => 'city',
      '#title' => $this->t('City'),
      '#maxlength' => 60,
      '#weight' => 40,
      '#required' => TRUE,
    );
    $form['shipping']['shipping_state'] = array(
      '#id' => 'edit-shipping-state',
      '#type' => 'select',
      '#field_name' => 'state',
      '#options' => _pos_utils_us_states(),
      '#title' => $this->t('State'),
      '#weight' => 45,
      '#required' => TRUE,
    );

    $form['shipping']['shipping_zip'] = array(
      '#id' => 'edit-shipping-zip',
      '#type' => 'textfield',
      '#field_name' => 'zip',
      '#title' => $this->t('Zip'),
      '#maxlength' => 15,
      '#weight' => 50,
      '#required' => TRUE,
    );


/*
    $form['shipping']['shipping_zip'] = array(
        '#id' => 'edit-shipping-zip',
        '#type' => 'textfield',
        '#field_name' => 'zip',
        '#title' => 'Zip',
        '#maxlength' => 15,
        '#weight' => 37,
        '#description' => 'Please enter Zip Code',
        '#prefix' => '<div id="shipping-zip-code"></div>',
        '#ajax' => array(
            'callback' => '::zipAutocompleteShipping',
            'effect' => 'fade',
            'event' => 'change',
            'progress' => array(
                'type' => 'throbber',
                'message' => NULL,
            ),
        ),
    );
*/

    $fields_ids = array();
    $this->shippingEntity = \Drupal::service('pos_operations.shipping')->getShippingEntity($order_id);
    foreach ($form['shipping'] as &$element) {
      if (!is_object($element) && isset($element['#field_name'])) {
        $element_value = $this->shippingEntity->getValue($element['#field_name']);
        $element['#default_value'] = $element_value;
        $fields_ids[$element['#field_name']] = $element['#id'];
        $element['#disabled'] = $disable_fields;
      }
    }
  }


  /**
   * Adds the fields for items.
   *
   * @param array $form
   *   Form structure.
   * @param int $order_id
   *   Order ID.
   */
  private function buildItems(&$form, $order_id, $disable_fields) {

    $form['order_items'] = array(
      '#type' => 'fieldset',
      '#title' => t('Items'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#attributes' => array(
        'class' => array(
          'fieldset-order-items',
        ),
      ),
      '#weight' => 35,
    );

    $this->itemsValues = \Drupal::service('pos_operations.item')->getItems($order_id);
    $this->tableselectItems = array();
    foreach ($this->itemsValues as $key => $item) {
      $num = $key + 1;
      if ($disable_fields) {
        $quantity = $item['quantity'];
      }
      else {
        $quantity = array(
          'data' => array(
            '#type' => 'number',
            '#min' => 1,
            '#size' => 4,
            '#value' => $item['quantity'],
            '#name' => 'item-quantity[' . $item['product']['id'] . ']',
          ),
        );
      }
      $this->tableselectItems[$item['id']] = array(
        'num' => $num,
        'title' => $item['product']['title'],
        'sku' => $item['product']['sku'],
        'quantity' => $quantity,
      );
    }

    $header = array(
      'num' => t('Num'),
      'title' => t('Title'),
      'sku' => t('SKU'),
      'quantity' => t('Quantity'),
    );

    $form['order_items']['items_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $this->tableselectItems,
      '#empty' => t('Order has no items.'),
      '#weight' => 10,
      '#disabled' => $disable_fields,
    );

    if (!$disable_fields) {

      if ($this->tableselectItems) {
        $form['order_items']['remove_items'] = [
          '#type' => 'submit',
          '#name' => 'remove_items',
          '#value' => $this->t('Remove Selected Items'),
          '#weight' => 15,
        ];
      }

      $url = Url::fromRoute('order.add_item', [
        'destination' => \Drupal::service('path.current')->getPath(),
        'order_id' => $this->orderEntity->id(),
      ]);
      $url->setOptions([
        'attributes' => [
          'class' => [
            'use-ajax',
            'button',
            'button--small',
          ],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 600]),
        ],
      ]);
      $form['order_items']['add_item'] = array(
        '#id' => 'change-email-add_item',
        '#type' => 'link',
        '#title' => $this->t('Add Item'),
        '#url' => $url,
        '#weight' => 20,
      );

    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order_id = $form_state->getValue('order_id');
    $this->orderEntity = \Drupal::service('pos_operations.order')->getOrderEntity($order_id);
  
    $original_status = $this->orderEntity->getValue('status');
    $new_status = $form_state->getValue('status');
    $comment = $form_state->getValue('comment');
    $triggering_element = $form_state->getTriggeringElement();
    if ($destination_path = $triggering_element['#parameter']['destination']) {
      $redirect_url = Url::fromUri('internal:/' . $destination_path);
    }
    else {
      $redirect_url = Url::fromRoute('<current>');
    }

    if ($this->isCanceledCategory($original_status)  &&  !$this->isCanceledCategory($new_status)) {
      \Drupal::service('pos_operations.order')->restoreCanceledOrder($this->orderEntity->id(), $new_status, $comment);
      $form_state->setRedirectUrl($redirect_url);
      drupal_set_message($this->t('Order %id updated.', ['%id' => $this->orderEntity->id()]));
    }
    elseif (!$this->isCanceledCategory($original_status)  &&  $this->isCanceledCategory($new_status)) {
      \Drupal::service('pos_operations.order')->cancelOrder($this->orderEntity->id(), $new_status, $comment);
      $form_state->setRedirectUrl($redirect_url);
      drupal_set_message($this->t('Order %id updated.', ['%id' => $this->orderEntity->id()]));
    }
    elseif ($new_status == 'returned') {
      \Drupal::service('pos_operations.order')->setOrderStatusToReturned($this->orderEntity->id(), $comment);
      $form_state->setRedirectUrl($redirect_url);
      drupal_set_message($this->t('Order %id updated.', ['%id' => $this->orderEntity->id()]));
    }
    elseif (!$form_state->getValue('disable_fields')) {
    //if (!$form_state->getValue('disable_fields')) {
      if ($triggering_element['#name'] == 'remove_items') {
        $this->removeSelectedItems($form_state);
      }
      else {
        $form_state->setRedirectUrl($redirect_url);
        if (in_array($triggering_element['#name'], ['save_1', 'save_2'])) {
          //start vera handling bulk orders
          if ($original_status!='bulk' && !$this->isCanceledCategory($original_status)  &&  $new_status=='bulk') {

            \Drupal::service('pos_operations.order')->bulkOrder($this->orderEntity->id(),$new_status,$comment);
            $form_state->setRedirectUrl($redirect_url);
          }
          elseif ($original_status=='bulk' && !$this->isCanceledCategory($new_status)  &&  $new_status!='bulk') {
            \Drupal::service('pos_operations.order')->approveBulkOrder($this->orderEntity->id(), $new_status, $comment);
            $form_state->setRedirectUrl($redirect_url);
          }
          //end vera

          $saved_items = $this->updateItemsQuantities($form_state);
          $saved_order = $this->updateOrderEntity($form_state);
          $saved_shipping = $this->updateShippingEntity($form_state, $order_id);
          if (($saved_items && $saved_order & $saved_shipping)) {
            drupal_set_message($this->t('Order %id updated.', ['%id' => $this->orderEntity->id()]));
          }
          else {
            if (!$saved_items) {
              drupal_set_message($this->t('Error while updating Items.'), 'error');
            }
            if (!$saved_order) {
              drupal_set_message($this->t('Error while updating Order.'), 'error');
            }
            if (!$saved_shipping) {
              drupal_set_message($this->t('Error while updating Shipping.'), 'error');
            }
          }
        }
      }
    }
  }

  /**
   * Check if the status a canceled category.
   *
   * @param $status
   *  Status ID.
   *
   * @return bool
   *   Whether the status is a canceled category.
   */
  private function isCanceledCategory($status) {
    return in_array($status, ['canceled', 'returned_canceled']);
  }

  /**
   * Update the Order entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return int
   *   > 0 is the entity was correctly saved.
   */
  private function updateOrderEntity(FormStateInterface $form_state)
  {
    $input = $form_state->getUserInput();
    $customer_id = isset($input['profile_id']) ? $input['profile_id'] : 0;
    $this->orderEntity->customer_id->setValue($customer_id);
    $this->orderEntity->caller_language->setValue($form_state->getValue('caller_language'));
    $this->orderEntity->call_center->setValue($form_state->getValue('call_center'));
    $this->orderEntity->how_received->setValue($form_state->getValue('how_received'));
    $this->orderEntity->how_heard->setValue($form_state->getValue('how_heard'));
    $this->orderEntity->job_title->setValue($this->getJobTitleTermId($form_state->getValue('job_title')));
    $this->orderEntity->organization_type->setValue($form_state->getValue('organization_type'));
    $this->orderEntity->status->setValue($form_state->getValue('status'));
//kint($form_state->getValue('status'));
//kint(\Drupal::service('pos_operations.item')->orderHasBulkItem($this->orderEntity->id()));

    if ($form_state->getValue('status') == 'bulk' || \Drupal::service('pos_operations.item')->orderHasBulkItem($this->orderEntity->id())){
      $id_bulk = TRUE;
      $this->orderEntity->is_bulk->setValue($id_bulk);
    }
    


    if ($result = $this->orderEntity->save()) {
      \Drupal::service('pos_operations.historic')->record(
        $this->orderEntity->id(),
        $form_state->getValue('status'),
        $form_state->getValue('comment')
      );
    }
    return $result;
  }

  /**
   * Update the Shipping entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return int
   *   > 0 is the entity was correctly saved.
   */
  private function updateShippingEntity(FormStateInterface $form_state, $order_id) {
    $input = $form_state->getUserInput();
    $this->shippingEntity = \Drupal::service('pos_operations.shipping')->getShippingEntity($order_id);
    $this->shippingEntity->customer_id->setValue($input['profile_id']);
    $this->shippingEntity->email->setValue($form_state->getValue('shipping_email'));
    $this->shippingEntity->first_name->setValue($form_state->getValue('shipping_first_name'));
    $this->shippingEntity->last_name->setValue($form_state->getValue('shipping_last_name'));
    $this->shippingEntity->honorific->setValue($form_state->getValue('shipping_honorific'));
    $this->shippingEntity->phone->setValue($form_state->getValue('shipping_phone'));
    $this->shippingEntity->phoneext->setValue($form_state->getValue('shipping_phoneext'));
    $this->shippingEntity->organization->setValue($form_state->getValue('shipping_organization'));
    $this->shippingEntity->address_1->setValue($form_state->getValue('shipping_address_1'));
    $this->shippingEntity->address_2->setValue($form_state->getValue('shipping_address_2'));
    $this->shippingEntity->city->setValue($form_state->getValue('shipping_city'));
    $this->shippingEntity->state->setValue($form_state->getValue('shipping_state'));
    $this->shippingEntity->zip->setValue($form_state->getValue('shipping_zip'));
    return $this->shippingEntity->save();
  }


  /**
   * Update the Items entities.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return int
   *   > 0 is the items were correctly saved.
   */
  private function updateItemsQuantities(FormStateInterface $form_state) {
    $order_id = $this->orderEntity->id();
    $input = $form_state->getUserInput();
    $result = 0;
    foreach ($input['item-quantity'] as $product_id => $quantity) {
      $result = $result + \Drupal::service('pos_operations.item')->updateQuantity($order_id, $product_id, $quantity);
    }
    return $result;
  }

  /**
   * Remove items from the tableselect.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  private function removeSelectedItems(FormStateInterface $form_state) {
    foreach ($form_state->getValue('items_table') as $id => $value) {
      if ($value && \Drupal::service('pos_operations.item')->deleteItem($id)) {
        drupal_set_message($this->t('Item %title removed from the Order %order_id', [
          '%title' => $this->tableselectItems[$id]['title'],
          '%order_id' => $this->orderEntity->id(),
        ]));
      }
    }
  }

  /**
   * Temporary work around to get the job title's tid.
   *
   * @param string $job_title
   *   Descritive job title.
   *
   * @TODO: make the form input field autocomplete.
   *
   * @return int
   *   Job title ID.
   */
  private function getJobTitleTermId($job_title) {
    $result = NULL;
    if ($job_title) {
      $term = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $job_title, 'vid' => 'job_title']);
      if ($term) {
        $term = array_shift($term);
        $result = $term->id();
      }
      else {
        if (Term::create(['name' => $job_title, 'vid' => 'job_title'])->save()) {
          $result = $this->getJobTitleTermId($job_title);
        }
      }
    }
    return $result;
  }

  /**
   * Temporary work around to get the job title's name.
   *
   * @param int $tid
   *   Job title term id.
   *
   * @TODO: make the form input field autocomplete.
   *
   * @return string
   *   Job title name.
   */
  private function getJobTitleName($tid) {
    if ($term = Term::load($tid)) {
      $result = $term->getName();
    }
    else {
      $result = NULL;
    }
    return $result;
  }

}
