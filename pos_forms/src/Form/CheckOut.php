<?php

// @TODO: Find a way to not repeat emails in customer profile

/**
 * @file
 * Contains \Drupal\pos_forms\Form\CheckOut.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pos_entities\Entity\PosSessions;
use Drupal\pos_entities\Entity\PosCustomerProfiles;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Ajax\HtmlCommand;


/**
 * Class CheckOut.
 *
 * @package Drupal\pos_forms\Form
 */
class CheckOut extends FormBase {

  use \Drupal\pos_forms\Form\ValidateAndSave;

  private $profileEntity = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->entityTitle = 'Shipping Address';
    $this->redirectRoute = 'checkout';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'check_out';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $info_type = NULL, $info_id = NULL) {

    $form['#cache'] = ['max-age' => 0];

    if (\Drupal::service('pos_operations.session')->sessionIsOff()) {
      $form['expired'] = [
        '#id' => 'item-expired',
        '#type' => 'markup',
        '#markup' => $this->t('This form has expired.'),
      ];
      return $form;
    }

    $form['first_name'] = [
      '#id' => 'edit-first-name',
      '#type' => 'textfield',
      '#field_name' => 'first_name',
      '#title' => $this->t('First name'),
      '#maxlength' => 50,
      '#weight' => 15,
      '#required' => TRUE,
    ];

    $form['last_name'] = [
      '#id' => 'edit-last-name',
      '#type' => 'textfield',
      '#field_name' => 'last_name',
      '#title' => $this->t('Last name'),
      '#maxlength' => 50,
      '#weight' => 20,
      '#required' => TRUE,
    ];

    $form['honorific'] = [
      '#id' => 'edit-honorific',
      '#type' => 'select',
      '#field_name' => 'honorific',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_honorific'),
      '#title' => $this->t('Title'),
      '#weight' => 25,
    ];

    $form['organization'] = [
      '#id' => 'edit-organization',
      '#type' => 'textfield',
      '#field_name' => 'organization',
      '#title' => $this->t('Organization'),
      '#maxlength' => 125,
      '#weight' => 25,
    ];

    $form['address_1'] = [
      '#id' => 'edit-address-1',
      '#type' => 'textfield',
      '#field_name' => 'address_1',
      '#title' => $this->t('Address 1'),
      '#maxlength' => 254,
      '#weight' => 30,
      '#required' => TRUE,
    ];

    $form['address_2'] = [
      '#id' => 'edit-address-2',
      '#description' => t('(Include Suite/Room xxx#)'),
      '#type' => 'textfield',
      '#field_name' => 'address_2',
      '#title' => $this->t('Address 2'),
      '#maxlength' => 254,
      '#weight' => 35,
      '#suffix' => '<div class="help-text">(Include Suite/Room #)</div>',
    ];

    $form['city'] = [
      '#id' => 'edit-city',
      '#type' => 'textfield',
      '#field_name' => 'city',
      '#title' => $this->t('City'),
      '#maxlength' => 60,
      '#weight' => 40,
      '#required' => TRUE,
    ];

    $form['state'] = [
      '#id' => 'edit-state',
      '#type' => 'select',
      '#field_name' => 'state',
      '#options' => _pos_utils_us_states(),
      '#title' => $this->t('State'),
      '#weight' => 45,
      '#required' => TRUE,
    ];

    $form['zip'] = [
        '#id' => 'edit-zip',
        '#type' => 'textfield',
        '#field_name' => 'zip',
        '#title' => $this->t('Zip Code'),
        '#maxlength' => 10,
        '#weight' => 50,
        '#step' => 0,
        '#required' => TRUE,
    ];
/*
    $form['zip'] = [
      '#id' => 'edit-zip',
      '#type' => 'textfield',
      '#field_name' => 'zip',
      '#title' => 'Zip Code',
      '#maxlength' => 15,
      '#weight' => 37,
      '#description' => 'Please enter Zip Code',
      '#prefix' => '<div id="customer-zip-code"></div>',
      '#ajax' => [
        'callback' => '::zipAutocomplete',
        'effect' => 'fade',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
*/
    $form['phone'] = [
      '#id' => 'edit-phone',
      '#type' => 'textfield',
      '#field_name' => 'phone',
      '#title' => $this->t('Phone'),
      '#maxlength' => 50,
      '#weight' => 55,
      '#required' => TRUE,
      '#suffix' => '(Please use the format 2345678901, only numbers are allowed)',
    ];

    $form['phoneext'] = [
      '#id' => 'edit-phoneext',
      '#type' => 'textfield',
      '#field_name' => 'phoneext',
      '#title' => $this->t('Extension'),
      '#maxlength' => 6,
      '#weight' => 55,
      '#required' => FALSE,
      '#suffix' => '(Please use the format 12345, only numbers are allowed)',
    ];

    $form['job_title'] = [
      '#id' => 'edit-job-title',
      '#type' => 'textfield',
      '#field_name' => 'job_title',
      '#title' => $this->t('What’s your job title?'),
      '#maxlength' => 254,
      '#weight' => 65,
    ];

    $form['organization_type'] = [
      '#id' => 'edit-organizationtype',
      '#type' => 'select',
      '#field_name' => 'organization_type',
      '#options' => _pos_utils_get_vocabulary_as_select_options('organization_type'),
      '#title' => $this->t('What best describes your organization?'),
      '#weight' => 70,
    ];

    // Check if Anonymous user...Hide Admin fields.
    $hide_fields = \Drupal::currentUser()->isAnonymous();
    if (!$hide_fields) {
      $this->buildOrderDetails($form, $hide_fields);
    }

    if (!$form_state->getUserInput()) {
      if ($info_type == 'profile' && $info_id) {
        $this->buildProfileForm($form, $form_state, $info_type, $info_id);
      }
      else {
        $form['email'] = [
          '#id' => 'edit-email',
          '#type' => 'email',
          '#field_name' => 'email',
          '#title' => $this->t('Email Address'),
          '#maxlength' => 254,
          '#weight' => 10,
          '#required' => TRUE,
        ];
        if ($info_type == 'shipping' && $info_id) {
          $this->buildShippingForm($form, $form_state, $info_type, $info_id);
        }
      }
    }

    $form['review'] = [
      '#id' => 'review-button',
      '#name' => 'review',
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#weight' => 90,
    ];

    $form['cancel'] = [
      '#id' => 'cancel-link',
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('products.cart_list'),
      '#weight' => 92,
      '#attributes' => [
        'class' => ['js-form-submit', 'form-submit', 'btn-default btn'],
      ],
    ];

    $form['#validate'][] = '::checkoutFormValidation';
    return $form;
  }

  /**
   * Build the elements to handle profile data.
   *
   * @param array $form
   *   Form alements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $info_type
   *   Type of the info. In this case: "profile".
   * @param int $info_id
   *   Profile ID.
   */
  private function buildProfileForm(array &$form, FormStateInterface $form_state, $info_type = NULL, $info_id = NULL) {
    $form['use_user_profile'] = [
      '#id' => 'use-user-profile',
      '#name' => 'use_user_profile',
      '#type' => 'button',
      '#value' => $this->t("Use user's profile"),
      '#weight' => 0,
      '#ajax' => [
        'callback' => [$this, 'useUsersProfileCallback'],
        'event' => 'click',
        'profile_id' => $info_id,
        'progress' => [
          'type' => 'throbber',
          'message' => t("Pulling data from user's profile..."),
        ],
      ],
    ];
    $form['dont_use_user_profile'] = [
      '#id' => 'dont_use-user-profile',
      '#name' => 'dont_use_user_profile',
      '#type' => 'button',
      '#value' => $this->t("Clear fields"),
      '#weight' => 0,
      '#ajax' => [
        'callback' => [$this, 'clearUsersProfileCallback'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => t("Resetting"),
        ],
      ],
    ];
    $form['customer-id-fieldset'] = [
      '#id' => 'fieldset-customer-id',
      '#type' => 'fieldset',
      '#title' => $this->t('Customer email'),
      '#weight' => 10,
    ];
    $form['customer-id-fieldset']['email'] = [
      '#id' => 'edit-email',
      '#type' => 'textfield',
      '#field_name' => 'email',
      '#title' => $this->t('Customer email'),
      '#maxlength' => 254,
      '#weight' => 10,
    ];
    $fields_ids = [
      'email' => 'edit-email',
    ];
    foreach ($form as $item) {
      if (isset($item['#field_name'])) {
        $fields_ids[$item['#field_name']] = $item['#id'];
      }
    }
    $url = Url::fromRoute('checkout.choose_another_customer', ['fields_ids' => serialize($fields_ids)]);
    $url->setOptions([
      'attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'button--small',
          'button-add-to-cart',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 600]),
      ],
    ]);
    $form['customer-id-fieldset']['change_email'] = [
      '#id' => 'change-email-button',
      '#type' => 'link',
      '#title' => $this->t('Choose another customer'),
      '#url' => $url,
      '#weight' => 5,
    ];

    $profile_entity = PosCustomerProfiles::load($info_id);
    $form['customer-id-fieldset']['email']['#value'] = $profile_entity->getValue('email');
    foreach ($form as &$element) {
      if (isset($element['#field_name'])) {
        $element_value = $profile_entity->getValue($element['#field_name']);
        $element['#value'] = $element_value;
      }
    }

  }


  /**
   * Adds fields for the Order.
   *
   * @param array $form
   *   Form structure.
   */
  private function buildOrderDetails(&$form, $hide_fields = FALSE) {
    $form['order_details'] = [
      '#type' => 'fieldset',
      '#title' => t('Order Details'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => [
        'class' => [
          'fieldset-order_details',
        ],
      ],
      '#weight' => 75,
    ];

    $form['order_details']['call_center'] = [
      '#type' => 'select',
      '#field_name' => 'call_center',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_call_center'),
      '#title' => $this->t('Call Center'),
      '#weight' => 25,
    ];

    $form['order_details']['how_received'] = [
      '#type' => 'select',
      '#field_name' => 'how_received',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_how_received'),
      '#title' => $this->t('How Received'),
      '#weight' => 30,
    ];

    $form['order_details']['how_heard'] = [
      '#type' => 'select',
      '#field_name' => 'how_heard',
      '#options' => _pos_utils_get_vocabulary_as_select_options('pos_how_heard'),
      '#title' => $this->t('How Heard'),
      '#weight' => 60,
    ];

    $form['order_details']['caller_language'] = [
      '#type' => 'select',
      '#field_name' => 'caller_language',
      '#options' => _pos_utils_get_vocabulary_as_select_options('language'),
      '#title' => $this->t('Caller Language'),
      '#weight' => 60,
    ];

    foreach ($form['order_details'] as &$element) {
      if (!is_object($element) && isset($element['#field_name'])) {
        $element['#disabled'] = $hide_fields;
      }
    }
  }


  /**
   * Build the elements to handle shipping data.
   *
   * @param array $form
   *   Form alements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $info_type
   *   Type of the info. In this case: "shipping".
   * @param int $info_id
   *   Profile ID.
   */
  private function buildShippingForm(array &$form, FormStateInterface $form_state, $info_type = NULL, $info_id = NULL) {
    if (!$form_state->getUserInput()) {
      $shipping = \Drupal::service('pos_operations.shipping')
        ->getShippingById($info_id);
      $order = \Drupal::service('pos_operations.order')->getRawOrder($info_id);
      foreach ($form as &$element) {
        if (isset($element['#field_name'])) {
          // Begin PHP 7.
          // $element['#value'] = @$shipping[$element['#field_name']] ?? @$order[$element['#field_name']];
          // End PHP 7.
          // Begin PHP 5.6.
          $field_name = $element['#field_name'];
          if (isset($shipping[$field_name])) {
            $element['#value'] = $shipping[$field_name];
          }
          else {
            $element['#value'] = $order[$field_name];
          }
          // End PHP 5.6.
          if ($element['#field_name'] == 'job_title') {
            $element['#value'] = $this->getJobTitleName(@$order[$element['#field_name']]);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutFormValidation(array &$form, FormStateInterface $form_state) {
    $input_values = $form_state->getUserInput();
    if (isset($input_values['email'])) {
      $email = trim($input_values['email']);
      if (\Drupal::service('email.validator')->isValid($email)) {
        $form_state->setValue('email', $email);
      }
      else {
        $form_state->setErrorByName('email', $this->t('Invalid email.'));
      }
    }
    $zip = trim($input_values['zip']);
    if (!validateUSAZip($zip)) {
      $form_state->setErrorByName('zip', $this->t('Invalid zip. Make sure you\'ve entered the ZIP code in either 5-digit or 9-digit format, for example 12345 or 12345-6789.'));
    }

    $phone = trim($input_values['phone']);
    if (!validatePhone($phone)) {
      $form_state->setErrorByName('phone', $this->t('Invalid phone. Enter numeric values only.'));
    }

    $phoneext = trim($input_values['phoneext']);
    if (!validatePhoneExt($phoneext)) {
      $form_state->setErrorByName('phoneext', $this->t('Invalid Extension. Enter numeric values only.'));
    }

  }


  /**
   * Ajax callback for textfield "Zip Code".
   *
   * @param array $form
   *   The $form_state object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */
  /*
  public function zipAutocomplete(array $form, FormStateInterface $form_state) {
    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();

    $zipcode = $form_state->getValue('zip');

    $city_state = \Drupal::service('pos_operations.profile')
      ->getCityStateByZip($zipcode);
    if ($city_state) {
      $color = 'green';
      $city = $city_state['City'];
      $state = $city_state['State'];

      $ajax_response->addCommand(new InvokeCommand('#edit-city', 'val', [$city]));
      $ajax_response->addCommand(new InvokeCommand('#edit-state', 'val', [$state]));
      $ajax_response->addCommand(new HtmlCommand('#customer-zip-code', ''));
      $ajax_response->addCommand(new InvokeCommand('#customer-zip-code', 'css', [
        'color',
        $color,
      ]));
    }
    else {
      $color = 'red';
      $ajax_response->addCommand(new HtmlCommand('#customer-zip-code', 'Invalid Zip Code'));
      // Add a command, InvokeCommand, which allows for custom jQuery commands.
      // In this case, we alter the color of the description.
      $ajax_response->addCommand(new InvokeCommand('#customer-zip-code', 'css', [
        'color',
        $color,
      ]));
    }
    // Return the AjaxResponse Object.
    return $ajax_response;
  }

*/
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    switch ($triggering_element['#name']) {
      case 'review':
        $this->sendToReview($form_state);
        break;

      case 'cancel':
        $form_state->setRedirect('checkout');
        break;

      default:
        // Do nothing.
    }
  }

  /**
   * Gathers data to be sent to the Review form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   */
  private function sendToReview(FormStateInterface $form_state) {
    $session_id = \Drupal::service('pos_operations.session')->getId();
    if ($session_entity = PosSessions::load($session_id)) {
      $order_id = $session_entity->getOrderId();
      $values = $form_state->getValues();
      $values['customer_id'] = (int) $session_entity->getCustomerId();
      // Check if Anonymous user...Hide Admin fields.
      $hide_fields = \Drupal::currentUser()->isAnonymous();
      if ($hide_fields) {
        $values['caller_language'] = 0;
        $values['call_center'] = 0;

        $is_anonymous = \Drupal::currentUser()
          ->isAnonymous();// Check if Anonymous user...Hide Admin fields
        $term_name = 'Online';
        $termid = _pos_utils_get_term_by_name_vid($term_name, 'pos_how_received');
        $values['how_received'] = $is_anonymous ? $termid : 0;
      }
      $values['job_title'] = $this->getJobTitleTermId($values['job_title']);
      if (!$order_id) {
        $order_id = \Drupal::service('pos_operations.order')
          ->createNewOrder($session_entity, $values);
      }
      else {
        \Drupal::service('pos_operations.order')
          ->updateOrder($order_id, $values);
      }
      $values['order_id'] = $order_id;
      \Drupal::service('pos_operations.shipping')->updateShipping($values);
      $form_state->setRedirect('checkout.review', ['order_id' => $order_id]);
    }
    else {
      drupal_set_message($this->t('There are no items in the cart.'), 'warning');
    }
  }

  /**
   * Ajax callback for checkbox "Use user's profile".
   *
   * @param array $form
   *   The $form_state object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */
  public function useUsersProfileCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $profile_id = $form['use_user_profile']['#ajax']['profile_id'];
    $profile_entity = PosCustomerProfiles::load($profile_id);
    foreach ($form as $element) {
      if (isset($element['#field_name'])) {
        $element_id = '#' . $element['#id'];
        $element_value = $profile_entity->getValue($element['#field_name']);
        $response->addCommand(new InvokeCommand($element_id, 'val', [$element_value]));
      }
    }
    $element_value = $profile_entity->getValue('email');
    $response->addCommand(new InvokeCommand('#edit-email', 'val', [$element_value]));
    return $response;
  }

  /**
   * Ajax callback for clearing the user's profile.
   *
   * @param array $form
   *   The $form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */
  public function clearUsersProfileCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    foreach ($form as $element) {
      if (isset($element['#field_name'])) {
        $element_id = '#' . $element['#id'];
        $response->addCommand(new InvokeCommand($element_id, 'val', [NULL]));
      }
    }
    $response->addCommand(new InvokeCommand('#edit-email', 'val', [NULL]));
    return $response;
  }

  /**
   * Ajax callback for field Customer ID.
   *
   * @param array $form
   *   The $form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   *
   * @return AjaxResponse $response
   *   The Ajax response.
   */
  public function changeCustomerIdCallback(array $form, FormStateInterface $form_state) {
    $profile_id = $form_state->getValue('customer_id_new');
    $this->profileEntity = PosCustomerProfiles::load($profile_id);
    return $this->useUsersProfileCallback($form, $form_state);
  }


  /**
   * Temporary work around to get the job title's tid.
   *
   * @param string $job_title
   *   Descritive job title.
   *
   * @TODO: make the form input field autocomplete.
   *
   * @return int
   *   Job title ID.
   */
  private function getJobTitleTermId($job_title) {
    $result = TRUE;
    if ($job_title = trim($job_title)) {
      $term = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $job_title, 'vid' => 'job_title']);
      if ($term) {
        $term = array_shift($term);
        $result = $term->id();
      }
      else {
        if (Term::create(['name' => $job_title, 'vid' => 'job_title'])
          ->save()) {
          $result = $this->getJobTitleTermId($job_title);
        }
      }
    }
    return $result;
  }

  /**
   * Temporary work around to get the job title's name.
   *
   * @param int $tid
   *   Job title term id.
   *
   * @TODO: make the form input field autocomplete.
   *
   * @return string
   *   Job title name.
   */
  private function getJobTitleName($tid) {
    if ($term = Term::load($tid)) {
      $result = $term->getName();
    }
    else {
      $result = NULL;
    }
    return $result;
  }

}
