<?php

/**
 * @file
 * Contains \Drupal\pos_forms\Form\ChooseAnotherCustomer.
 */

namespace Drupal\pos_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;


/**
 * Class ChooseAnotherCustomer.
 *
 * @package Drupal\pos_forms\Form
 */
class ChooseAnotherCustomer extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'choose_another_customer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,  $fields_ids = NULL) {

    $fields_ids = unserialize($fields_ids);
    $form_state->set('fields_ids', $fields_ids);

    $form['search_for'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search for'),
      '#description' => $this->t('Will be searched in first and last names, telephone and email'),
      '#maxlength' => 64,
    ];

    $form['search_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#submit' => array('::addResults'),
      '#ajax' => [
        'callback' => '::searchButtonCallback',
        'wrapper' => 'results-wrapper',
      ],
    ];

    $form['results_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'results-wrapper'],
    ];

    if ($results_rows = $form_state->get('results_rows')) {
      foreach ($results_rows as $result_row) {
        $row_container_name = 'row_container_' . $result_row['id'];
        $form['results_container'][$row_container_name] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['customer-search-results-row-wrapper']],
        ];
        $form['results_container'][$row_container_name]['choose_button_' . $result_row['id']] = [
          '#type' => 'submit',
          '#name' => 'choose-button-' . $result_row['id'],
          '#value' => $this->t('Choose'),
          '#ajax' => [
            'callback' => '::chooseButtonCallback',
            'event' => 'click',
            'profile' => $result_row,
          ],
        ];
        $form['results_container'][$row_container_name]['email_' . $result_row['id']] = [
          '#type' => 'markup',
          '#markup' => $result_row['email'],
          '#prefix' => '<span class="customer-search-results-field-wrapper">',
          '#suffix' => '</span>',
        ];
        $form['results_container'][$row_container_name]['first_name_' . $result_row['id']] = [
          '#type' => 'markup',
          '#markup' => $result_row['first_name'],
          '#prefix' => '<span class="customer-search-results-field-wrapper">',
          '#suffix' => '</span>',
        ];
        $form['results_container'][$row_container_name]['last_name_' . $result_row['id']] = [
          '#type' => 'markup',
          '#markup' => $result_row['last_name'],
          '#prefix' => '<span class="customer-search-results-field-wrapper">',
          '#suffix' => '</span>',
        ];
        $form['results_container'][$row_container_name]['phone_' . $result_row['id']] = [
          '#type' => 'markup',
          '#markup' => $result_row['phone'],
          '#prefix' => '<span class="customer-search-results-field-wrapper">',
          '#suffix' => '</span>',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function addResults(array &$form, FormStateInterface $form_state) {
    $search_for = trim($form_state->getValue('search_for'));
    $results = NULL;
    if (!empty($search_for)) {
      $results = $this->getResults($search_for);
    }
    $form_state->set('results_rows', $results);
    $form_state->setRebuild();
  }

  /**
   * Implements the submit handler for the ajax call.
   *
   * @param array $form
   *   Render array representing from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array
   *   Form element array.
   */
  public function searchButtonCallback(array &$form, FormStateInterface $form_state) {
    return $form['results_container'];
  }

  /**
   * Implements the submit handler for the ajax call.
   *
   * @param array $form
   *   Render array representing from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Array of ajax commands to execute on submit of the modal form.
   */
  public function chooseButtonCallback(array &$form, FormStateInterface $form_state) {
    $fields_ids = $form_state->get('fields_ids');
    $response = new AjaxResponse();
    $triggering_element = $form_state->getTriggeringElement();
    $profile = $triggering_element['#ajax']['profile'];
    foreach ($fields_ids as $field_name => $element_id) {
      $element_id = '#' . $element_id;
      $element_value = $profile[$field_name];
      $response->addCommand(new InvokeCommand($element_id, 'val', array($element_value)));
    }
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * Search for customer based on a search term.
   *
   * @param string $search_for
   *   Search term.
   *
   * @return string
   *   Rendered table with the search results.
   */
  private function getResults($search_for) {
    $search_for = '%' . $search_for . '%';
    $connection = Database::getConnection();
    $query = $connection->select('pos_customer_profiles', 'c');
    $query->addField('c', 'id');
    $query->addField('c', 'email');
    $query->addField('c', 'first_name');
    $query->addField('c', 'last_name');
    $query->addField('c', 'phone');
    $query->addField('c', 'honorific');
    $query->addField('c', 'organization');
    $query->addField('c', 'address_1');
    $query->addField('c', 'address_2');
    $query->addField('c', 'city');
    $query->addField('c', 'zip');
    $or = $query->orConditionGroup();
    $or->condition('c.email', $search_for, 'LIKE');
    $or->condition('c.first_name', $search_for, 'LIKE');
    $or->condition('c.last_name', $search_for, 'LIKE');
    $or->condition('c.phone', $search_for, 'LIKE');
    $query->condition($or);
    $data = $query->execute();
    $profiles = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $profiles;
  }

}
